import 'dart:async';
import 'package:flutter/material.dart';
import 'package:woof_fini/AbstractApi/AuthAPI.dart';
import 'package:woof_fini/AbstractApi/PostAPI.dart';
import 'package:woof_fini/BLocs/BLoc.dart';
import 'package:woof_fini/Model/ForgotPassword/ForgotPasswordRequest.dart';
import 'package:woof_fini/Model/Login/LoginRequest.dart';
import 'package:woof_fini/Model/SignUp/SignUpRequest.dart';
import 'package:woof_fini/Model/UserDetail/UserDetailRequest.dart';
import 'package:woof_fini/AbstractApi/AuthAPI.dart';

class PostBLoc extends BLoc {
  PostAPI postAPI;

  PostBLoc({
    @required this.postAPI,
  });

  Future<dynamic> postList({
    @required BuildContext context,
  }) async {
    return await postAPI.postList(
      context: context,
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
  }

  /* Future<dynamic> signUpUser({
    @required SignUpRequest request,
    @required BuildContext context,
  }) async {
    return await authAPI.signUpUser(
      requestBody: request,
      context: context,
    );
  }

  Future<dynamic> signUpOTPVerify({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.signUpOTPVerifyUser(
      requestBody: request,
      context: context,
    );
  }

  Future<dynamic> changePhoneEmailOTPVerifyUser({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.changePhoneEmailOTPVerifyUser(
      requestBody: request,
      context: context,
    );
  }

  Future<dynamic> resendOTP({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.resendOTPUser(
      requestBody: request,
      context: context,
    );
  }

  Future<dynamic> forgotPassword({
    @required ForgotPasswordRequest request,
    @required BuildContext context,
  }) async {
    return await authAPI.forgotPasswordUser(
      requestBody: request,
      context: context,
    );
  }

  Future<dynamic> forgotOtpVerify({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.forgotOTPVerifyUser(
      requestBody: request,
      context: context,
    );
  }

  Future<dynamic> resetPassword({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.resetPasswordUser(
      requestBody: request,
      context: context,
    );
  }

  Future<dynamic> changePassword({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.changePasswordUser(
      requestBody: request,
      context: context,
    );
  }

  Future<dynamic> changeLanguage({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.changeLanguage(
      requestBody: request,
      context: context,
    );
  }

  Future<dynamic> pushNotificationToggle({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.pushNotificationToggle(
        requestBody: request, context: context);
  }

  Future<dynamic> changePhoneEmail({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.changePhoneEmail(
      requestBody: request,
      context: context,
    );
  }

//  uploadCompanyLogoUser
  Future<dynamic> companyLogo(
      {@required Map request, @required BuildContext context}) async {
    return await authAPI.companyLogoUser(
      requestBody: request,
      context: context,
    );
  }

  //  skip user profile
  Future<dynamic> skipProfile({
    @required Map request,
    @required BuildContext context,
  }) async {
    return await authAPI.skipUserProfile(
      requestBody: request,
      context: context,
    );
  }

  //  Check user profile is approved or not by admin.
  Future<dynamic> isUserApproved({
    @required BuildContext context,
  }) async {
    return await authAPI.isUserApproved(
      context: context,
    );
  }

  //  Returns user profile is approved or not by admin.
  Future<dynamic> getProfile({
    @required BuildContext context,
  }) async {
    return await authAPI.getProfile(
      context: context,
    );
  }

  //  Check categories.
  Future<dynamic> getCategories({
    @required BuildContext context,
  }) async {
    return await authAPI.getCategories(
      context: context,
    );
  }

  //  Update Farmer Profile
  Future<dynamic> updateProfile({
    @required BuildContext context,
    @required UserDetailRequest userDetailRequest,
  }) async {
    return await authAPI.updateProfile(
      context: context,
      userDetailRequest: userDetailRequest,
    );
  }

  //  Gets warehouses.
  Future<dynamic> getWarehouses({
    @required BuildContext context,
    @required int pageNumber,
  }) async {
    return await authAPI.getWarehouses(
      context: context,
      pageNumber: pageNumber,
    );
  }

  //  Add Warehouse
  Future<dynamic> addWarehouse({
    @required BuildContext context,
    @required Map request,
  }) async {
    return await authAPI.addWarehouse(
      context: context,
      requestBody: request,
    );
  }

  //  Update Warehouse
  Future<dynamic> updateWarehouse({
    @required BuildContext context,
    @required Map request,
    @required String warehouseId,
  }) async {
    return await authAPI.updateWarehouse(
      context: context,
      requestBody: request,
      warehouseId: warehouseId,
    );
  }

  @override
  void dispose() {
    // implement dispose
  }*/
}
