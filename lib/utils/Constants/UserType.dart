// User type
import '../LocalizationValues.dart';

enum UserType {
  ADMIN,
  SUB_ADMIN,
  EXPORTER,
  MILL,
  FARMER,
  IMPORTER,
  ROASTER,
  CAFE_STORE,
  CO_OPS,
  CUSTOMER,
}

// User type keys
class UserTypeKeys {
  static const String admin = "ADMIN";
  static const String subAdmin = "SUB_ADMIN";
  static const String exporter = "EXPORTER";
  static const String mill = "MILL";
  static const String farmer = "FARMER";
  static const String importer = "IMPORTER";
  static const String roaster = "ROASTER";
  static const String cafeStore = "CAFE_STORE";
  static const String coops = "CO_OPS";
  static const String customer = "CUSTOMER";
}

class UserTypeValue {
  static const int admin = 1;
  static const int subAdmin = 2;
  static const int exporter = 3;
  static const int mill = 4;
  static const int farmer = 5;
  static const int importer = 6;
  static const int roaster = 7;
  static const int cafeStore = 8;
  static const int coops = 9;
  static const int customer = 10;
}

//User Types Code map
const UserTypesCodeMap = {
  UserTypeKeys.admin: 1,
  UserTypeKeys.subAdmin: 2,
  UserTypeKeys.exporter: 3,
  UserTypeKeys.mill: 4,
  UserTypeKeys.farmer: 5,
  UserTypeKeys.importer: 6,
  UserTypeKeys.roaster: 7,
  UserTypeKeys.cafeStore: 8,
  UserTypeKeys.coops: 9,
  UserTypeKeys.customer: 10,
};

//User Value Code to Type map
const UserCodeToTypeMap = {
  1: UserType.ADMIN,
  2: UserType.SUB_ADMIN,
  3: UserType.EXPORTER,
  4: UserType.MILL,
  5: UserType.FARMER,
  6: UserType.IMPORTER,
  7: UserType.ROASTER,
  8: UserType.CAFE_STORE,
  9: UserType.CO_OPS,
  10: UserType.CUSTOMER,
};

//User Value Code to label  map
const UserCodeToLabelMap= {
  3: LocalizationValues.exporter,
  4: LocalizationValues.mill,
  5: LocalizationValues.farmer,
  6: LocalizationValues.importer,
  7: LocalizationValues.roaster,
  8: LocalizationValues.cafeStore,
  9: LocalizationValues.coops,
  10: LocalizationValues.customer,
};

class LanguageCode {
  static const String english = 'en';
  static const String spanish = 'es';
}
