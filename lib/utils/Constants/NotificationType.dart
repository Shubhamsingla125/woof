class NotificationType{
  static const int newUser= 0;
  static const int homeTabNewOrders= 1;
  static const int ordersTabInProgress= 2;
  static const int farmerInProgressOrderDetails= 11;
}
