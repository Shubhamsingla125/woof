class AdditionalFileType {
  static const int DOC = 1;
  static const int PHOTO = 2;
  static const int WEB_LINK = 3;
}
