class DateFormatPattern {
  DateFormatPattern._();

  static const String monthDayYearHourMinute = "MMM dd, y, hh:mma";
  static const String dayMonthYear = "dd-MMM-yyyy";
}
