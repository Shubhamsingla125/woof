// Order type
import 'package:flutter/material.dart';
import '../Localization.dart';
import '../LocalizationValues.dart';

enum OrderType {
  REQUEST,
  IN_PROGRESS,
  COMPLETED,
}

// Sub order status
class SubOrderStatus {
  static const int pending = 0;
  static const int accepted = 1;
  static const int rejected = 2;
  static const int cancelled = 3;
  static const int expired = 4;
  static const int completed = 5;
  static const int approvedDataPoints = 6;
  static const int declinedDataPoints = 7;
  static const int dataPointsApprovalPending = 8;
  static const int processing = 1;
  static const int atMill = 9;
  static const int readyToShip = 10;
  static const int subOrderCreationPending = 11;
  static const int orderReady = 12;
}

// Farmer Payment status
class FarmerPaymentStatus {
  static const int pending = 0;
  static const int completed = 1;
}

// Importer accept status
class ImporterAcceptStatus {
  static const int accept = 1;
  static const int reject = 2;
}

// Main order status
class MainOrderStatus {
  static const int pending = 0;
  static const int exporter_accepted = 1;
  static const int farmer_accepted = 2;
  static const int delivered_at_mill = 3;
  static const int ready_at_mill = 4;
  static const int shipped_from_mill = 5;
  static const int received_by_importer = 6;
  static const int shipped_by_importer = 7;
  static const int received_by_roaster = 8;
  static const int shipped_by_roaster = 9;
  static const int received_by_cafe = 10;
  static const int completed = 11;
  static const int expired = 12;

  static const Map<int, String> labels = {
    pending: LocalizationValues.pending,
    exporter_accepted: LocalizationValues.exporter_accepted,
    farmer_accepted: LocalizationValues.farmer_accepted,
    delivered_at_mill: LocalizationValues.delivered_at_mill,
    ready_at_mill: LocalizationValues.ready_at_mill,
    shipped_from_mill: LocalizationValues.shipped_from_mill,
    received_by_importer: LocalizationValues.received_by_importer,
    shipped_by_importer: LocalizationValues.shipped_by_importer,
    received_by_roaster: LocalizationValues.received_by_roaster,
    shipped_by_roaster: LocalizationValues.shipped_by_roaster,
    received_by_cafe: LocalizationValues.received_by_cafe,
    completed: LocalizationValues.completed,
    expired: LocalizationValues.expired,
  };

  static String getLocalizedLabel({
    @required BuildContext context,
    @required int status,
  }) {
    return Localization.of(context).trans(labels[status]) ?? "";
  }
}

// Farmer order status
class FarmerOrderStatus {
  static const int pending = 0;
  static const int accepted = 1;
  static const int declined = 2;
  static const int cancelled = 3;
  static const int completed = 4;
  static const int expired = 5;

  static const Map<int, String> labels = {
    pending : LocalizationValues.pending,
    accepted : LocalizationValues.accepted,
    declined : LocalizationValues.declined,
    cancelled : LocalizationValues.cancelled,
    completed : LocalizationValues.completed,
    expired : LocalizationValues.expired,
  };

  static String getLocalizedLabel({
    @required BuildContext context,
    @required int status,
  }) {
    return Localization.of(context).trans(labels[status]) ?? "";
  }
}

class SendDataPointType {
  static const int sendDataPoints = 1;
  static const int sendOtpForDataPoints = 2;
}

class AcceptRejectStatus {
  static const int accept = 1;
  static const int reject = 2;
}
