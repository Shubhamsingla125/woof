import 'package:intl/intl.dart';

class StringUtils {
  static String capitalize(String s) {
    if (s == null || s.isEmpty) return s;
    return '${s[0].toUpperCase()}${s.substring(1)}';
  }

  static String capitalizeEveryWord(String s) {
    if (s == null || s.isEmpty) return s;

    var output = "";

    s.split(" ").forEach((i) {
      if (output.isNotEmpty) output += " ";
      output += toBeginningOfSentenceCase(i);
    });

    return output;
  }

  static String initialFromString(String name, {int count = 2}) {
    if (name == null || name.isEmpty) return name;

    var output = "";

    var items = name.split(" ");

    for (int i = 0; i < items.length; i++) {
      if (items[i].isEmpty) continue;
      output += items[i][0];
      if (output.length == count) break;
    }

    return output;
  }

  static bool isValidString(String value) {
    return value != null && value.isNotEmpty && !value.contains("null");
  }

  static bool isValidEmail(String email) {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
  }
}
