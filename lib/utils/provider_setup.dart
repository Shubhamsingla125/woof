//import 'package:maid_me/modules/_common/entities/theme_state.dart';
import 'package:provider/provider.dart';
import 'package:woof_fini/utils/theme_state.dart';

List<SingleChildCloneableWidget> providers = [
  ...independentServices,
  ...dependentServices,
];

List<SingleChildCloneableWidget> independentServices = [
  ChangeNotifierProvider.value(value: ThemeState()),
];

List<SingleChildCloneableWidget> dependentServices = [];
