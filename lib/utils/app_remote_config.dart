class AppRemoteConfig {
  int maxCalenderWeeks = 5;

  int minimumHoursPerJob = 2;
  int maximumHoursPerJob = 8;

  int providerServiceStartTime = 6;
  int providerServiceEndTime = 20;

  int defaultProviderServiceStartTime = 9;
  int defaultProviderServiceEndTime = 17;
}
