import 'package:flutter/material.dart';
import 'package:woof_fini/utils/app_localizations.dart';
import 'package:woof_fini/utils/string_utils.dart';
import 'package:woof_fini/utils/theme.dart';

import 'app_constants.dart';

/// Text

/// NAME       SIZE   WEIGHT   SPACING  2018 NAME
/// display4   112.0  thin     0.0      headline1
/// display3   56.0   normal   0.0      headline2
/// display2   45.0   normal   0.0      headline3
/// display1   34.0   normal   0.0      headline4
/// headline   24.0   normal   0.0      headline5
/// title      20.0   medium   0.0      headline6
/// subhead    16.0   normal   0.0      subtitle1
/// body2      14.0   medium   0.0      body1
/// body1      14.0   normal   0.0      body2
/// caption    12.0   normal   0.0      caption
/// button     14.0   medium   0.0      button
/// subtitle   14.0   medium   0.0      subtitle2
/// overline   10.0   normal   0.0      overline

class ThemeHelper {
  static Widget paddingTop({padding = ThemeDimension.paddingVerySmall}) =>
      Padding(padding: EdgeInsets.only(top: padding));

  static Widget paddingTopSmall({padding = ThemeDimension.paddingSmall15}) =>
      Padding(
        padding: EdgeInsets.only(top: padding),
      );

  static Widget paddingTopLarge({padding = ThemeDimension.paddingLarge30}) =>
      Padding(
        padding: EdgeInsets.only(top: padding),
      );

//  static Widget paddingTopSmall({padding = ThemeDimension.paddingSmall}) =>
//      Padding(padding: EdgeInsets.only(top: padding));

  static Widget paddingTopTiny() =>
      Padding(padding: EdgeInsets.only(top: ThemeDimension.paddingTiny));

  static Widget paddingLeft({padding = ThemeDimension.paddingVerySmall}) =>
      Padding(padding: EdgeInsets.only(left: padding));

  static Widget paddingLeftTiny({padding = ThemeDimension.paddingTiny}) =>
      Padding(padding: EdgeInsets.only(left: padding));

  static Widget paddingHorizontal(
          {padding = ThemeDimension.paddingVerySmall}) =>
      Padding(padding: EdgeInsets.symmetric(horizontal: padding));

  static Widget paddingVertical({padding = ThemeDimension.paddingVerySmall}) =>
      Padding(padding: EdgeInsets.symmetric(vertical: padding));

  static Text headingText(context, String text) =>
      Text(text, style: Theme.of(context).textTheme.headline);

  static Text heading1Text(context, String text) =>
      Text(text, style: Theme.of(context).textTheme.display4);

  static Text heading2Text(context, String text) =>
      Text(text, style: Theme.of(context).textTheme.display3);

  static Text heading3Text(context, String text) =>
      Text(text, style: Theme.of(context).textTheme.display2);

  static Text heading4Text(context, String text) =>
      Text(text, style: Theme.of(context).textTheme.display1);

  static Text heading5Text(context, String text) =>
      Text(text, style: Theme.of(context).textTheme.headline);

  static Text headingTextWithColor(context, String text, {color}) => Text(text,
      style: Theme.of(context).textTheme.headline.copyWith(
          color: color ?? ThemeColor.primaryColor,
          fontWeight: FontWeight.bold));

  static Text heading5TextGreenBold(context, String text) => Text(text,
      style: Theme.of(context)
          .textTheme
          .headline
          .copyWith(color: ThemeColor.greenFF, fontWeight: FontWeight.bold));

  static Text heading5TextPrimaryBold(context, String text) => Text(text,
      style: Theme.of(context).textTheme.headline.copyWith(
          color: ThemeColor.primaryColor, fontWeight: FontWeight.bold));

  static Text heading6Text(context, String text,
          {Color color = ThemeColor.heading6FontColor}) =>
      Text(text,
          style: Theme.of(context).textTheme.title.copyWith(color: color));

  static Text subHeadingText(context, String text) =>
      Text(text, style: Theme.of(context).textTheme.subhead);

  static Text subHeadingTextWithColorPrimary(context, String text) =>
      subHeadingTextWithColor(context, text, ThemeColor.primaryColor);

  static Text subHeadingTextWithPrimaryColorWeightSemiBold(
          context, String text) =>
      Text(text,
          style: Theme.of(context).textTheme.subhead.copyWith(
              color: ThemeColor.primaryColor, fontWeight: FontWeight.w600));

  static Text subHeadingTextWithColor(context, String text, var color,
          {bool isBold: false, var maxLines}) =>
      Text(
        text,
        style: Theme.of(context).textTheme.subhead.copyWith(
            color: color,
            fontWeight: isBold ? FontWeight.bold : FontWeight.normal),
        maxLines: maxLines,
      );

  static Text subHeading2TextWithColor(context, String text, var color,
          {bool isBold: false, var maxLines}) =>
      Text(
        text,
        style: Theme.of(context).textTheme.display3.copyWith(
            color: color,
            fontWeight: isBold ? FontWeight.bold : FontWeight.normal),
        maxLines: maxLines,
      );

  static Text titleTextWithColor(context, String text, var color,
          {bool isBold: false,
          var maxLines,
          TextAlign textAlign: TextAlign.left}) =>
      Text(
        text,
        textAlign: textAlign,
        style: Theme.of(context).textTheme.title.copyWith(
            color: color,
            fontWeight: isBold ? FontWeight.bold : FontWeight.normal),
        maxLines: maxLines,
      );

  static Text overLineText(context, String text) =>
      Text(text, style: Theme.of(context).textTheme.overline);

  static Text overLineTextWithColorFF89(context, String text) => Text(text,
      style: Theme.of(context)
          .textTheme
          .overline
          .copyWith(color: ThemeColor.blackTextColorFF89));

  static Text overLineWithWeightText(context, String text,
          {int maxLines, Color color, FontWeight weight: FontWeight.w400}) =>
      Text(text,
          style: Theme.of(context)
              .textTheme
              .overline
              .copyWith(color: color, fontWeight: weight),
          maxLines: maxLines);

  static Text overLineTextWithColor(context, String text,
          {Color color, TextAlign textAlignment = TextAlign.left}) =>
      Text(text,
          textAlign: textAlignment,
          style: Theme.of(context).textTheme.overline.copyWith(color: color));

// predefined methods are not working...so made this new method @akshay
  static Text VATTextWithColorAndSize(context, String text,
          {Color color, double size}) =>
      Text(text, style: TextStyle(fontSize: 10, color: color));

  static Text titleText(context, String text,
          {int maxLines, Color color, FontWeight weight: FontWeight.w400}) =>
      Text(text,
          style: Theme.of(context)
              .textTheme
              .title
              .copyWith(color: color, fontWeight: weight),
          maxLines: maxLines);

  static Text titleTextFontNormal(context, String text, {int maxLines}) =>
      titleText(context, text, weight: FontWeight.normal, maxLines: maxLines);

  static Text titleText99Color(context, String text, {int maxLines}) =>
      titleText(context, text, color: Color(0x99000000), maxLines: maxLines);

  static TextStyle subtitleStyle(BuildContext context,
          {Color color,
          FontWeight weight: FontWeight.normal,
          double letterSpacing = 0}) =>
      Theme.of(context).textTheme.subtitle.copyWith(
          color: color ?? Theme.of(context).textTheme.subtitle.color,
          fontWeight: weight,
          letterSpacing: letterSpacing);

  static Text subTitleText(context, String text,
          {int maxLines, Color color, FontWeight weight: FontWeight.w400}) =>
      Text(text,
          style: Theme.of(context)
              .textTheme
              .subtitle
              .copyWith(color: color, fontWeight: weight),
          maxLines: maxLines);

  static Text subTitleTextPrimary(context, String text, {int maxLines}) =>
      subTitleText(context, text,
          color: Theme.of(context).primaryColor, maxLines: maxLines);

  //change text color to blackTextColorFF = Color(0xFF333348);
  static Text subTitleTextFF(context, String text, {int maxLines}) => Text(text,
      style: Theme.of(context)
          .textTheme
          .subtitle
          .copyWith(color: ThemeColor.blackTextColorFF33),
      maxLines: maxLines);

  static Text subTitleTextWithColor(context, String text,
          {int maxLines,
          var color,
          bool isBold: false,
          TextAlign textAlign: TextAlign.left}) =>
      Text(
        text,
        style: Theme.of(context).textTheme.subtitle.copyWith(
            color: color ?? Color(0x4c000000),
            fontWeight: isBold ? FontWeight.bold : FontWeight.normal),
        maxLines: maxLines,
        textAlign: textAlign,
      );

  static Text subTitleTextFontWeight400(context, String text, {int maxLines}) =>
      Text(text,
          style: Theme.of(context)
              .textTheme
              .subtitle
              .copyWith(fontWeight: FontWeight.normal),
          maxLines: maxLines);

  static Text subTitleTextWithColorFF31WeightMedium(context, String text,
          {int maxLines}) =>
      Text(text,
          style: Theme.of(context).textTheme.subtitle.copyWith(
              fontWeight: FontWeight.w500,
              color: ThemeColor.blackTextColorFF31),
          maxLines: maxLines);

  static Text subTitleTextDeColor(context, String text, {int maxLines}) =>
      Text(text,
          style: Theme.of(context)
              .textTheme
              .subtitle
              .copyWith(color: Color(0xde000000)),
          maxLines: maxLines);

  /// body 2.............
  static Text body2Text(BuildContext context, String text, {int maxLines}) =>
      Text(text, style: Theme.of(context).textTheme.body2, maxLines: maxLines);

  static Text body2TextPrimaryColor(BuildContext context, String text,
          {int maxLines}) =>
      Text(text,
          style: Theme.of(context)
              .textTheme
              .body2
              .copyWith(color: Theme.of(context).primaryColor),
          maxLines: maxLines);

  static Text body2TextWithColor(BuildContext context, String text,
          {int maxLines,
          var color,
          bool isBold: false,
          TextAlign textAlignment = TextAlign.left}) =>
      Text(text,
          textAlign: textAlignment,
          style: Theme.of(context).textTheme.body2.copyWith(
              color: color,
              fontWeight: isBold ? FontWeight.bold : FontWeight.normal),
          maxLines: maxLines);

  static Text body1Text(BuildContext context, String text, {int maxLines}) =>
      Text(text, style: Theme.of(context).textTheme.body1, maxLines: maxLines);

  static Text body1TextWithColor(BuildContext context, String text,
          {int maxLines,
          var color,
          bool isBold: false,
          TextAlign textAlignment = TextAlign.left}) =>
      Text(text,
          textAlign: textAlignment,
          style: Theme.of(context).textTheme.body1.copyWith(
              color: color,
              fontWeight: isBold ? FontWeight.bold : FontWeight.normal),
          maxLines: maxLines);

  static Text body1TextBlack89(BuildContext context, String text,
          {int maxLines}) =>
      Text(text,
          style: Theme.of(context)
              .textTheme
              .body1
              .copyWith(color: ThemeColor.blackTextColorFF89),
          maxLines: maxLines);

  static Text body1TextYellowE6(BuildContext context, String text,
          {int maxLines}) =>
      Text(text,
          style: Theme.of(context)
              .textTheme
              .body1
              .copyWith(color: ThemeColor.yellowFFE6),
          maxLines: maxLines);

  static TextStyle captionStyle(BuildContext context,
          {Color color,
          FontWeight weight: FontWeight.normal,
          double letterSpacing = 0}) =>
      Theme.of(context).textTheme.caption.copyWith(
          color: color ?? Theme.of(context).textTheme.caption.color,
          fontWeight: weight,
          letterSpacing: letterSpacing);

  static TextStyle captionStyleWithUnderLine(BuildContext context,
          {Color color,
          FontWeight weight: FontWeight.normal,
          double letterSpacing = 0}) =>
      Theme.of(context).textTheme.caption.copyWith(
          color: color ?? Theme.of(context).textTheme.caption.color,
          fontWeight: weight,
          letterSpacing: letterSpacing,
          decoration: TextDecoration.underline,
          decorationColor: ThemeColor.primaryColor,
          decorationStyle: TextDecorationStyle.solid);

  static Text captionTextWithStyle(BuildContext context, String text,
      {TextStyle style, TextAlign textAlign = TextAlign.left, int maxLines}) {
    return Text(text,
        style: style ?? captionStyle(context),
        textAlign: textAlign,
        maxLines: maxLines);
  }

  static Text captionTextWithUnderline(BuildContext context, String text,
          {int maxLines,
          Color color,
          FontWeight weight = FontWeight.normal,
          TextAlign textAlign = TextAlign.left}) =>
      captionTextWithStyle(
        context,
        text,
        style: captionStyleWithUnderLine(context,
            color: ThemeColor.primaryColor, weight: weight),
        textAlign: textAlign,
        maxLines: maxLines,
      );

  static Text captionText(BuildContext context, String text,
          {int maxLines,
          Color color,
          FontWeight weight = FontWeight.normal,
          TextAlign textAlign = TextAlign.left}) =>
      captionTextWithStyle(context, text,
          style: captionStyle(context,
              color: color ?? Theme.of(context).textTheme.caption.color,
              weight: weight),
          textAlign: textAlign,
          maxLines: maxLines);

  static Text captionTextWithColorFF89(BuildContext context, String text,
          {int maxLines}) =>
      captionText(context, text, color: ThemeColor.blackTextColorFF89);

  static Text captionTextSemiBoldWithColorFF89(
          BuildContext context, String text, {int maxLines}) =>
      captionText(context, text,
          color: ThemeColor.blackTextColorFF89, weight: FontWeight.w600);

  static Text captionTextWithColor(BuildContext context, String text, var color,
          {int maxLines, FontWeight weight, TextAlign textAlign}) =>
      captionText(context, text,
          color: color, weight: weight, maxLines: maxLines);

  static TextStyle body1Style(BuildContext context,
          {Color color,
          FontWeight weight: FontWeight.normal,
          double letterSpacing = 0,
          TextAlign textAlign = TextAlign.left}) =>
      Theme.of(context).textTheme.body1.copyWith(
          color: color ?? Theme.of(context).textTheme.body1.color,
          fontWeight: weight,
          letterSpacing: letterSpacing);

  ///  text style
  static TextStyle checkboxTextStyle(context, bool isSelected) =>
      Theme.of(context).textTheme.subtitle.copyWith(
          letterSpacing: 0.0,
          fontWeight: isSelected ? FontWeight.w500 : FontWeight.w400,
          color: isSelected ? Color(0xff262a2e) : Color(0xff3b3d43));

  static TextStyle defaultTextFieldStyle(context) {
    return Theme.of(context).textTheme.body1.copyWith(
        letterSpacing: 0.0,
        fontWeight: FontWeight.normal,
        color: ThemeColor.textLightBlackColor);
  }

//  static TextStyle sendMessageTextStyle(context) {
//    return TextStyle(color: ThemeColor.textHintGrey,
//      fontFamily: T);
//  }

  static InputDecoration defaultTextFieldDecorationWithIcons(
      context, String hintText,
      {Icon prefixIcon, Widget suffixIcon}) {
    return InputDecoration(
        contentPadding: EdgeInsets.all(0.0),
        prefixIcon: prefixIcon ?? Icon(null),
        suffixIcon: suffixIcon ?? Icon(null),
        hintText: hintText,
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0x51000000), width: 1.0),
        ));
  }

  static InputDecoration defaultTextFieldDecoration(
    context,
    String hintText,
  ) {
    return InputDecoration(
        contentPadding: EdgeInsets.all(ThemeDimension.paddingSmall),
        hintText: hintText,
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0x51000000), width: 1.0),
        ));
  }

  static InputDecoration defaultTextFieldDecoration2(context, String hintText) {
    return InputDecoration(
        hintText: hintText,
        contentPadding: EdgeInsets.all(ThemeDimension.paddingVerySmall));
  }

  ///register screen
  static InputDecoration textFieldInputDecoration(context, {String hintText}) {
    return InputDecoration(
      hintText: hintText ?? "",
      hintStyle: Theme.of(context)
          .textTheme
          .subhead
          .copyWith(color: ThemeColor.textHintGrey),
      border: InputBorder.none,
      contentPadding: EdgeInsets.all(ThemeDimension.paddingMedium),
    );
  }

  static InputDecoration defaultTextFieldDecorationWithCaptionSizedHint(
    context,
    String hintText,
  ) {
    return InputDecoration(
        contentPadding: EdgeInsets.all(ThemeDimension.paddingSmall),
        hintText: hintText,
        hintStyle: Theme.of(context).textTheme.caption,
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0x51000000), width: 1.0),
        ));
  }

  static InputDecoration defaultTextFieldDecorationPadding12(
    context,
    String hintText,
  ) {
    return InputDecoration(
        fillColor: ThemeColor.lightGreyBorderColor,
        contentPadding: EdgeInsets.all(ThemeDimension.paddingSmall12),
        hintText: hintText,
        hintStyle: TextStyle(color: ThemeColor.textHintGrey),
        border: OutlineInputBorder(
            borderSide:
                BorderSide(color: ThemeColor.textBorderBlackColor, width: 1.0),
            borderRadius: BorderRadius.all(Radius.circular(10.0))));
  }

  static BoxDecoration cardContentDecoration(context,
          {Color color: ThemeColor.whiteColor,
          BorderRadius borderRadius = const BorderRadius.all(
              Radius.circular(ThemeDimension.borderRadius10))}) =>
      BoxDecoration(
          color: color,
          borderRadius:
              BorderRadius.all(Radius.circular(ThemeDimension.borderRadius10)));

  static Text buttonText(BuildContext context, String text, {int maxLines}) =>
      Text(text, style: Theme.of(context).textTheme.button, maxLines: maxLines);

  static Text buttonTextWithPrimaryColorWeightSemiBold(
          BuildContext context, String text, {int maxLines}) =>
      Text(text,
          style: Theme.of(context).textTheme.button.copyWith(
              color: ThemeColor.primaryColor, fontWeight: FontWeight.w600),
          maxLines: maxLines);

  static Text buttonTextWithColorAlignmentCenter(
          BuildContext context, String text, {int maxLines}) =>
      Text(text,
          textAlign: TextAlign.center,
          style: Theme.of(context)
              .textTheme
              .button
              .copyWith(color: ThemeColor.blackTextColorFF31),
          maxLines: maxLines);

  static Text buttonTextPrimary(BuildContext context, String text,
          {int maxLines}) =>
      Text(text,
          style: Theme.of(context)
              .textTheme
              .button
              .copyWith(color: ThemeColor.primaryColor),
          maxLines: maxLines);

  static Text buttonTextWithColor(
          BuildContext context, String text, Color color, {int maxLines}) =>
      Text(text,
          style: Theme.of(context).textTheme.button.copyWith(color: color),
          maxLines: maxLines);

  static Text buttonTextWhite(BuildContext context, String text,
          {int maxLines}) =>
      Text(text,
          style:
              Theme.of(context).textTheme.button.copyWith(color: Colors.white),
          maxLines: maxLines);

  /// Dividers.......................................................................................

  static Divider veryLightHorizontalDivider({@required double height}) =>
      Divider(
        height: height,
        color: ThemeColor.greyBorderColor,
      );

  static Divider lightHorizontalDivider(
          {double height, Color color = ThemeColor.textFieldBorderColor}) =>
      Divider(height: height, color: color);

  static Divider defaultHorizontalDivider(
          {double height = 1, Color color = ThemeColor.dividerColorDefault}) =>
      Divider(height: height, color: color);

  static Divider darkHorizontalDivider({double height = 1}) =>
      Divider(height: height, color: ThemeColor.dividerColorDark);

  static VerticalDivider lightVerticalDivider({double width = 1}) =>
      VerticalDivider(width: width, color: ThemeColor.dividerColorLight);

  static VerticalDivider defaultVerticalDivider({double width = 1}) =>
      VerticalDivider(width: width, color: ThemeColor.dividerColorDefault);

  static VerticalDivider darkVerticalDivider({double width = 1}) =>
      VerticalDivider(width: width, color: ThemeColor.dividerColorDark);

  /// icons
  static Icon primaryColorIcon(context, IconData data) =>
      Icon(data, color: Theme.of(context).primaryColor);

  /// App bar
  static Container appBarDivider(context,
          {double height = ThemeDimension.appBarDividerHeight}) =>
      Container(
          width: double.maxFinite,
          height: height,
          color: Theme.of(context).accentColor);

  // appBar Bottom Divider
  static PreferredSize appBarBottomDivider(
          {double height = 2.0,
          Color borderColor = ThemeColor.orangeBoarder}) =>
      PreferredSize(
          child: Container(
            color: ThemeColor.orangeBoarder,
            height: height,
          ),
          preferredSize: Size.fromHeight(height));

  static Text appBarTitle(context, String title) =>
      ThemeHelper.subHeadingTextWithColor(context, (title ?? ""), Colors.white,
          isBold: true);

  static Widget appBarTitleSubtitle(context, String title, String subtitle) =>
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          ThemeHelper.titleText(context, title),
          ThemeHelper.subTitleTextFontWeight400(context, subtitle)
        ],
      );

  static Widget appBarLogo(context, String logo) => Image.asset(
        logo,
        height: ThemeDimension.appBarLogoHeight,
      );

  static PreferredSize appBarBottomDividerWidget(context) => PreferredSize(
      child: appBarDivider(context),
      preferredSize:
          Size(double.maxFinite, ThemeDimension.appBarDividerHeight));

  /// App bar
  static AppBar transparentAppBar(BuildContext context, {String title}) =>
      AppBar(
        title: appBarTitle(context, title),
        backgroundColor: Colors.transparent,
        elevation: 0.0, //Shadow gone
      );

  static AppBar defaultAppBar(BuildContext context,
          {Widget title,
          List<Widget> actions,
          Color backgroundColor = ThemeColor.primaryColor,
          bool centerTitle = false,
          Widget leading,
          bool roundedCorner = true,
          double elevation = ThemeDimension.appBarElevation,
          Widget bottom}) =>
      AppBar(
        leading: leading,
        brightness: Brightness.light,
        textTheme: Theme.of(context).textTheme,
        title: title,
        backgroundColor: backgroundColor,
        elevation: elevation,
        actions: actions,
        centerTitle: centerTitle,
        bottom: bottom,
        shape: roundedCorner
            ? RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.circular(25),
                ),
              )
            : null,
        automaticallyImplyLeading: true,
        //Shadow gone
      );

  static AppBar appBarWithTitle(BuildContext context,
          {String title = "",
          List<Widget> actions,
          Color backgroundColor = ThemeColor.primaryColor,
          bool centerTitle,
          Widget leading,
          double elevation,
          Widget bottom}) =>
      defaultAppBar(context,
          title: appBarTitle(context, title),
          backgroundColor: backgroundColor,
          leading: leading,
          elevation: elevation,
          actions: actions,
          centerTitle: centerTitle,
          bottom: bottom);

  // AppBar with  logo
  static AppBar appBarWithLogo(BuildContext context,
          {String logo,
          List<Widget> actions,
          Color backgroundColor = ThemeColor.primaryColor,
          bool centerTitle,
          Widget leading,
          double elevation,
          Widget bottom}) =>
      defaultAppBar(context,
          title: appBarLogo(context, logo),
          backgroundColor: backgroundColor,
          leading: leading,
          elevation: elevation,
          actions: actions,
          centerTitle: centerTitle,
          bottom: bottom);

//
  /// app bar action / icon
  static Widget getAppbarAction(Widget icon, VoidCallback callback,
          {String tooltip = ""}) =>
      Container(
        height: 40,
        width: 40,
        alignment: Alignment.center,
        margin:
            EdgeInsets.symmetric(horizontal: ThemeDimension.paddingVerySmall),
        child: Tooltip(
          decoration: BoxDecoration(color: ThemeColor.whiteColor),
          message: tooltip,
          child: InkWell(
            borderRadius: BorderRadius.circular(100),
            onTap: () => callback(),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: icon,
            ),
          ),
        ),
      );

  /// Radio with titles......................................
  static Widget radioButtonWithWidget(Widget radio, Widget widget) =>
      Row(children: [radio, ThemeHelper.paddingLeft(), widget]);

  static Widget radioButtonWithWidgetOnRightSide(Widget radio, Widget widget) =>
      Row(children: [
        Expanded(child: widget),
        ThemeHelper.paddingLeft(),
        radio
      ]);

  static Widget radioButtonWithTitle(context, Widget radio, String title,
          {Color titleColor}) =>
      Row(children: [
        radio,
        ThemeHelper.paddingLeft(),
        ThemeHelper.subTitleTextWithColor(context, title, color: titleColor)
      ]);

  static Widget radioButtonWithTitleColorFF3150(
          context, Widget radio, String title) =>
      Row(children: [
        radio,
        ThemeHelper.paddingLeft(),
        ThemeHelper.subTitleTextWithColorFF31WeightMedium(context, title)
      ]);

  /// checkbox with titles
  static Widget checkboxWithWidget(Widget radio, Widget widget) =>
      Row(children: [radio, ThemeHelper.paddingLeft(), widget]);

  /// box shadows....

  static BoxDecoration defaultBoxShadow() =>
      BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            color: Color(0x0e000000),
            offset: Offset(0, 2),
            blurRadius: 2,
            spreadRadius: 0)
      ]);

  /// input containers and fields
  static Widget inputContainer(Widget child,
          {padding = const EdgeInsets.symmetric(
              horizontal: ThemeDimension.paddingMedium, vertical: 6)}) =>
      Container(
        padding: padding,
        child: child,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(ThemeDimension.borderRadius6),
            border: Border.all(width: 1)),
      );

  static Widget inputDropdown(
          hint, value, List<DropdownMenuItem> items, onChanged,
          {bool isExpanded = true}) =>
      InputDecorator(
        decoration: defaultInputDecorationDropdown((value != null) ? hint : ""),
        child: DropdownButton(
          isExpanded: isExpanded,
          isDense: true,
          value: value,
          underline: Container(),
          items: items,
          onChanged: onChanged,
          hint: Text(hint),
        ),
      );

  static Decoration getDefaultGreyBorderDecoration() {
    return new BoxDecoration(
        color: Color(0xffffffff),
        border: Border.all(color: Color(0x59484460), width: 1),
        borderRadius: BorderRadius.circular(3));
  }

  static Decoration getDefaultDecoration(
      {Color backgroundColor, Color borderColor}) {
    return new BoxDecoration(
        color: backgroundColor,
        border: Border.all(color: borderColor, width: 1),
        borderRadius: BorderRadius.circular(10));
  }

  static Decoration getDefaultDecorationWithPadding15(
      {Color backgroundColor, Color borderColor}) {
    return new BoxDecoration(
        color: backgroundColor,
        border: Border.all(color: borderColor, width: 1),
        borderRadius: BorderRadius.circular(15));
  }

  static Decoration getBorderDecorationWithColor(
      var solidColor, var borderColor,
      {double borderRadius: 3}) {
    return new BoxDecoration(
        color: solidColor,
        border: Border.all(color: borderColor, width: 1),
        borderRadius: BorderRadius.circular(borderRadius));
  }

  static Decoration getBorderDecoration(var solidColor, var borderColor,
      double borderWidth, double cornerRadius) {
    return new BoxDecoration(
        color: solidColor,
        border: Border.all(color: borderColor, width: borderWidth),
        borderRadius: BorderRadius.circular(cornerRadius));
  }

  static Decoration getBorderDecorationWithDropDown() {
    return new BoxDecoration(
        border: Border.all(color: Color(0x51000000)),
        borderRadius: BorderRadius.circular(10.0));
  }

  static InputDecoration defaultInputDecorationDropdown(String label) =>
      InputDecoration(
        labelText: label,
        border: OutlineInputBorder(),
      );

  static InputDecoration defaultInputDecoration(String label) =>
      InputDecoration(
        hintText: label,
        labelText: label,
        border: OutlineInputBorder(),
        focusedBorder: OutlineInputBorder(),
      );

  /// icons
  static Widget defaultPlaceHolderIcon(BuildContext context,
          {double radius = ThemeDimension.avatarRadiusDefault}) =>
      CircleAvatar(
        child: Icon(
          Icons.account_balance,
          size: ThemeDimension.avatarRadiusDefault * 2,
          color: Colors.black87,
        ),
        radius: radius,
        backgroundColor: Colors.white,
      );

  static Widget defaultPlaceHolderAvatar(BuildContext context,
          {double radius = ThemeDimension.avatarRadiusDefault,
          String initial}) =>
      CircleAvatar(
        child: Text(initial.toUpperCase(),
            style: Theme.of(context).primaryTextTheme.button),
        radius: radius,
        backgroundColor: Colors.black54,
      );

  static Widget drawerMenuItems(
          BuildContext context, String text, VoidCallback onTap) =>
      ListTile(
          title: ThemeHelper.subTitleTextWithColor(context, text,
              color: ThemeColor.blackTextColorFF89),
          onTap: onTap);

  static Widget getAvatarWithBorder(BuildContext context,
          {double radius = ThemeDimension.avatarRadiusDefault,
          String initial,
          String imageUrl,
          var borderColor: Colors.black54,
          var filledColor: Colors.black54,
          double borderRadius: ThemeDimension.paddingTiny}) =>
      CircleAvatar(
        child: StringUtils.isValidString(imageUrl)
            ? Container(
                height: radius * 2,
                width: radius * 2,
                margin: EdgeInsets.all(borderRadius),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.cover, image: NetworkImage(imageUrl))))
            : Container(
                height: radius * 2,
                width: radius * 2,
                margin: EdgeInsets.all(borderRadius),
                child: CircleAvatar(
                    backgroundColor: filledColor,
                    child: Text(initial ?? "s".toUpperCase(),
                        style: Theme.of(context).primaryTextTheme.button))),
        radius: radius,
        backgroundColor: borderColor,
      );

  static Widget getAvatarWithGradientBorder(
          BuildContext context, var startColor, var endColor,
          {double radius = ThemeDimension.avatarRadiusDefault,
          String initial,
          String imageUrl,
          var filledColor: Colors.black54,
          double borderRadius: ThemeDimension.paddingTiny}) =>
      Container(
        height: radius * 2,
        width: radius * 2,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          // Box decoration takes a gradient
          gradient: LinearGradient(
            // Where the linear gradient begins and ends
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              startColor,
              endColor,
            ],
          ),
        ),
        child: StringUtils.isValidString(imageUrl)
            ? Container(
                height: radius * 2,
                width: radius * 2,
                margin: EdgeInsets.all(borderRadius),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.cover, image: NetworkImage(imageUrl))))
            : Container(
                height: radius * 2,
                width: radius * 2,
                margin: EdgeInsets.all(borderRadius),
                child: CircleAvatar(
                    backgroundColor: filledColor,
                    child: Text(initial ?? "s".toUpperCase(),
                        style: Theme.of(context).primaryTextTheme.button))),
      );

  ///bottom sheets
  static RoundedRectangleBorder getDefaultBottomSheetShape() {
    return RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft:
                Radius.circular(ThemeDimension.defaultBottomSheetBorderRadius),
            topRight: Radius.circular(
                ThemeDimension.defaultBottomSheetBorderRadius)));
  }

  static TextStyle buttonTextStyle(BuildContext context, Color color) =>
      Theme.of(context)
          .textTheme
          .button
          .copyWith(color: ThemeColor.primaryColor);

  /// by default raised button with primary color
  static Widget defaultRaisedButton(
          BuildContext context, String text, VoidCallback onPressed,
          {Color color = ThemeColor.blueButtonColor,
          bool isBorderSide = false,
          EdgeInsets padding = const EdgeInsets.symmetric(
              horizontal: ThemeDimension.paddingLarge25,
              vertical: ThemeDimension.paddingSmall12),
          RoundedRectangleBorder shape,
          double elevation = ThemeDimension.buttonDefaultElevation,
          TextStyle textStyle,
          double minWidth: 0}) =>
      ButtonTheme(
        minWidth: minWidth,
        child: RaisedButton(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          onPressed: onPressed,
          child: Padding(
            padding: padding,
            child: Text(
              text,
              style: textStyle ??
                  Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: Colors.white),
            ),
          ),
          shape: shape ??
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(ThemeDimension.buttonBorderRadius24),
                  ),
                  side: isBorderSide
                      ? BorderSide(width: 1, color: ThemeColor.primaryColor)
                      : BorderSide.none),
          color: color ?? Theme.of(context).primaryColor,
          elevation: elevation,
        ),
      );

  static Widget getCardWidget(BuildContext context, Widget content,
          {double marginTop = 0.0,
          double marginLeft = 0.0,
          double marginRight = 0.0,
          double marginBottom = 0.0,
          double cardBorderRadius = ThemeDimension.defaultCardCornerRadius}) =>
      Container(
        margin: EdgeInsets.only(
            top: marginTop,
            left: marginLeft,
            right: marginRight,
            bottom: marginBottom),
        child: Card(
          elevation: ThemeDimension.defaultCardElevation,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(cardBorderRadius),
          ),
          child: content,
        ),
      );

  ///Card without default margin
  static Widget getCardWidgetTwo(BuildContext context, Widget content,
          {EdgeInsets padding = const EdgeInsets.only(),
          BorderRadius borderRadius = const BorderRadius.all(
              Radius.circular(ThemeDimension.defaultCardCornerRadius)),
          Color color = Colors.white,
          EdgeInsets margin =
              const EdgeInsets.all(ThemeDimension.defaultCardMargin)}) =>
      Container(
        margin: margin,
        padding: padding,
        child: Card(
          margin: EdgeInsets.only(),
          color: color,
          elevation: ThemeDimension.defaultCardElevation,
          shape: RoundedRectangleBorder(
            borderRadius: borderRadius,
          ),
          child: content,
        ),
      );

  static TextStyle textFormFieldStyle(BuildContext context) {
    return Theme.of(context).textTheme.subtitle.copyWith(
        fontWeight: FontWeight.w600, color: ThemeColor.blackTextColorFF31);
  }

  static TextStyle textFormFieldStyleColorFF89(BuildContext context) {
    return Theme.of(context).textTheme.caption.copyWith(
        fontWeight: FontWeight.normal, color: ThemeColor.blackTextColorFF89);
  }

  static TextStyle tabLabelStyle(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .subhead
        .copyWith(color: ThemeColor.primaryColor);
  }

  static TextStyle tabLabelCaptionTextStyle(BuildContext context) {
    return Theme.of(context)
        .textTheme
        .caption
        .copyWith(color: ThemeColor.primaryColor);
  }

  static OutlineInputBorder textFormFieldBorder() {
    return OutlineInputBorder(
        borderRadius: BorderRadius.all(
            Radius.circular(ThemeDimension.textFieldBorderRadius)),
        borderSide:
            BorderSide(color: ThemeColor.textFieldBorderColor, width: 1));
  }

  static Widget getStackForPrimaryBackground(
          BuildContext context, Widget content,
          {double height =
              ThemeDimension.backgroundCurtainHeightWithoutAppBar}) =>
      Stack(
        children: <Widget>[
          getBackGroundCurtain(context, height: height),
          content,
        ],
      );

  static Widget getBackGroundCurtain(BuildContext context,
          {double height =
              ThemeDimension.backgroundCurtainHeightWithoutAppBar}) =>
      Container(
        height: height,
        decoration: BoxDecoration(
          color: ThemeColor.primaryColor,
          borderRadius: BorderRadius.vertical(
              bottom:
                  Radius.elliptical(MediaQuery.of(context).size.width, 70.0)),
        ),
      );

  static Widget verticalDottedLine(dottedLIneLength) {
    return RotatedBox(
        quarterTurns: 1, child: dottedLineWidget(dottedLIneLength));
  }

  static Widget dottedLineWidget(dottedLineLength) {
    var _totalWidth = dottedLineLength;
    var _emptyWidth = ThemeDimension.dottedLineEmptyWidth;
    double _dashWidth = ThemeDimension.dottedLineDashWidth;
    double _dashHeight = ThemeDimension.dottedLineDashHeight;
    var _dashColor = ThemeColor.primaryColor;
    return Row(
      children: List.generate(
        _totalWidth ~/ (_dashWidth + _emptyWidth),
        (_) => Container(
          width: _dashWidth,
          height: _dashHeight,
          color: _dashColor,
          margin:
              EdgeInsets.only(left: _emptyWidth / 2, right: _emptyWidth / 2),
        ),
      ),
    );
  }

  static Widget circularBorderText(BuildContext context, String text) {
    return Container(
        padding: const EdgeInsets.only(
            left: ThemeDimension.paddingSmall6,
            right: ThemeDimension.paddingSmall6,
            top: ThemeDimension.paddingTiny,
            bottom: ThemeDimension.paddingTiny),
        decoration: new BoxDecoration(
            border: new Border.all(color: ThemeColor.occupationBorderColor),
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(ThemeDimension.avatarRadiusMedium25),
                topLeft: Radius.circular(ThemeDimension.avatarRadiusMedium25),
                bottomLeft:
                    Radius.circular(ThemeDimension.avatarRadiusMedium25),
                bottomRight:
                    Radius.circular(ThemeDimension.avatarRadiusMedium25))),
        child: ThemeHelper.captionText(context, text, weight: FontWeight.bold));
  }

  static Widget circularBorderTextTwo(context, String s) {
    return Container(
        padding: const EdgeInsets.only(
            left: ThemeDimension.paddingSmall6,
            right: ThemeDimension.paddingSmall6,
            top: ThemeDimension.paddingTiny,
            bottom: ThemeDimension.paddingTiny),
        decoration: new BoxDecoration(
            border: new Border.all(color: ThemeColor.occupationBorderColor),
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(ThemeDimension.avatarRadiusMedium25),
                topLeft: Radius.circular(ThemeDimension.avatarRadiusMedium25),
                bottomLeft:
                    Radius.circular(ThemeDimension.avatarRadiusMedium25),
                bottomRight:
                    Radius.circular(ThemeDimension.avatarRadiusMedium25))),
        child: ThemeHelper.captionText(context, s));
  }

  static Decoration selectedDateCalendarDecoration() {
    return ThemeHelper.getDefaultDecoration(
        borderColor: ThemeColor.primaryColor,
        backgroundColor: ThemeColor.backgroundColorPrimaryLight);
  }

  static InputDecoration inputDecoration({hint}) {
    return InputDecoration(
        contentPadding: EdgeInsets.symmetric(
            vertical: ThemeDimension.paddingSmall10,
            horizontal: ThemeDimension.paddingSmall10),
        labelStyle: TextStyle(),
        labelText: hint ?? "",
        border: ThemeHelper.textFormFieldBorder());
  }

  static Decoration tabsDecoration() {
    return BoxDecoration(
        color: ThemeColor.primaryColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(ThemeDimension.borderRadius28),
          topRight: Radius.circular(ThemeDimension.borderRadius28),
        ));
  }

  //  Qoka Vendor App
// Register Form TextFormField
  static Widget textFormField(context, String hintText,
      {TextAlign textAlign: TextAlign.center}) {
    return TextFormField(
      textAlign: textAlign,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: Theme.of(context).textTheme.subhead,
        border: InputBorder.none,
        contentPadding: EdgeInsets.all(ThemeDimension.paddingMedium),
      ),
    );
  }

  ///underline
  static Widget textUnderline(context,
      {double height = 1,
      color = ThemeColor.textHintGrey,
      double width = 180}) {
    return Container(
      alignment: Alignment.centerLeft,
      width: width,
      height: height,
      color: color,
    );
  }

  ///
  static showCustomDialog(BuildContext context, Widget content) => showDialog(
      context: context,
      builder: (BuildContext context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(ThemeDimension.borderRadiusLarge20),
            ),
            elevation: ThemeDimension.defaultCardElevation,
            child: content,
          ));

  static showCustomDialogTwo(BuildContext context, String title, Widget content,
          String buttonText, VoidCallback onPressed,
          {double height = ThemeDimension.defaultDialogHeight,
          bool isCancelButtonVisible = false,
          String titleImagePath = ""}) =>
      showDialog(
          context: context,
          builder: (BuildContext context) => Dialog(
                shape: RoundedRectangleBorder(
                  borderRadius:
                      BorderRadius.circular(ThemeDimension.borderRadius10),
                ),
                elevation: ThemeDimension.defaultCardElevation,
                child: Container(
                  height: height,
                  child: Column(
                    children: <Widget>[
                      Container(
                        decoration: BoxDecoration(
                            color: ThemeColor.blueE2,
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(
                                    ThemeDimension.borderRadius10),
                                topRight: Radius.circular(
                                    ThemeDimension.borderRadius10))),
                        padding: EdgeInsets.all(ThemeDimension.paddingSmall12),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            titleImagePath != ""
                                ? Image.asset(
                                    titleImagePath,
                                    width: ThemeDimension.iconSizeMedium24,
                                    height: ThemeDimension.iconSizeMedium24,
                                  )
                                : Container(
                                    width: ThemeDimension.iconSizeMedium24,
                                    height: ThemeDimension.iconSizeMedium24,
                                  ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: ThemeDimension.paddingSmall),
                                child: ThemeHelper.titleTextWithColor(
                                    context, title, ThemeColor.blue25,
                                    textAlign: TextAlign.center, isBold: true),
                              ),
                            ),
                            InkWell(
                              child: Icon(Icons.cancel,
                                  color: ThemeColor.whiteColor),
                              onTap: () {
                                Navigator.of(context).pop();
                              },
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Scrollbar(
                          child: ListView(
                            physics: ClampingScrollPhysics(),
                            shrinkWrap: true,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(
                                    ThemeDimension.paddingMedium),
                                child: content,
                              )
                            ],
                          ),
                        ),
                      ),
                      isCancelButtonVisible
                          ? ThemeHelper.defaultRaisedButton(context, "Cancel",
                              () {
                              Navigator.of(context).pop();
                            },
                              minWidth: double.maxFinite,
                              elevation: 0,
                              color: /*ThemeColor.whiteColor*/ ThemeColor
                                  .blueE2,
                              textStyle:
                                  TextStyle(color: ThemeColor.textHintGrey),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(
                                          ThemeDimension.valueZero),
                                      topRight: Radius.circular(
                                          ThemeDimension.valueZero),
                                      bottomLeft: Radius.circular(ThemeDimension
                                          . /*borderRadius10*/ valueZero),
                                      bottomRight: Radius.circular(
                                          ThemeDimension
                                              . /*borderRadius10*/ valueZero))))
                          : Container(),
                      ThemeHelper.defaultRaisedButton(
                          context, buttonText, onPressed,
                          minWidth: double.maxFinite,
                          elevation: 0,
                          color: ThemeColor.blue2F,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  topLeft:
                                      Radius.circular(ThemeDimension.valueZero),
                                  topRight:
                                      Radius.circular(ThemeDimension.valueZero),
                                  bottomLeft: Radius.circular(
                                      ThemeDimension.borderRadius10),
                                  bottomRight: Radius.circular(
                                      ThemeDimension.borderRadius10))))
                    ],
                  ),
                ),
              ));

  static searchBoxDecoration(BuildContext context) {
    return BoxDecoration(
        color: ThemeColor.whiteColor,
        border: Border.all(
            width: ThemeDimension.valueOne, color: ThemeColor.textHintGrey),
        borderRadius: BorderRadius.all(
            Radius.circular(ThemeDimension.borderRadiusLarge24)));
  }

  static bottomSheetBlueButtonBoxDecoration(BuildContext context) {
    return BoxDecoration(
        color: ThemeColor.blue2F,
        borderRadius: BorderRadius.all(
            Radius.circular(ThemeDimension.borderRadiusLarge24)));
  }

  static safeAreaWidget({Widget widget}) {
    return SafeArea(
      child: widget ?? Container(),
    );
  }

  static BoxDecoration boxDecoration() {
    return BoxDecoration(
        color: ThemeColor.whiteColor,
        border: Border.all(color: ThemeColor.buttonBorderColorFFC7, width: 1.2),
        borderRadius: BorderRadius.circular(10));
  }

  static Widget textWithLoaderHorizontal(String text) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(text),
          SizedBox(
              height: 20,
              width: 20,
              child: CircularProgressIndicator(strokeWidth: 2)),
        ],
      );

  static void showPickerDialog(
      BuildContext context, Function onCameraTap, Function onGalleryTap) {
    AppLocalizations localize = AppLocalizations.of(context);

    Dialog imagePickerDialog = ThemeHelper.showCustomDialogTwo(
        context,
        localize.imagePicker,
        Wrap(
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(ThemeDimension.paddingSmall15),
                    child: InkWell(
                      onTap: onCameraTap,
                      child: ThemeHelper.body2TextWithColor(
                          context, localize.pickImageFromCamera,
                          color: ThemeColor.primaryColor),
                    ),
                  ),
                  Divider(),
                  InkWell(
                    onTap: onGalleryTap,
                    child: Padding(
                      padding: EdgeInsets.all(ThemeDimension.paddingSmall15),
                      child: ThemeHelper.body2TextWithColor(
                          context, localize.pickImageFromGallery,
                          color: ThemeColor.primaryColor),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        localize.cancel, () {
      Navigator.of(context).pop();
    }, height: 230);

    showDialog(
        context: context,
        builder: (BuildContext context) => Wrap(
              children: <Widget>[imagePickerDialog],
            ));
  }

  static showWrappedCustomDialogTwo(BuildContext context, String title,
          Widget content, String buttonText, VoidCallback onPressed,
          {double height = ThemeDimension.defaultDialogHeight,
          bool isCancelButtonVisible = false,
          String titleImagePath = ""}) =>
      showDialog(
          context: context,
          builder: (BuildContext context) => Center(
                child: Wrap(
                  children: <Widget>[
                    Dialog(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(
                            ThemeDimension.borderRadius10),
                      ),
                      elevation: ThemeDimension.defaultCardElevation,
                      child: Container(
                        height: height,
                        child: Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: ThemeColor.blueE2,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(
                                          ThemeDimension.borderRadius10),
                                      topRight: Radius.circular(
                                          ThemeDimension.borderRadius10))),
                              padding:
                                  EdgeInsets.all(ThemeDimension.paddingSmall12),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  titleImagePath != ""
                                      ? Image.asset(
                                          titleImagePath,
                                          width:
                                              ThemeDimension.iconSizeMedium24,
                                          height:
                                              ThemeDimension.iconSizeMedium24,
                                        )
                                      : Container(
                                          width:
                                              ThemeDimension.iconSizeMedium24,
                                          height:
                                              ThemeDimension.iconSizeMedium24,
                                        ),
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(
                                          horizontal:
                                              ThemeDimension.paddingSmall),
                                      child: ThemeHelper.titleTextWithColor(
                                          context, title, ThemeColor.blue25,
                                          textAlign: TextAlign.center,
                                          isBold: true),
                                    ),
                                  ),
                                  InkWell(
                                    child: Icon(Icons.cancel,
                                        color: ThemeColor.whiteColor),
                                    onTap: () {
                                      Navigator.of(context).pop();
                                    },
                                  )
                                ],
                              ),
                            ),
                            ListView(
                              physics: ClampingScrollPhysics(),
                              shrinkWrap: true,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(
                                      ThemeDimension.paddingMedium),
                                  child: content,
                                )
                              ],
                            ),
                            isCancelButtonVisible
                                ? ThemeHelper.defaultRaisedButton(
                                    context, "Cancel", () {
                                    Navigator.of(context).pop();
                                  },
                                    minWidth: double.maxFinite,
                                    elevation: 0,
                                    color: ThemeColor.blueE2,
                                    textStyle: TextStyle(
                                        color: ThemeColor.textHintGrey),
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(
                                                ThemeDimension.valueZero),
                                            topRight: Radius.circular(
                                                ThemeDimension.valueZero),
                                            bottomLeft: Radius.circular(
                                                ThemeDimension
                                                    . /*borderRadius10*/ valueZero),
                                            bottomRight: Radius.circular(
                                                ThemeDimension
                                                    . /*borderRadius10*/ valueZero))))
                                : Container(),
                            ThemeHelper.defaultRaisedButton(
                                context, buttonText, onPressed,
                                minWidth: double.maxFinite,
                                elevation: 0,
                                color: ThemeColor.blue2F,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(
                                            ThemeDimension.valueZero),
                                        topRight: Radius.circular(
                                            ThemeDimension.valueZero),
                                        bottomLeft: Radius.circular(
                                            ThemeDimension.borderRadius10),
                                        bottomRight: Radius.circular(
                                            ThemeDimension.borderRadius10))))
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ));

  static Widget networkImage(context, item) {
    return FadeInImage.assetNetwork(
      placeholder: AppImages.loadingGif,
      placeholderScale: 4,
      image: item.imageUrl,
      width: MediaQuery.of(context).size.width,
    );
  }

  static Widget getTransparentAppbar() {
    return AppBar(
      elevation: 0,
    );
  }
}

///100% — FF
///95% — F2
///90% — E6
///85% — D9
///80% — CC
///75% — BF
///70% — B3
///65% — A6
///60% — 99
///55% — 8C
///50% — 80
///45% — 73
///40% — 66
///35% — 59
///30% — 4D
///25% — 40
///20% — 33
///15% — 26
///10% — 1A
///5% — 0D
///0% — 00
