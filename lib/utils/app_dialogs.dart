import 'package:flutter/material.dart';
import 'package:woof_fini/utils/app_localizations.dart';
import 'package:woof_fini/utils/theme_helper.dart';

class AppDialogs {
  /// dialogs
  static AlertDialog getCancelAlertDialog(
          context, String title, Widget content) =>
      AlertDialog(
        title: ThemeHelper.heading6Text(context, title),
        content: content,
        contentPadding: const EdgeInsets.fromLTRB(24.0, 32.0, 24.0, 0),
        actions: <Widget>[
          FlatButton(
              color: Colors.white,
              onPressed: () => Navigator.of(context).pop(),
              child: ThemeHelper.body2TextPrimaryColor(
                  context, AppLocalizations.of(context).cancel.toUpperCase()))
        ],
      );
}
