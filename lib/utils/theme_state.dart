import 'package:flutter/material.dart';

class ThemeState with ChangeNotifier {
  Brightness mySchoolBright = Brightness.light;
  Brightness mySchoolDark = Brightness.dark;

  Brightness _brightness;
  bool isDark = false;

  ThemeState() {
    _brightness = mySchoolBright;
  }

  Brightness getBrightness() => _brightness;

  setBrightness() {
    if (!isDark) {
      _brightness = mySchoolDark;
    } else {
      _brightness = mySchoolBright;
    }

    isDark = !isDark;
    notifyListeners();
  }
}
