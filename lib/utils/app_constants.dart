

import 'package:woof_fini/utils/constants.dart';
import 'package:woof_fini/utils/event.dart';
import 'package:intl/intl.dart';


class UserLoginProgress {
  static final int userUnauthenticated = 111;
  static final int userAuthenticated = 222;
  static final int userNumberRegistered = 333;
  static final int userDetailFilled = 444;
  static final int userSkippedLogin = 555;
}

class ResponseStatusCode {
  static final int success = 1;
  static final int fail = 2;
}

class VisitorStatus {
  static const inLine = "in_line";
  static const String served = "served";
  static const String deleted = "deleted";
}

class ErrorMessages {
  static final String somethingWentWrong = "Something went wrong.";
}

class AppConstants extends Constants {
  static String verificationFaile =
      ""; // temporary number for verification failed
  static bool isAddressSaved = false;
  static String userIdFirebaseKey = "userIdFirebaseKey";
  static String isUserDetailsAdded = "isUserDetailsAdded";
  static String isClientLoggedin = "isClientLoggedin";
  static String isMobileNumberVerified = "isMobileNumberVerified";
  static String isAdminLoggedIn = "isAdminLoggedIn";
  static String address = "";

  // country code...
  static String countryCodeIndia = "91";
  static String countryCodeNorway = "47";
  static String countryCodeUK = "44";

  static String statusTypeImage = "image";
  static String statusTypeText = "text";
  static var showAutoBookButton = true;
  static var showAutoBookButtonForProvider = true;
  static var showHireAgainOptions = false;

  static final int dbVersion = 2;
  static final String dbName = "maid-me";
  static final DateFormat defaultDisplayDateFormat = DateFormat.yMMMEd();
  static final translateFilePath = "assets/lang/";

  // Qoka Vendor

  static const int splashDelay = 3;
  static const int timeMillisecondDefault = 500;
  static const double itemOpacityVisible = 1.0;
  static const double itemOpacityInvisible = 0.0;

  /// Google api keys
  static const String googleApiKeyAndroid =
      "AIzaSyAU5qdaxDBIczeF-R6BviJtUU61Fi_QVos";
  static const String googleApiKeyIos =
      "AIzaSyAU5qdaxDBIczeF-R6BviJtUU61Fi_QVos";


  ///ScreenTyp
  static const int selectOfferScreenType = 21;
  static const int inQueuePageType = 22;
  static const int servedPageType = 23;
  static const int addStaffMemberScreenType = 24;
  static const int registerScreenType = 25;
  static const int settingSmsScreenType = 26;
  static const int settingEmailScreenType = 28;
  static const int logInScreenType = 29;

  ///MyStatus Dialog Type
  static const int editStatusDialogType = 101;
  static const int editQueueListDialogType = 102;
  static const int addQueueListDialogType = 103;
  static const int newStatusDialogType = 104;
  static const int editVisitorDialogType = 105;

  ///Business Message Screen
  static const int confirmTabType = 401;
  static const int nextTabType = 402;
  static const int alertTabType = 403;
  static const int servedTabType = 404;

  ///OfferScreen
  static const String statusDeleted = "deleted";
  static const String statusInline = "inline";
  static const String statusInQueue = "inQueue";
  static const String statusServed = "served";
  static const String visitorStatusHigh = "high";
  static const String visitorStatusMedium = "medium";
  static const String visitorStatusLow = "low";

  ///businessDetailList
  static const String businessSaloonType = "Salon";
  static const String businessRestaurantsType = "Restaurants";
  static const String businessHospitalsType = "Hospitals";

  ///AddStaffMemberScreen
  static List<bool> permissionBoolLIst = [false, false, false];

  ///Convert double to int
  static int convertDoubleToInt(ranks) {
    double multiplier = .5;
    return (multiplier * ranks).round();
  }

  /// Gradient color HashMap

}

class AppColumn extends Column {
  static const columnStartAt = "s_at";
}

class AppKey {
  static const male = "male";
  static const female = "female";
  static const provider = "provider";
  static const client = "client";
  static const admin = "admin";
}

class AppEventType extends EventType {
  //  start from 10
  static const int itemLike = 11;
  static const int itemUnLike = 12;
  static const int itemPhone = 13;
  static const int itemSms = 14;
  static const int itemVisitorProfile = 15;
  static const int itemDelete = 16;
  static const int itemChangeOrder = 17;

  static const int fetchItemsForUser = 111;
  static const int fetchAllItemsForUser = 112;

  static const int authenticateUser = 121;
  static const int checkLoggedIn = 122;
  static const int logOut = 123;

  static const int navigate = 131;
  static const int locationUpdate = 132;
  static const int messageSettingChanged = 133;
  static const int createQueueType = 134;
  static const int editQueueType = 135;
  static const int openEditQueueTypeDialog = 136;
  static const int deleteQueue = 137;
  static const int visitorServed = 138;
  static const int createNewOffer = 139;
  static const int searchOffer = 140;
  static const int changeItemSelection = 141;
  static const int deleteOffers = 142;
  static const int openOfferDetails = 143;
  static const int offerSelected = 144;
  static const int sendOffer = 145;
}

class AppImages {
  ///images
  /// 1x - mdpi
  /// 2x - xhdpi
  /// 3x - xxhdpi

  static const String imagesPath = "assets/images/";

// qoka vendor app images path
  static const String tutorialsImageComponent = imagesPath + "component.png";
  static const String pindBalluchiProfile = imagesPath + "pindballuchi.png";
  static const String liveStatus = imagesPath + "live_status.png";
  static const String calendar = imagesPath + "calender.png";
  static const String camera = imagesPath + "camera.png";
  static const String edit = imagesPath + "edit.png";
  static const String video = imagesPath + "video.png";
  static const String logo = imagesPath + "logo.png";
  static const String infoIcon = imagesPath + "info.png";
  static const String phone = imagesPath + "phone.png";
  static const String sort = imagesPath + "sort.png";
  static const String userEdit = imagesPath + "user_edit.png";
  static const String email = imagesPath + "email.png";
  static const String delete = imagesPath + "delete.png";
  static const String iconFilter = imagesPath + "icon_filter.png";

//  static const String tutorialsImageComponent = imagesPath + "component.png";
//  static const String logo = imagesPath + "logo.png";
//  static const String infoIcon = imagesPath + "info.png";

  static const String norwayFlag = imagesPath + "norway.png";
  static const String appLogo = imagesPath + "maid_me_simple.png";
  static const String maidImage = imagesPath + "maid_image.png";

  static const String giftImage = imagesPath + "gift_box_image.png";
  static const String upgradePlansLogo = imagesPath + "pind_balluchi_logo.png";
  static const String iconEdit = imagesPath + "icon_edit.png";

  static const String iconDelete = imagesPath + "icon_delete.png";
  static const String offerSendImage = imagesPath + "offer_send_image.png";
  static const String highBusinessGrowthImage =
      imagesPath + "high_buisness_growth.png";
  static const String homeImage = imagesPath + "home_image.png";
  static const String profile_banner_image =
      imagesPath + "profile_baneer_image.png";
  static const String profileOfferImage = imagesPath + "offer_image.png";
  static const String facebookLogoImage = imagesPath + "facebook_logo.png";
  static const String facebookLogoCircle = imagesPath + "icon_fb_circular.png";
  static const String facebookLogoImageNew =
      imagesPath + "facebook_logo_new.png";
  static const String instagramLogoImage = imagesPath + "instagram_logo.png";
  static const String instagramLogoImageNew = imagesPath + "insta_logo_new.png";
  static const String instagramLogoCircle =
      imagesPath + "icon_instagram_circle.png";
  static const String linkedInLogoImage = imagesPath + "linked_in_logo.png";
  static const String linkedInLogoImageNew = imagesPath + "linked_in_log.png";
  static const String achieveDreamImage =
      imagesPath + "achieve_dream_image.png";
  static const String addVisitorOneClickImage =
      imagesPath + "add_visitor_one_click_image.png";
  static const String linkedInLogoCircle =
      imagesPath + "icon_linked_in_circle.png";
  static const String dummyMapImage = imagesPath + "dummy_map_photo.jpg";
  static const String iconAddNewVisitor =
      imagesPath + "icon_add_new_visitor.png";
  static const String iconMyStatus = imagesPath + "icon_my_status.png";
  static const String iconServed = imagesPath + "icon_served.png";
  static const String iconServedGreen = imagesPath + "icon_served_green.png";
  static const String addNewVisitorImage = imagesPath + "new_visitor_image.png";
  static const String loadingGif = imagesPath + "loading.gif";
  static const String unAvailableProfileImage =
      imagesPath + "unavailable_profile_image.jpg";
}
