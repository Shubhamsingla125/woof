import 'dart:io';

import 'package:woof_fini/utils/app_constants.dart';

class LocalData {
//  UserEntity userData;
  File userProfilePic;
  int userLoginProgress;

  LocalData.init();

  LocalData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    if (json['user_data'] != null) {
//      this.userData = UserEntity.fromJson(json['user_data']);
    }
    this.userLoginProgress = (json['user_login_progress'] != null)
        ? json['user_login_progress']
        : UserLoginProgress.userUnauthenticated;
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

//    if (userData != null) data['user_data'] = this.userData.toJson();
//    data['is_user_login'] = this.isUserLogin;
    data["user_login_progress"] = this.userLoginProgress;

    return data;
  }

  String getUserId() {
//    return this.userData != null ? this.userData.uniqueId : "";
  }

  File getUserPic() {
    return userProfilePic;
  }
}
