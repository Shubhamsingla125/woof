import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'l10n/messages_all.dart';

///
class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en'].contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return AppLocalizations.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) {
    return false;
  }
}

class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);

  // Qoka Vendor Temporary String

  get profile => Intl.message('Profile', name: 'profile');

  get myStatus => Intl.message('My Status', name: 'myStatus');

  get liveStatus => Intl.message('Live Status', name: 'liveStatus');

  get editStatus => Intl.message('Edit Status', name: 'editStatus');

  get offerStatus => Intl.message('Offer Status', name: 'offerStatus');

  get change => Intl.message('Change', name: 'change');

  get manageStaff => Intl.message('Manage Staff', name: 'manageStaff');

  get save => Intl.message('Save', name: 'save');

  get foodList => Intl.message('Food List', name: 'foodList');

  get drList => Intl.message('Dr.List', name: 'drList');

  get barList => Intl.message('Bar List', name: 'barList');

  get createdBy => Intl.message('Created By', name: 'createdBy');

  get createdWhen => Intl.message('Created When', name: 'createdWhen');

  get addNewList => Intl.message('Add New List', name: 'addNewList');

  String get profileSubTitleName =>
      Intl.message('PIND BALLUCHI', name: 'profileSubTitleName');

  /// All String Qoka Vendor App

  get hours => Intl.message('hours', name: 'hours');

  get today => Intl.message('Today', name: 'today');

  get customizeDate => Intl.message('Customize Date', name: 'customizeDate');

  get sms => Intl.message('SMS', name: 'sms');

  get websiteLink => Intl.message('Website Link', name: 'websiteLink');

  get others => Intl.message('Others', name: 'others');

  get alphabetically => Intl.message('Alphabetically', name: 'alphabetically');

  get chatWithUs => Intl.message('Chat With Us', name: 'chatWithUs');

  get videoTutorials => Intl.message('Video Tutorials', name: 'videoTutorials');

  get knowMoreAbout => Intl.message('Know More About', name: 'knowMoreAbout');

  get contactUs => Intl.message('Contact Us', name: 'contactUs');

  get appGuideTutorials =>
      Intl.message('App Guide Tutorials', name: 'appGuideTutorials');

  get emailUsMessageUs =>
      Intl.message('Email Us/Message Us', name: 'emailUsMessageUs');

  get averageServedTime =>
      Intl.message('Average Served time', name: 'averageServedTime');

  get updateStatus => Intl.message('Update Status', name: 'updateStatus');

  get imagePicker => Intl.message('Image Picker', name: 'imagePicker');

  get businessName => Intl.message('Business Name', name: 'businessName');

  get queueList => Intl.message('Queue List', name: 'queueList');

  get dashboard => Intl.message('Dashborad', name: 'dashboard');

  get summary => Intl.message('Summary', name: 'summary');

  get targetMarketing =>
      Intl.message('Target Marketing', name: 'targetMarketing');

  get totalServed => Intl.message('Total Served', name: 'totalServed');

  get uniqueCustomers =>
      Intl.message('Unique Customers', name: 'uniqueCustomers');

  get upgrade => Intl.message('Upgrade', name: 'upgrade');

  get averageWaitTime =>
      Intl.message('Average wait time', name: 'averageWaitTime');

  get noShow => Intl.message('No show', name: 'noShow');

  get cancellation => Intl.message('Cancellation', name: 'cancellation');

  get trend => Intl.message('Trend', name: 'trend');

  get sendOffer => Intl.message('Send Offer', name: 'sendOffer');

  get sortBy => Intl.message('Sort By', name: 'sortBy');

  get startDate => Intl.message('Start Date', name: 'startDate');

  get endDate => Intl.message('End Date', name: 'endDate');

  get ok => Intl.message('Ok', name: 'ok');

  get orderBy => Intl.message('Order By', name: 'orderBy');

  get premium => Intl.message('Premium(coming soon)', name: 'premium');

  get setting => Intl.message('Settings', name: 'setting');

  get help => Intl.message('Help', name: 'help');

  String get areYouSureYouWantToLogout =>
      Intl.message("Are you sure you want to logout?",
          name: "areYouSureYouWantToLogout");

  get logout => Intl.message('Logout', name: 'logout');

  get uploadingProfilePic =>
      Intl.message('Uploading profile pic...', name: 'uploadingProfilePic');

  get uploadingStatus =>
      Intl.message('Uploading status...', name: 'uploadingStatus');

  get statusUploaded => Intl.message('Status uploaded', name: 'statusUploaded');

  get statusUploadedSuccessfully => Intl.message('Status uploaded successfully',
      name: 'statusUploadedSuccessfully');

  get minutes => Intl.message('minutes', name: 'minutes');

  get confirmMessage => Intl.message(
      "Hi <<Customer Name>> you are queued to <<business>> as #<<queue #>> in line. See live wait at <<link>>. If you wish to cancel click<<cancel button>>",
      name: "writeSomeMessageDetail");

  get inQueue => Intl.message('In-Queue', name: 'inQueue');

  get add => Intl.message('Add', name: 'add');

  get served => Intl.message('Served', name: 'served');

  get queueListName => Intl.message('Queue list name', name: 'queueListName');

  get search => Intl.message('Search', name: 'search');

  get imageUploadFailed =>
      Intl.message('Image uploading failed', name: 'imageUploadFailed');

  get offer => Intl.message('Offer', name: 'offer');

  get reset => Intl.message('Reset', name: 'reset');

  get confirmation => Intl.message('Confirmation', name: 'confirmation');

  get confirmations => Intl.message('Confirmations', name: 'confirmations');

  get nextInLine => Intl.message('Next', name: 'nextInLine');

  get alert => Intl.message('Alert', name: 'alert');

  get youAreAllSet =>
      Intl.message('Your are all set to add your first customer',
          name: 'youAreAllSet');

  get showcaseYourse =>
      Intl.message('Showcase yourself with personalised logo and tagline',
          name: 'showcaseYourselfWithPersonalisedLogoAndTagLine');

  get ifYouWantYouCanSkipNowAndChange => Intl.message(
      'If you want you can skip for now and can change in Profile tab any time now feel like',
      name: 'ifYouWantYouCanSkipNowAndChange');

  get registerNewUser =>
      Intl.message('Register New User', name: 'registerNewUser');

  get addNewVisitor => Intl.message('Add New Visitor', name: 'addNewVisitor');

  String get fullName => Intl.message('Full Name', name: 'fullName');

  get phoneNo => Intl.message('Phone no.', name: 'phoneNo');

  get remarks => Intl.message('Remarks', name: 'remarks');

  get waitingTime => Intl.message('Waiting Time', name: 'waitingTime');

  get email => Intl.message('Email', name: 'email');

  get createPassword => Intl.message('Create Password', name: 'createPassword');

  get confirmPassword =>
      Intl.message('Confirm Password', name: 'confirmPassword');

  get register => Intl.message('Register', name: 'register');

  get preview => Intl.message('Preview', name: 'preview');

  get tagLine => Intl.message('Tag Line', name: 'tagLine');

  get businessDetails =>
      Intl.message('Business Details', name: 'businessDetails');

  get addressPickup => Intl.message('Address Pickup', name: 'addressPickup');

  get saveAndContinue =>
      Intl.message('Save and Continue', name: 'saveAndContinue');

  get companyName => Intl.message('Company Name', name: 'companyName');

  get locateYourBusinessName =>
      Intl.message('Locate Your Business Name', name: 'locateYourBusinessName');

  get businessType => Intl.message('Business Type', name: 'businessType');

  get pickImageFromGallery =>
      Intl.message('Pick Image From Gallery', name: 'pickImageFromGallery');

  get pickImageFromCamera =>
      Intl.message('Pick Image From Camera', name: 'pickImageFromCamera');

  String get alreadyAMember =>
      Intl.message('Already a Member?', name: 'alreadyAMember');

  get byClickingOnRegister =>
      Intl.message('By Clicking On Register You Accept Our ',
          name: 'byClickingOnRegister');

  String get termsAndConditions =>
      Intl.message('Term and Conditions.', name: 'termsAndConditions');

  get prev => Intl.message('Prev', name: 'prev');

  get updateMessage => Intl.message('Update message', name: 'updateMessage');

  get SendAutomaticallyTo =>
      Intl.message('Send automatically to new visitors added to your waitlist',
          name: 'sentSmsAutomaticallyConfirmation');

  /// All Strings

  String get off => Intl.message("off", name: "off");

  String get activeContracts =>
      Intl.message('Active contracts: ', name: 'activeContracts');

  String get status => Intl.message('Status', name: 'status');

  String get addNewStatus =>
      Intl.message('Add new status', name: 'addNewStatus');

  String get created => Intl.message('Created', name: 'created');

  /// error messages

  String get onAllOrders => Intl.message("on all orders.", name: "onAllOrders");

  String get useCode => Intl.message("Use code", name: "useCode");

  String get predefined => Intl.message("Predefined", name: "predefined");

  String get selectOffers =>
      Intl.message("Select Offers", name: "selectOffers");

  String get writeNewOffer =>
      Intl.message("Write New Offer", name: "writeNewOffer");

  String get whomToSend => Intl.message("Whom to Send", name: "whomToSend");

  String get currentOffer =>
      Intl.message("Current Offer", name: "currentOffer");

  String get selectOffer => Intl.message("Select Offer", name: "selectOffer");

  String get offerSelected =>
      Intl.message("Offer Selected", name: "offerSelected");

  String get offers => Intl.message("Offer", name: "offers");

  String get send => Intl.message("Send", name: "send");

  /// dialogs

  String get cancel => Intl.message("Cancel", name: "cancel");

  String get selectedVisitors =>
      Intl.message("Selected visitors", name: "selectedVisitors");

  String get high => Intl.message("High", name: "high");

  get medium => Intl.message("Medium", name: "medium");

  String get login => Intl.message('Login', name: 'login');

  String get close => Intl.message('Close', name: 'close');

  // home

  //MaidMe

  String get language => Intl.message("Language", name: "language");

  String get next => Intl.message("Next", name: "next");

  String get done => Intl.message("Done", name: "done");

  String get addComment => Intl.message("Add Comment", name: "addComment");

  String get addStatus => Intl.message("Add Status", name: "addStatus");

  String get remove => Intl.message("Remove", name: "remove");

  get user => Intl.message("User", name: "user");

  String get home => Intl.message("Home", name: "home");

  String get navigation => Intl.message("Navigation", name: "navigation");

  String get filter => Intl.message("Filter", name: "filter");

  //
  String get address => Intl.message("Address", name: "address");

  String get date => Intl.message("DATE:", name: "date");

  String get from => Intl.message("From", name: "from");

  String get total => Intl.message("Total", name: "total");

  String get set => Intl.message("Set", name: "set");

//  String get monthly => Intl.message("Monthly", name: "monthly");

  String get accept => Intl.message("Accept", name: "accept");

  String get skip => Intl.message("Skip", name: "skip");

//  String get bookNow => Intl.message("Book now", name: "bookNow");
//  String get scheduleYourBooking => Intl.message("Schedule your booking", name: "scheduleYourBooking");

  get low => Intl.message("Low", name: "low");

//  String get skip => Intl.message("Skip", name: "skip");

//  String get next => Intl.message("Next", name: "next");

//  String get home => Intl.message("Home", name: "home");

//  String get login => Intl.message("Login", name: "login");

  String get error => Intl.message("Error", name: "error");

  String get errorFetchingItems =>
      Intl.message("Error fetching items", name: "errorFetchingItems");

  String get noItemFound => Intl.message("No item found", name: "noItemFound");

  String get previous => Intl.message("Previous", name: "previous");

  String get selectAll => Intl.message("Select All", name: "selectAll");

  String get unlimitedCustomer =>
      Intl.message("Unlimited Customer", name: "unlimitedCustomer");

  String get upgradeNow => Intl.message("Upgrade\n now ", name: "upgradeNow");

  String get month => Intl.message("Month", name: "month");

  String get youCanAdd =>
      Intl.message("You can add as many customer you want ...", name: "month");

  String get upgradePlans =>
      Intl.message("Upgrade Plans", name: "upgradePlans");

  String get rupeeSymbol => Intl.message("\u20B9", name: "rupeeSymbol");

  String get more => Intl.message("More", name: "more");

  get addUpTo100Customer =>
      Intl.message("Add up to 100 customer to list without any charged...",
          name: "addUpTo100Customer");

  String get activePlans => Intl.message("Active Plan", name: "activePlans");

  String get free => Intl.message("Free", name: "free");

  String get name => Intl.message("Name", name: "name");

  String get addStaffMember =>
      Intl.message("Add staff member", name: "addStaffMember");

//  String get save => Intl.message("Save", name: "save");

  String get editMember =>
      Intl.message("Edit staff member", name: "editMember");

  String get manageStaffMember =>
      Intl.message("Manage staff member", name: "manageStaffMember");

  String get writeMessageHere =>
      Intl.message("Write message here", name: "writeMessageHere");

  String get userChat => Intl.message("User Chat", name: "userChat");

  String get code => Intl.message("Code", name: "code");

  String get offPercentage =>
      Intl.message("Off percentage", name: "offPercentage");

  String get title => Intl.message("Title", name: "title");

  String get create => Intl.message("Create", name: "create");

  String get description => Intl.message("Description", name: "description");

  String get newOffer => Intl.message("New offer", name: "newOffer");

  String get pleaseEnterOfferTitle =>
      Intl.message("Please enter offer title", name: "pleaseEnterOfferTitle");

  String get pleaseEnterOfferCode =>
      Intl.message("Please enter offer code", name: "pleaseEnterOfferCode");

  String get pleaseEnterUniqueCode =>
      Intl.message("Please enter unique code", name: "pleaseEnterUniqueCode");

  String get pleaseEnterOffPercentage =>
      Intl.message("Please enter Off percentage",
          name: "pleaseEnterOffPercentage");

  String get pleaseEnterDescription =>
      Intl.message("Please enter description", name: "pleaseEnterDescription");

  String get lastOffer => Intl.message("Last Offer", name: "lastOffer");

  String get forgotPassword =>
      Intl.message("Forgot password?", name: "forgotPassword");

  String get dontHaveAccount =>
      Intl.message("Don’t have an account?", name: "dontHaveAccount");

  String get createNewAccount =>
      Intl.message("Create a new account.", name: "createNewAccount");

  String get sendOTP => Intl.message("Send OTP", name: "sendOTP");

  String get confirmOTP => Intl.message("Confirm OTP", name: "confirmOTP");

  String get resendOTP => Intl.message("Resend OTP", name: "resendOTP");

  String get verify => Intl.message("Verify", name: "verify");

  String get pleaseEnterBusinessName =>
      Intl.message("Please enter business name",
          name: "pleaseEnterBusinessName");

  String get pleaseEnterPhone =>
      Intl.message("Please enter contact number", name: "pleaseEnterPhone");

  String get pleaseEnterValidEmail =>
      Intl.message("Please enter valid email", name: "pleaseEnterValidEmail");

  String get numberLengthShouldbeNorway =>
      Intl.message("Number should be 8 digits",
          name: "numberLengthShouldbeNorway");

  String get numberLengthShouldbeUnitedKingdom =>
      Intl.message("Number should be 10 digits",
          name: "numberLengthShouldbeUnitedKingdom");

  String get numberLengthShouldbeIndia =>
      Intl.message("Number should be 10 digits",
          name: "numberLengthShouldbeIndia");

  String get pleaseSelectVisitor =>
      Intl.message("Please select visitor", name: "pleaseSelectVisitor");

  String get manageQueueList =>
      Intl.message("Manage queue list", name: "manageQueueList");

  String get newQueueList =>
      Intl.message("New queue list", name: "newQueueList");

  String get pleaseEnterName =>
      Intl.message("Please enter name", name: "pleaseEnterName");

  String get performance => Intl.message("Performance", name: "performance");

  String get confirm => Intl.message("Confirm", name: "confirm");

  String get pleaseUploadImage =>
      Intl.message("Please upload image", name: "confirm");

  String get pleaseEnterTagLine =>
      Intl.message("Please enter tag line", name: "confirm");

  String get uploadImage => Intl.message("Upload Image", name: "confirm");

  String get pleaseEnterUploadImageAndTagLine =>
      Intl.message("Please Upload image and enter tagline",
          name: "pleaseEnterTagLineAndUploadImage");

  String get pleaseEnterFullName =>
      Intl.message("Please enter full name", name: "pleaseEnterFullName");

  String get pleaseEnterPhoneNumber =>
      Intl.message("Please enter phone number", name: "pleaseEnterPhoneNumber");

  String get pleaseEnterEmail =>
      Intl.message("Please enter email Address", name: "pleaseEnterEmail");

  String get pleaseEnterPassword =>
      Intl.message("Please enter password", name: "pleaseEnterPassword");

  String get pleaseEnterConfirmPassword =>
      Intl.message("Please enter confirm password",
          name: "pleaseEnterConfirmPassword");

  String get pleaseAcceptTermsAndCondition =>
      Intl.message("Please accept terms and condition",
          name: "pleaseAcceptTermsAndCondition");

  String get editQueueList =>
      Intl.message("Edit queue list", name: "editQueueList");

  String get areYouSureServedList =>
      Intl.message("Are you sure you want to move visitor in Served list?",
          name: "areYouSureServedList");

  String get submit => Intl.message("Submit", name: "submit");

  String get visitorStatus =>
      Intl.message("Visitor status", name: "visitorStatus");

  String get pleaseSelectOffer =>
      Intl.message("Please select offer", name: "pleaseSelectOffer");

  String get pleaseSelectBusinessType =>
      Intl.message("Please select business type",
          name: "pleaseSelectBusinessType");

  String get pleaseEnterVisitorName =>
      Intl.message("Please enter visitor name", name: "pleaseEnterVisitorName");

  String get deleteTheQueue =>
      Intl.message("Are you sure, you want to delete these queues?",
          name: "deleteTheQueue");

  String get delete => Intl.message("Delete", name: "delete");

  String get pleaseEnterWaitingTime =>
      Intl.message("Please enter waiting time", name: "pleaseEnterWaitingTime");

  String get pleaseEnterRemarks =>
      Intl.message("Please enter remarks", name: "pleaseEnterRemarks");

  String get update => Intl.message("Update", name: "update");

  String get loginWithOtp =>
      Intl.message("Login with OTP", name: "loginWithOtp");

  String get enterOtp => Intl.message("Enter OTP", name: "enterOtp");

  String get processing => Intl.message("Processing...", name: "processing");

  String get enterValidOtp =>
      Intl.message("Please enter valid OTP", name: "enterValidOtp");

  String get pindBaluchi => Intl.message("Pind Balluchi", name: "pindBaluchi");

  String get editTagLineHere =>
      Intl.message("Edit tag line here", name: "editTagLineHere");

  String get edit => Intl.message("Edit", name: "edit");

  String get manageYourStaff =>
      Intl.message("Manage your staff", name: "manageYourStaff");

  String get addMore => Intl.message("Add more", name: "addMore");

  String get editSocialMediaLinks =>
      Intl.message("Edit social media links", name: "editSocialMediaLinks");

  String get stopReq => Intl.message("STOP\nREQ", name: "stopRequest");

  String get stopRequest => Intl.message("STOP REQUEST", name: "stopRequest");

  String get manageQueue => Intl.message("Manage Queue", name: "manageQueue");

  String get visitorName => Intl.message("Visitor Name", name: "visitorName");

  String get uniqueCode => Intl.message("Unique Code", name: "uniqueCode");

  String get copyToPredefinedTab =>
      Intl.message("Copy to predefined tab", name: "copyToPredefinedTab");

  String get newStatus => Intl.message("New Status", name: "newStatus");

  String get areYouSureToStop =>
      Intl.message("Are you sure you want to stop?", name: "areYouSureToStop");

  String get noNewMemberAdded =>
      Intl.message("No new member will be added if you shut down",
          name: "noNewMemberAdded");

  String get entryDetails =>
      Intl.message("Entry Details", name: "entryDetails");

  String get newPassword => Intl.message("New Password", name: "newPassword");

  String get averageWaitTimeInQueue =>
      Intl.message("Average wait time in Queue",
          name: "averageWaitTimeInQueue");

  String get queueName => Intl.message("Queue Name", name: "queueName");

  String get apply => Intl.message("Apply", name: "apply");

  String get resend => Intl.message("Re-send", name: "resend");

  String get didNotReceiveCode =>
      Intl.message("Did not receive the code?", name: "didNotReceiveCode");

  String get businessSpecialization =>
      Intl.message("Business Specialization", name: "businessSpecialization");

  String get editSpecialization =>
      Intl.message("Edit Specialization", name: "editSpecialization");

  String get enterCode => Intl.message("Enter code", name: "enterCode");

  String get otpWasWrong => Intl.message("Otp was wrong", name: "otpWasWrong");

  String get nextInLineMessage => Intl.message(
      "Heads up! Your are currently moved to 3rd in queue at <<business>> so start heading to location <<google link of business location>>,"
      "If you can’t make it, go to app or<<link>> to slide yourself down the list or to cancel click cancel button",
      name: "nextInLineMessage");

  String get alertMessage => Intl.message(
      "Hi! It’s your turn at <<business>>, Please be at location in 5 min. If you can’t make it, go to app or<<link>> to slide yourself down the list or to cancel click cancel button",
      name: "alertMessage");

  String get servedMessage => Intl.message(
      "Greetings! You have been served at <<business>>Post Served Text"
      "\nThankyou! Please rate your experience at <<Business>> by clicking <<link>>",
      name: "nextInLineMessage");

  String get enterYourMobile => Intl.message(
      "Enter your mobile number we will send you the OTP to verify",
      name: "enterYourMobile");

  String get userAlreadyExists =>
      Intl.message("User already exists", name: "userAlreadyExists");

  String get userNotFound =>
      Intl.message("User not found", name: "userNotFound");

  String get signingIn => Intl.message("Signing in", name: "signingIn");

  String get pickAddress => Intl.message("Pick address", name: "pickAddress");

  String get creatingUser =>
      Intl.message("Creating profile", name: "creatingUser");

  String get deleteTheOffers =>
      Intl.message("Are you sure, you want to delete these offers?",
          name: "deleteTheOffers");

  String get errorSendingOffer =>
      Intl.message("Error sending offer. Try again later.",
          name: "errorSendingOffer");

  String get offerSentSuccessfully =>
      Intl.message("Offer sent successfully", name: "offerSentSuccessfully");

  String get pleaseWait => Intl.message("Please wait.", name: "pleaseWait");

  String hoursAgo(String hour) =>
      Intl.message("$hour Hours ago", name: "hoursAgo", args: [hour]);

  String areYouSureToDelete(String visitorName) =>
      Intl.message("Are you sure you want to delete $visitorName?",
          name: "areYouSureToDelete");

  String sentOtpToNumber(String phoneNumber) =>
      Intl.message("We have sent otp on $phoneNumber",
          name: "sentOtpToNumber", args: [phoneNumber]);

  String otpExpiredIn(String time) =>
      Intl.message("Expired in $time", name: "enterOtp", args: [time]);

  String weSentOtp(String number) => Intl.message(
      "We have sent you an SMS on contact ${number ?? "contact"} with 6 digit verification code.",
      name: "weSentOtp",
      args: [number]);

  String permissionLevel(String level) =>
      Intl.message("Permissions level $level",
          name: "permissionLevel", args: [level]);

  String get password => Intl.message("Password", name: "password");

//  String get  => Intl.message('', name: '');
//  String get  => Intl.message('');
//  String get  => Intl.message('', name: '');

  /// Plurals
  String studentPlural(howMany) => Intl.plural(howMany,
      zero: 'Student',
      one: "Student",
      other: "Students",
      args: [howMany],
      name: "studentPlural");

  static Future<AppLocalizations> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return new AppLocalizations(locale);
    });
  }

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }
}
