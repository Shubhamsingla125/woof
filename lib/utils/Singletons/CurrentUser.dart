import 'package:woof_fini/Model/Login/LoginResponse.dart';

class CurrentUser {
  LoginResponse userData;

  static final _instance = new CurrentUser._internal();

  CurrentUser._internal();

  // Getters
  static CurrentUser get instance => _instance;

  LoginResponse get data => this.userData;
}