// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

// ignore: unnecessary_new
final messages = new MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);

  static _notInlinedMessages(_) => <String, Function>{
        "Notifications": MessageLookupByLibrary.simpleMessage("Notifications"),
        "addGoalMessage": MessageLookupByLibrary.simpleMessage(
            "Add goal by click on plus button in bottom right corner"),
        "addStudentMessage": MessageLookupByLibrary.simpleMessage(
            "Add student by click on plus button in bottom right corner"),
        "appName": MessageLookupByLibrary.simpleMessage("Spectrum Abilities"),
        "archive": MessageLookupByLibrary.simpleMessage("Archive"),
        "clone": MessageLookupByLibrary.simpleMessage("Clone"),
        "contactUs": MessageLookupByLibrary.simpleMessage("Contact us"),
        "copyrightDescription": MessageLookupByLibrary.simpleMessage(
            "Copyright (c) 2019 Spectrum Abilities India Pvt Ltd. All rights reserved"),
        "delete": MessageLookupByLibrary.simpleMessage("Delete"),
        "export": MessageLookupByLibrary.simpleMessage("Export"),
        "goalProgress": MessageLookupByLibrary.simpleMessage("Goal Progress"),
        "grade": MessageLookupByLibrary.simpleMessage("Grade"),
        "help": MessageLookupByLibrary.simpleMessage("Help"),
        "iepTeam": MessageLookupByLibrary.simpleMessage("IEP Team"),
        "iepTeamDescription": MessageLookupByLibrary.simpleMessage(
            "Connect with Students IEP Team\nView & update Goal Progress Update"),
        "ieponeFeatures": MessageLookupByLibrary.simpleMessage(
            "Platform Access & Experts Advice\nCompanion (AJ) Reports"),
        "ieponeWithSeparator": MessageLookupByLibrary.simpleMessage("IEP|ONE"),
        "lastUpdate": MessageLookupByLibrary.simpleMessage("Last update"),
        "noGoalAdded": MessageLookupByLibrary.simpleMessage("No Goal Added"),
        "noStudentAdded":
            MessageLookupByLibrary.simpleMessage("No Student Added"),
        "notificationsDescriptions": MessageLookupByLibrary.simpleMessage(
            "Messages from Parents\nSystem Notifications\nAlert Messages"),
        "progressReport":
            MessageLookupByLibrary.simpleMessage("Progress report"),
        "specialEducation":
            MessageLookupByLibrary.simpleMessage("Special Education"),
        "status": MessageLookupByLibrary.simpleMessage("Status"),
        "students": MessageLookupByLibrary.simpleMessage("Students"),
        "studentsAndIep":
            MessageLookupByLibrary.simpleMessage("Students & IEPs"),
        "studentsTheirIepDescription": MessageLookupByLibrary.simpleMessage(
            "Students and their IEP\'s \nAdd, Remove, Manage Students and IEP\'s"),
        "tutorials": MessageLookupByLibrary.simpleMessage("Tutorials")
      };
}
