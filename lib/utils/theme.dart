import 'package:flutter/material.dart';

class ThemeColor {
  //indicator color
  static const Color grey = Color(0xffC2C2C2);
  static const Color blueIndicatorActive = Color(0xff2533D6);

  /// Colors.............
  static const Color accentColor = blue3A58FC;

//  static const Color accentColor = Color(0xfffb5c00);
  static const Color primaryColor = blueDark;
  static const Color primaryDarkColor = blueDark;
  static const Color backgroundColor = Color(0xFFF5F5F5);

  /// black color  variations
  static const Color pinkColorFE = Color(0xFFFE4A95);
  static const Color blackColor16 = Color(0xFF163850);
  static const Color blackColor27 = Color(0xFF272727);

  /// blueGrey color variations
  static const Color blueGreyColor81 = Color(0xFF8195B8);

  ///Pink Color
  static const Color blackColorA3 = Color(0xa3000000);
  static const Color blackColor51 = Color(0xff515C6F);

  /// blue shades
  static const Color blueB4 = Color(0xffb4d7ff);
  static const Color blue4A = Color(0xff4a90e2);
  static const Color blueDark = Color(0xff070C44);
  static const Color blue6C = Color(0xff6C63FF);
  static const Color blue45 = Color(0xFF45B1E1);
  static const Color blueE2 = Color(0xFFE2E4FF);

  static const Color blue25 = Color(0xff2533D6);
  static const Color blue6E = Color(0xFF6E9EFF);

  static const Color blue06 = Color(0xFF06C3FF);
//  static const Color blue25 = Color(0xff2533D6);
  static const Color blueA5 = Color(0xffA54FFC);
  static const Color blue63 = Color(0xff63DAFF);

  /// red shades
  static const Color redF7 = Color(0xFFf7d5d4);
  static const Color redEE = Color(0xFFee0a0a);
  static const Color redE9 = Color(0xFFe92727);
  static const Color redFF6 = Color(0xFFFF6969);

  ///orange shades
  static const Color orangeF7 = Color(0xffffe9df);
  static const Color orangeEE = Color(0xFFfb5c00);
  static const Color orangeF0 = Color(0xFFF09635);

  /// blue shades
  static const Color blueF7 = Color(0xFFe4edff);
  static const Color blueEE = Color(0xFF0a58ee);
  static const Color blue3A58FC = Color(0xFF3A58FC);
  static const Color blueFC = Color(0xfc6a83b3);
  static const Color blue424a = Color(0x424a90e2);

  /// green shades
  static const Color green1C = Color(0xff1c8b1e);
  static const Color greenFF = Color(0xFF009688);
  static const Color greenFF0D = Color(0xFF0DB1A1);
  static const Color greenFF0C = Color(0xFF0CA898); //#0CA898
  static const Color greenFF15 = Color(0xFF15CB7C); //#0CA898
  static const Color green3A = Color(0xFF3AB09A); //#0CA898

  static const Color green8D = Color(0xff8DDC51); //#0CA898

  static const Color greenFF00 = Color(0xFF002BFF29); //#0CA898
  static const Color greenFF06 = Color(0xFF069D83); //#0CA898
  static const Color greenF6 = Color(0xFFF6FEFF); //#0CA898

  /// black color  variations
  static const Color blackColorCC = Color(0xcc000000);
  static const Color blackColor99 = Color(0x99000000);
  static const Color blackColor71 = Color(0x71000000);

  static const Color whiteColor = Colors.white;
  static const Color whiteColorFFFB = Color(0xFFFBFBFB);
  static const Color whiteColor26 = Color(0x26FFFFFF);
  static const Color blackColor = Colors.black;
  static const Color graphBlue = Color(0xA60A58ED);
  static const Color circleGraphBlueColor = Color(0xFF89A6FF);
  static const Color circleChatLightGreyColor = Color(0xFFf4f4f4);
  static const Color graphOrange = Color(0xA6FC7333);
  static const Color orange = Color(0xFFFB5C00);
  static const Color orangeBoarder = Color(0xFFFC7232);
  static const Color red = Color(0xFFDB3717);
  static const Color green = Color(0xFF099413);
  static const Color yellow = Color(0xFFF0C031);
  static const Color yellowFFE6 = Color(0xFFE6B12E); //#EFCE4A
  static const Color yellowEFCE = Color(0xFFEFCE4A);
  static const Color darkGreyColor = Color(0x8A000000);
  static const Color darkGreyColor51 = Color(0x8A323551);

  static const Color lightWhite = Color(0x99FFFFFF);

  /// blue shades
  static const Color blueColor = Color(0xFF0a58ee);
  static const Color blue2F = Color(0xff2F368B);
  static const Color blue81 = Color(0xff8195B8);

  static Color dropdownBorderColor = Color(0x51000000);

  ///shadow
  static const Color lightBlackShadowColor = Color(0x4D000000);
  static const Color lightBlackCircleShadowColorTwo = Color(0x1A000000);
  static const Color lightBlackCircleShadowColor00 = Color(0x00000029);

  /// grey shades
  static const Color grey97 = Color(0xff979797);
  static const Color greyE4 = Color(0xffe4e4e4);
  static const Color greyD1 = Color(0xffd1d1d1);
  static const Color grey5D = Color(0xff5d5d5d);
  static const Color grey3D = Color(0xff3b3d43);
  static const Color grey1E = Color(0xff1e1e1e);
  static const Color grey8f = Color(0xff18f8f8f);
  static const Color grey0B = Color(0xff0b0b0b);
  static const Color grey9B = Color(0xff9b9b9b);
  static const Color grey6E = Color(0xff6e6e6e);
  static const Color grey72 = Color(0xFF727C8E);
  static const Color greyC4 = Color(0xFFC4C4C4);
  static const Color grey8A = Color(0xFF8A8A8A);

  static const Color grey9D = Color(0xff9DA0A5);
  static const Color greyF1 = Color(0xffF1F1F9);
  static const Color greyC2 = Color(0xffC2C2C2);
  static const Color greyF5 = Color(0xffF5F6F8);
  static const Color grey70 = Color(0xff707070);
  static const Color greyE6 = Color(0xffE6E6E6);

//  static const Color greyF5 = Color(0xFFF5F6F8);

  /// black color  variations
  static const Color blackColor8a = Color(0x8a000000);
  static const Color blackColor33 = Color(0x33000000);
  static const Color redFB = Color(0xFFfb5253);

  /// grey shades
  static const Color greyD8 = Color(0xffd8d8d8);

// text Color
  static const Color textLightBlackColor = blackColor51;
  static const Color bodyTextColor = Color(0xFF8D9AA9);
  static const Color greyTextColor = Color(0x61000000);
  static const Color blackTextColor = Color(0x99000000);
  static const Color blackTextColorFF89 = Color(0xFF898A8F);
  static const Color blackTextColorFF33 = Color(0xFF333348);
  static const Color blackTextColorFF31 = Color(0xFF313450);
  static const Color summaryTileGradientColorFirst = Color(0xFF7372FF);
  static const Color summaryTileGradientColorSecond = Color(0XFF99AAF7);
  static const Color headlineColor = Color(0xDE000000);
  static const Color blueButtonColor = blue2F;

  //textField
  static const Color textFieldBorderColor = Color(0xFFECECEC);

//  button border color
  static const Color buttonBorderColor = Color(0xFFE5D6FF);

// shadow color
  static const Color tileBoxShadowColor = Color(0xFFD6E1FF);

// border Color
  static const Color iconBorderColor = Color(0xFFe6e6e6);
  static const Color buttonBorderColorFFC7 = Color(0xFFC7C7C7);
  static const Color graphContainerBorderColor = Color(0xffebebeb);
  static const Color lightGreyBorderColor = Color(0xFFF6F6F6);
  static const Color graphBorderCOlor = Color(0xFFEBEBEB);
  static const Color greyBorderColor = Color(0xFFd8d8d8);
  static const Color attendanceHeaderColor = Color(0xfff5f5f5);
  static const Color greyBorderColorFFDE = Color(0xFFDEDEDE);

  static const Color heading6FontColor = Color(0xde000000);
  static const Color body3FontColor = Color(0xcc000000);

  static const Color greyNavigationExpandColor = Color(0xFFdfe1e5);

  static const Color seekbarBackgroundColor = Color(0xFFE3E3E3);

  // navigation color
  static const Color navigationTextColor = Color(0xFF878787);

  /// gradient colors
  static const Color avatarStartColor = Color(0xfffad961);
  static const Color avatarEndColor = Color(0xfff76b1c);

  static const String graphBlueHex = '#A60A58ED';
  static const String graphOrangeHex = '#A6FC7333';
  static const String blueHex = '#1d3fdd';
  static const String redHex = '#d0021b';
  static const String orangeHex = "#ff9947";
  static const String greenHex = "#03bd5b";
  static const String purpleHex = "#acabff";
  static const String graphBlueHex59 = '#5984d6';
  static const String graphBlueHexDB = '#dbe9f9';

  /// default donut graph colors
  static const String defaultDonutGraphFilledColor = '#89a6ff';
  static const String defaultDonutGraphEmptyColor = '#f4f4f4';

  /// divider color
  static const Color dividerColorLight = Color(0xfff1f1f1);
  static const Color dividerColorDa = Color(0xffdadada);
  static const Color dividerColorDefault = Colors.black54;
  static const Color dividerColorDark = Colors.black87;

  static const Color expansionTileBackground = Color(0x21848c9b);

  static const Color skyBlueColor = Color(0xFFdde9ff);
  static const Color homeScreenToolbarColor = Color(0xFF0DB1A1);
  static const Color titleNameTextColor = Color(0xFF313450);
  static const Color fadedTextColor = Color(0xFF898A8F);
  static const Color orangeTextColor = Color(0xFFE6B12E);
  static const Color occupationBorderColor = Color(0xFFECECEC);
  static const Color autoBookDescriptionTextColor = Color(0xFF898A8F);
  static const Color backgroundColorLightGrey = Color(0xFFEEEEEE);
  static const Color backgroundColorLightGreyD6 = Color(0xFFD6D6D6);
  static const Color backgroundColorPrimaryLight = Color(0xFFF3FBFA);

  //enter otp code  underline color //C9C9C9
  static const Color enterOtpUnderlineColor = Color(0xFFC9C9C9);
  static const Color borderColor4460 = Color(0x59484460);

  /// Qoka Vendor
//  static const Color textLightBlack = blackColor51;
  static const Color textHintGreyColor = grey9D;
  static const Color lightGreyColor = greyF1;
//  static const Color switchGreenColor = green8D;
  static const Color bottonNavigationBlueColor = blue2F;
  static const Color textHourAndMinutesLightBlueColor = blue81;
  static const Color lightGreyContainerColor = greyF5;
  static const Color darkGrey70Color = grey70;

  /// Indicator color
  static const Color unSelectedGreyColor = greyC2;
  static const Color activeBlueColor = blue25;

  ///
  //Qoka Vendor
  static const Color textBorderBlackColor = Color(0xff515C6F);
  static const Color textHintGrey = Color(0xff9DA0A5);
  static const Color lightGrey = Color(0xffF1F1F9);
  static const Color switchGreenColor = Color(0xff8DDC51);
  static const Color bottonNavigationColor = Color(0xff2F368B);
  static const Color textHourAndMinutesColor = Color(0xff8195B8);
}

// font
class ThemeFont {
  String roboto = "RobotoRegular";
  String helvetica = "Helvetica";
  String sansSerif = "SansSerif";
}

/// NAME       SIZE   WEIGHT   SPACING  2018 NAME
/// display4   112.0  thin     0.0      headline1    96.0  light   -1.5     54 - 0x8A000000
/// display3   56.0   normal   0.0      headline2    60.0  light   -0.5     54 - 0x8A000000
/// display2   45.0   normal   0.0      headline3    48.0  normal   0.0     54 - 0x8A000000
/// display1   34.0   normal   0.0      headline4    34.0  normal   0.25    54 - 0x8A000000
/// headline   24.0   normal   0.0      headline5    24.0  normal   0.0     87 - 0xDD000000
/// title      20.0   medium   0.0      headline6    20.0  medium   0.15    87 - 0xDD000000
/// subhead    16.0   normal   0.0      subtitle1    16.0  normal   0.15    87 - 0xDD000000
/// body2      14.0   medium   0.0      body1        16.0  normal   0.5     87 - 0xDD000000
/// body1      14.0   normal   0.0      body2        14.0  normal   0.25    87 - 0xDD000000
/// caption    12.0   normal   0.0      caption      12.0  normal   0.4     54 - 0x8A000000
/// button     14.0   medium   0.0      button       14.0  medium   0.75    87 - 0xDD000000
/// subtitle   14.0   medium   0.0      subtitle2    14.0  medium   0.1     colors.black
/// overline   10.0   normal   0.0      overline     10.0  normal   1.5     colors.black

final typography2018 = Typography(
  platform: TargetPlatform.android,
  englishLike: Typography.englishLike2018,
  dense: Typography.dense2018,
  tall: Typography.tall2018,
);

final appbarTheme = AppBarTheme(
    brightness: Brightness.light,
    iconTheme: IconThemeData(color: Colors.white),
    color: Colors.white,
    elevation: 4);

final tabBarTheme = TabBarTheme(
  labelColor: Colors.black87,
  unselectedLabelColor: Colors.black54,
);

/// Dimensions

class ThemeDimension {
  static const double appBarElevation = 0;
  static const double appBarLogoHeight = 34;

  static const double paddingTiny = 2;
  static const double paddingVerySmall = 4;
  static const double paddingSmall6 = 6;
  static const double paddingSmall = 8;
  static const double paddingSmall10 = 10;
  static const double paddingSmall12 = 12;
  static const double paddingSmall14 = 14;
  static const double paddingSmall15 = 15;
  static const double paddingMedium = 16;
  static const double paddingMedium17 = 17;
  static const double paddingMedium18 = 18;
  static const double paddingMediumX = 20;
  static const double paddingMediumX22 = 22;
  static const double paddingMediumX23 = 23;
  static const double paddingLarge = 24;
  static const double paddingLarge26 = 26;
  static const double paddingLarge28 = 28;
  static const double paddingLarge25 = 25;
  static const double paddingLarge30 = 30;
  static const double paddingLarge40 = 40;
  static const double paddingVeryLarge = 32;
  static const double paddingVeryLarge36 = 36;
  static const double paddingVeryLarge42 = 42;
  static const double paddingVeryLarge50 = 50;
  static const double paddingVeryLarge55 = 55;
  static const double paddingVeryLarge60 = 60;
  static const double paddingVeryLarge66 = 66;
  static const double paddingVeryLarge70 = 70;
  static const double paddingVeryLarge75 = 75;
  static const double paddingVeryLarge76 = 76;
  static const double paddingVeryLarge80 = 80;
  static const double paddingVeryLarge84 = 84;
  static const double paddingVeryLarge88 = 88;
  static const double paddingVeryLarge100 = 100;
  static const double paddingVeryLarge110 = 110;
  static const double paddingVeryLarge120 = 120;
  static const double paddingVeryLarge122 = 122;
  static const double paddingVeryLarge123 = 123;
  static const double paddingVeryLarge130 = 130;
  static const double paddingVeryLarge150 = 150;
  static const double paddingVeryLarge200 = 200;
  static const double paddingVeryLarge250 = 250;
  static const double paddingVeryLarge380 = 380;
  static const double paddingVeryLarge400 = 400;

  static const double avatarRadiusTiny = 10;
  static const double avatarRadiusVerySmall = 12;
  static const double avatarRadiusDefault = avatarRadiusSmall;
  static const double avatarRadiusSmall = 18;
  static const double avatarRadiusMedium = 24;
  static const double avatarRadiusMedium25 = 25;
  static const double avatarRadiusLarge = 32;
  static const double avatarRadiusVeryLarge = 48;

  static const double avatarDimensionVerySmall = 20;
  static const double avatarDimensionSmall = 24;
  static const double avatarDimensionSmallLarge = 28;
  static const double avatarDimensionMediumSmall = 34;
  static const double avatarDimensionMedium = 40;
  static const double avatarDimensionMedium44 = 44;
  static const double avatarDimensionMediumLarge = 48;
  static const double avatarDimensionLargeVerySmall = 54.0;
  static const double avatarDimensionLargeSmall = 64;
  static const double avatarDimensionLarge = 80;

  static const double iconSizeVerySmall = 12;
  static const double iconSizeVerySmall10 = 10;
  static const double iconSizeSmall = 14;
  static const double iconSizeSmallLarge = 16;
  static const double iconSizeSmallLarge17 = 17;
  static const double iconSizeSmallLarge18 = 18;
  static const double iconSizeMediumSmall = 20;
  static const double iconSizeMedium = 22;
  static const double iconSizeMedium24 = 24;
  static const double iconSizeLarge = 28;
  static const double iconSizeVeryLarge = 32;
  static const double iconSizeVeryLarge40 = 40;

  static const double borderRadiusVerySmall = 4.0;
  static const double borderRadiusSmall = 6.0;
  static const double borderRadiusMediumSmall = 8.0;
  static const double borderRadiusMedium = 10.0;
  static const double borderRadiusMedium11 = 11.0;
  static const double borderRadiusMedium12 = 12.0;
  static const double borderRadiusLarge = 18;
  static const double borderRadiusLarge20 = 20;
  static const double borderRadiusLarge24 = 24;
  static const double borderRadiusVeryLarge = 30.0;
  static const double borderRadiusVeryLarge31 = 31.0;

  static const double borderHeightVerySmall = 1.0;
  static const double borderHeightSmall = 2.0;
  static const double borderHeightMedium = 4.0;

  static const double valueZero = 0.0;
  static const double valueOne = 1.0;

  /// Radius
  static const double borderRadius2 = 2;
  static const double borderRadius4 = 4;
  static const double borderRadius6 = 6;
  static const double borderRadius10 = 10;
  static const double borderRadius12 = 12;
  static const double buttonBorderRadius24 = 24;
  static const double borderRadius28 = 28;

  static const double countryFlagRadius = 4;
  static const double servedRadius = 16;

  static const double profileImageRadiusSmall = 30;
  static const double profileImageRadius = 45;
  static const double profileImageRadiusLarge = 60;
  static const double profileImageRadiusVeryLarge = 85;
  static const double profileImageBackgroundRadius = 47;
  static const double reviewImageRadius = 25;
  static const double completeJobImageRadius = 75;
  static const double verificationsImageRadius = 60;

  static const double monthCalenderCellDimension = 36;
  static const double appBarDividerHeight = 2;
  static const double appBarHeight = 84;
  static const double itemSelectionRowHeight = 34.0;

  static const double defaultButtonBorderRadius = 1;
  static const double defaultBottomSheetBorderRadius = 10;
  static const double homeworkTitleAvatarRadius = 20;
  static const double childAvatarRadius = 35.0;

  static const double atmCardHeight = 175;
  static const double drawerHeaderHeight = 194;
  static const double progressBarHeight = 3;
  static const double verifiedBackgroundWidth = 90;
  static const double verifiedBackgroundWidthLarge = 165;
  static const double saveButtonWidth = 100;
  static const double verifiedBackgroundHeight = 25;
  static const double greenBorderButtonWidth = 25;
  static const double greenBorderButtonWidthLarge = 35;
  static const double dummyMapImageWidth = 250;
  static const double dummyMapImageHeight = 170;

  /// Card
  static final double defaultCardElevation = 12.0;
  static const double defaultCardCornerRadius = 12.0;
  static const double cardCornerRadius24 = 24.0;
  static const double defaultCardMargin = 4.0;

  ///button
  static final double buttonWidthMedium = 30;
  static const double buttonWidthLarge = 40;
  static const double buttonWidthLarge42 = 42;
  static const double defaultButtonBorderWidth = 1;
  static final double buttonWidthLarge50 = 50;
  static final double buttonWidthLarge60 = 50;
  static final double buttonWidthTiny = 10;
  static const double buttonDefaultElevation = 6;

  ///otp text height
  static const double otpTextWidthAndHeight = 25;

  ///image
  static const double giftBoxImageWidthAndHeight = paddingLarge40;

  ///tab height
  static const double defaultTabHeight = 52;

  ///default dialog height
  static const double defaultDialogHeight = 300;

  ///selected visitor dialog in offerScreen
  static const double customDialogHeightOfferScreen = 420;
  static const double customDialogHeight385 = 380;

  //textField
  static final double textFieldBorderRadius = 8.0;

  //addOrEdit Profile Screen
  static final double addOrEditUserImageHeightWidth = 118.0;
  static final double addOrEditUserImageHeightWidthTwo = 32.0;
  static final double timeLineCircleWidthAndHeight = 22.0;
  static final double cameraImageHeightWidth = 50.0;
  static final double userPicInChatWidthHeight = 40.0;

  static const double backgroundCurtainHeightWithAppBar = 140;
  static const double backgroundCurtainHeightWithoutAppBar = 230;

  static double dottedLineTotalWidth = 430;
  static const double dottedLineEmptyWidth = 8;
  static const double dottedLineDashWidth = 10;
  static const double dottedLineDashHeight = 2;
  static double dottedLineHeightJobeCompleted = 250;
  static double dottedLineHeightInvoiceRaised = 360;
  static double dottedLineHeightMaidAccepted = 0;
  static double dottedLineHeightMaidOnTheWay = 80;
  static double dottedLineHeightJobStarted = 160;
  static double dottedLineHeightHistory = 70;

  static double dottedLineHeightMaidOnTheWayProvider = 90;
  static double dottedLineHeightJobStartedProvider = 180;
  static double dottedLineHeightJobCompletedProvider = 260;

  static int radioValue0 = 0;
  static int radioValue1 = 1;
  static int radioValue2 = 2;
  static int radioValue3 = 3;
  static int radioValue4 = 4;

  static double faqContainerHeight = 80;

  ///  Qoka Vendor Logo Image
  static double imageSizeSmall = 200;
  static double imageSizeMedium = 250;
  static double imageSizeLarge = 300;

  static double imageSizeMedium80 = 80;
  static double offerImageWidth = 110;
  static double offerImageHeight = 150;

  /// Qoka Vendor Dimension
  static const double indicatorRadiusDefault = 12;
  static const double indicatorSpaceDefault = 20;

  // Qoka Vendor Logo Image
  static double widthImage250 = 250;
  static double heightImage200 = 200;

//Qoka Vendor Dimession
  static const double indicatorShap12 = 12;
  static const double indicatorShapSpace20 = 20;

  ///Opacity
  ///Opacity value goes from 0.0 to 1.0
  static const double opacity75 = 0.75;
}
