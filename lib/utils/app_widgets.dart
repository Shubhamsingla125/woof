import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProgressView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}

class EmptyView extends StatelessWidget {
  final String title, message;

  EmptyView(this.title, {this.message});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              title,
              style: Theme.of(context).textTheme.headline,
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(top: 24),
            ),
            message != null
                ? Text(message,
                    style: Theme.of(context).textTheme.subtitle,
                    textAlign: TextAlign.center)
                : Container(),
          ],
        ),
      ),
    );
  }
}

class ErrorView extends StatelessWidget {
  final String title, message;

  ErrorView(this.title, this.message);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 16.0),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: EdgeInsets.only(top: 4.0),
            ),
            Text(
                message ??
                    "Unable to connect to server properly. Please try again later.",
                style: TextStyle(color: Theme.of(context).accentColor),
                textAlign: TextAlign.center)
          ],
        ),
      ),
    );
  }
}

class MyAppBar extends AppBar {
  MyAppBar(
      {Key key, Widget title, VoidCallback onBackPressed, List<Widget> actions})
      : super(
            actions: actions,
            key: key,
            title: title,
            centerTitle: true,
            backgroundColor: Colors.blue,
            elevation: 0.0,
            leading: IconButton(
              icon: Icon(
                Icons.chevron_left,
                color: Colors.white,
              ),
              onPressed: onBackPressed,
            ));
}

class MessageInCenterWidget extends StatelessWidget {
  final String _message;

  MessageInCenterWidget(this._message);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(_message, style: Theme.of(context).textTheme.title),
    );
  }
}
