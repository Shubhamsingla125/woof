// Validates email format
import 'package:flutter/cupertino.dart';
import 'package:woof_fini/Utils/Localization.dart';
import 'package:woof_fini/Utils/LocalizationValues.dart';

bool isEmailFormatValid(String email) {
  String p =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(p);
  return regExp.hasMatch(email);
}

bool isWebsiteFormatValid(String website) {
  String p =
      r"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+([\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.])+$";
  RegExp regExp = new RegExp(p, caseSensitive: false);
  return regExp.hasMatch(website);
}

// Validates email field
String emailValidator(
    {@required String email, @required BuildContext context}) {
  if (email.trim().isEmpty) {
    return "Please enter an Email.";
  } else if (!isEmailFormatValid(email.trim())) {
    return "Please enter valid Email.";
  }
  return null;
}
//DOB Validator
String dobValidator(
    {@required String dob, @required BuildContext context}) {
  if (dob.trim().isEmpty) {
    return "Please select date of birth.";
  }
  return null;
}

String breedValidator(
    {@required String breed, @required BuildContext context}) {
  if (breed.trim().isEmpty) {
    return "Please enter breed.";
  }
  return null;
}

// Validates website field
String websiteValidator(
    {@required String website, @required BuildContext context}) {
  if (website.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterWebsite);
  } else if (!isWebsiteFormatValid(website.trim())) {
    return Localization.of(context).trans(LocalizationValues.enterValidWebsite);
  }
  return null;
}

// Validates cupScore field
String cupScoreValidator(
    {@required String cupScore, @required BuildContext context}) {
  if (cupScore.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterCupScore);
  }
  return null;
}

// Validates destinationPort field
String destinationPortValidator(
    {@required String destinationPortName, @required BuildContext context}) {
  if (destinationPortName.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterDestinationPortName);
  }
  return null;
}

// Validates loadingPort field
String loadingPortValidator(
    {@required String loadingPortName, @required BuildContext context}) {
  if (loadingPortName.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterLoadingPortName);
  }
  return null;
}

// Validates warehouse field
String warehouseValidator(
    {@required String warehouse, @required BuildContext context}) {
  if (warehouse.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterWarehouse);
  }
  return null;
}

// Validates millingProcess field
String millingProcessValidator(
    {@required String millingProcess, @required BuildContext context}) {
  if (millingProcess.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterMillingProcess);
  }
  return null;
}

// Validates additionalNotes field
String additionalNotesValidator(
    {@required String additionalNotes, @required BuildContext context}) {
  if (additionalNotes.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterAdditionalNotes);
  }
  return null;
}

// Validates grossWeight field
String parchmentWeightValidator({
  @required String parchmentWeight,
  @required String maxLimit,
  @required BuildContext context,
}) {
  if (parchmentWeight.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterParchmentWeight);
  }
//  else if ((double.tryParse(grossWeight) ?? 0) >
//      (double.tryParse(maxLimit) ?? 0)) {
//    return Localization.of(context).trans(LocalizationValues.grossWeightCannotGreater)+" $maxLimit.";
//  }

  return null;
}

// Validates netWeight field
String netWeightValidator({
  @required String netWeight,
  @required String maxLimit,
  @required BuildContext context,
}) {
//  if (grossWeight.trim().isEmpty) {
//    return Localization.of(context).trans(LocalizationValues.enterGrossWeight);
//  }
//  else
  if ((double.tryParse(netWeight) ?? 0) > (double.tryParse(maxLimit) ?? 0)) {
    return Localization.of(context)
            .trans(LocalizationValues.netWeightCannotGreater) +
        " $maxLimit.";
  }

  return null;
}

// Validates greenWeight field
String greenWeightValidator({
  @required String greenWeight,
  @required String maxLimit,
  @required BuildContext context,
}) {
//  if (grossWeight.trim().isEmpty) {
//    return Localization.of(context).trans(LocalizationValues.enterGrossWeight);
//  }
//  else
  if ((double.tryParse(greenWeight) ?? 0) > (double.tryParse(maxLimit) ?? 0)) {
    return Localization.of(context)
            .trans(LocalizationValues.greenWeightCannotGreater) +
        " $maxLimit.";
  }

  return null;
}

// Validates harvestMonth field
String harvestMonthValidator(
    {@required String harvestMonth, @required BuildContext context}) {
  if (harvestMonth.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterHarvestMonth);
  }
  return null;
}

// Validates farmerCancelOrder field
String farmerCancelOrderValidator(
    {@required String reason, @required BuildContext context}) {
  if (reason.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterReason);
  }
  return null;
}

// Validates reasonForRejection field
String reasonForRejectionValidator(
    {@required String reason, @required BuildContext context}) {
  if (reason.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterReasonForRejection);
  }
  return null;
}

// Validates qrCode field
String qrCodeValidator(
    {@required String reason, @required BuildContext context}) {
  if (reason.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.pleaseEnterQRCode);
  }
  return null;
}

// Validates lot number field
String lotNumberValidator(
    {@required String reason, @required BuildContext context}) {
  if (reason.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.pleaseEnterLotNumber);
  }
  return null;
}

// Validates moistureContent field
String moistureContentValidator(
    {@required String moistureContent, @required BuildContext context}) {
  if (moistureContent.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterMoistureContent);
  }
  return null;
}

// Validates factor field
String factorValidator(
    {@required String factor, @required BuildContext context}) {
  if (factor.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterFactor);
  }
  return null;
}

// Validates profile picture field
String profilePictureValidator({
  @required String profilePicturePath,
  @required BuildContext context,
}) {
  if (profilePicturePath.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.selectProfilePicture);
  }
  return null;
}

// Validates farm picture field
String farmPicValidator({
  @required BuildContext context,
}) {
  return Localization.of(context).trans(LocalizationValues.atleastOneFarmPic);
}

// Validates region field
String regionValidator({
  @required String region,
  @required BuildContext context,
}) {
  if (region.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.pleaseSelectRegion);
  }
  return null;
}

// Validates variety field
String varietyValidator({
  @required String variety,
  @required BuildContext context,
}) {
  if (variety.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.pleaseSelectVariety);
  }
  return null;
}

// Validates process field
String processValidator({
  @required String process,
  @required BuildContext context,
}) {
  if (process.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.pleaseSelectProcess);
  }
  return null;
}

// Validates certification field
String certificationValidator({
  @required String certification,
  @required BuildContext context,
}) {
  if (certification.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.pleaseSelectCertifications);
  }
  return null;
}

// Validates dropDown field
String dropDownValidator({
  @required String value,
  @required BuildContext context,
  @required String label,
}) {
  if (value.trim().toUpperCase() ==
      Localization.of(context).trans(LocalizationValues.select).toUpperCase()) {
    return Localization.of(context).trans(LocalizationValues.pleaseSelect) +
        " $label.";
  }
  return null;
}

// Validates email/Phone field
String emailPhoneValidator(
    {@required String email, @required BuildContext context}) {
  print('Localization.of(context)--->${Localization.of(context)}');
  if (email.trim().isEmpty) {
    return "Please enter an Email.";
  } else if (!isEmailFormatValid(email.trim())) {
    return  "Please enter valid Email.";
  }
  return null;
}

// Validates userName/Phone field
String userNamePhoneValidator(
    {@required String userName, @required BuildContext context}) {
  if (userName.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterUserNamePhoneNo);
  }
//  else if (!isEmailFormatValid(userName.trim())) {
//    int phoneNumber = int.tryParse(userName.trim()) ?? null;
//    return phoneNumber != null
//        ? phoneNumberValidator(
//            context: context,
//            phoneNumber: userName,
//          )
//        : Localization.of(context).trans(LocalizationValues.enterValidEmail);
//  }
  return null;
}

// Validates minElevation field
String minElevationValidator({
  @required String minElevation,
  @required BuildContext context,
  @required int elevationLowerLimit,
  @required int elevationUpperLimit,
  @required int maxElevation,
}) {
  int enteredMinElevation = int.tryParse(minElevation.trim()) ?? null;

  if (minElevation.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterMinElevation);
  } else if (!(enteredMinElevation >= elevationLowerLimit &&
      enteredMinElevation <= elevationUpperLimit)) {
    if (enteredMinElevation <= elevationLowerLimit) {
      return Localization.of(context)
              .trans(LocalizationValues.minElevationCannotBeLesser) +
          " $elevationLowerLimit.";
    }
    return Localization.of(context)
            .trans(LocalizationValues.minElevationCannotBeGreater) +
        " $elevationUpperLimit.";
  } else if (!(enteredMinElevation < (maxElevation ?? elevationUpperLimit))) {
    return Localization.of(context)
        .trans(LocalizationValues.minElevationMustBeSmaller);
  }
  return null;
}

// Validates maxElevation field
String maxElevationValidator({
  @required String maxElevation,
  @required BuildContext context,
  @required int elevationLowerLimit,
  @required int elevationUpperLimit,
  @required int minElevation,
}) {
  int enteredMaxElevation = int.tryParse(maxElevation.trim()) ?? null;

  if (maxElevation.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterMaxElevation);
  } else if (!(enteredMaxElevation >= elevationLowerLimit &&
      enteredMaxElevation <= elevationUpperLimit)) {
    if (enteredMaxElevation < elevationLowerLimit) {
      return Localization.of(context)
              .trans(LocalizationValues.maxElevationCannotBeLesser) +
          " $elevationLowerLimit.";
    }
    return Localization.of(context)
            .trans(LocalizationValues.maxElevationCannotBeGreater) +
        " $elevationUpperLimit.";
  } else if (!(enteredMaxElevation > (minElevation ?? elevationLowerLimit))) {
    return Localization.of(context)
        .trans(LocalizationValues.maxElevationMustBeGreater);
  }
  return null;
}

// Validates farm size field
String farmSizeValidator({
  @required String farmSize,
  @required BuildContext context,
  @required int farmSizeLowerLimit,
  @required int farmSizeUpperLimit,
}) {
  int enteredFarmSize = int.tryParse(farmSize.trim()) ?? null;

  if (farmSize.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterFarmSize);
  } else if (!(enteredFarmSize >= farmSizeLowerLimit &&
      enteredFarmSize <= farmSizeUpperLimit)) {
    if (enteredFarmSize < farmSizeLowerLimit) {
      return Localization.of(context)
              .trans(LocalizationValues.farmSizeCannotBeLesser) +
          " $farmSizeLowerLimit.";
    }
    return Localization.of(context)
            .trans(LocalizationValues.farmSizeCannotBeGreater) +
        " $farmSizeUpperLimit.";
  }
  return null;
}

// Validates phone number field
String phoneNumberValidator(
    {@required String phoneNumber, @required BuildContext context}) {
  if (phoneNumber.trim().isEmpty) {
    return "Please enter Phone No.";
  } else if (!isNameLengthValid(phoneNumber.trim())) {
    return "Please enter phone no. between 6-15 characters.";
  }
  return null;
}
// Validates username field
String userNameValidator(
    {@required String username,
    @required BuildContext context,
    bool isAvailable}) {
  if (username.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterUsername);
  } else if (!isUsernameLengthValid(username.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.username4To15Chars);
  } else if (!isAvailable) {
    return Localization.of(context).trans(LocalizationValues.usernameTaken);
  }
  return null;
}

// Validated "entered password" field
String enteredPasswordValidator(
    {@required String enteredPassword, @required BuildContext context}) {
  if (enteredPassword.isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterPwd);
  }
  return null;
}

// Validates password format
bool isPasswordLengthValid(String password) {
  if (password.length >= 8 && password.length <= 16) {
    return true;
  }
  return false;
}

// Validates password complexity
bool isPasswordComplexEnough(String password) {
  String p = r"^(?=(.*[0-9]))(?=(.*[A-Za-z]))";
  RegExp regExp = new RegExp(p);
  return regExp.hasMatch(password);
}

// Validates "new password" field
String passwordValidator(
    {@required String newPassword, @required BuildContext context}) {
  if (newPassword.isEmpty) {
    return "Please enter password.";
  } else if (!isPasswordLengthValid(newPassword)) {
    return "Please enter password between 8 to 16 characters.";
  } else if (!isPasswordComplexEnough(newPassword)) {
    return "Your password should include atleast\none alphabet and a number.";
  }
  return null;
}

// Validates "new password" field
String confirmPasswordValidator(
    {@required String confirmPassword,@required String password, @required BuildContext context}) {
  if (confirmPassword.isEmpty) {
    return "Please enter confirm password.";
  } else if (!isPasswordLengthValid(confirmPassword)) {
    return "Please enter password between 8 to 16 characters.";
  } else if (password.isEmpty) {
    return "Please enter password.";
  } else if (password != confirmPassword) {
    return "Password and confirm password do not match.";
  }
  return null;
}

// Validates "new password" field
String newPasswordValidator(
    {@required String newPassword, @required BuildContext context}) {
  if (newPassword.isEmpty) {
    return "Please enter password.";
  } else if (!isPasswordLengthValid(newPassword)) {
    return "Please enter password between 8 to 16 characters.";
  } else if (!isPasswordComplexEnough(newPassword)) {
    return "Your password should include atleast\none alphabet and a number.";
  }
  return null;
}

// Validates "current password" field
String currentPasswordValidator(
    {@required String currentPassword, @required BuildContext context}) {
  if (currentPassword.isEmpty) {
    return "Please enter current password.";
  } else if (!isPasswordLengthValid(currentPassword)) {
    return "Please use at least 6 characters for password.";
  }
  return null;
}

// Validates name format
bool isNameFormatValid(String name) {
  String p = r"^[a-zA-Z]*$";
  RegExp regExp = new RegExp(p);
  return regExp.hasMatch(name);
}

// Validates name length
bool isNameLengthValid(String name) {
  if (name.trim().length >= 2 && name.trim().length <= 45) {
    return true;
  }
  return false;
}

// Validates username length
bool isUsernameLengthValid(String name) {
  if (name.trim().length >= 4
  // todo: uncomment to restrict length
//      && name.trim().length <= 15
  ) {
    return true;
  }
  return false;
}

// Validates phone length
bool isPhoneNumberLengthValid(String phoneNumber) {
  if (phoneNumber.trim().length >= 6 && phoneNumber.trim().length <= 15) {
    return true;
  }
  return false;
}

// Validates first name
String firstNameValidator(
    {@required String firstName, @required BuildContext context}) {
  if (firstName.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterFirstName);
  } else if (!isNameLengthValid(firstName.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.firstName2To45Chars);
  } else if (!isNameFormatValid(firstName.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.noSpecialCharAllowed);
  }
  return null;
}

// Validates  name
String nameValidator({@required String name, @required BuildContext context}) {
  if (name.trim().isEmpty) {
    return "Please enter name.";
  } else if (!isNameLengthValid(name.trim())) {
    return "Please enter name between 2-45 characters.";
  }
  return null;
}

// Validates last name
String lastNameValidator(
    {@required String lastName, @required BuildContext context}) {
  if (lastName.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterLastName);
  } else if (!isNameLengthValid(lastName.trim())) {
    return 'Please enter last name between 2-45 characters.';
  } else if (!isNameFormatValid(lastName.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.noSpecialCharAllowed);
  }
  return null;
}

// Validates  company name
String companyNameValidator(
    {@required String companyName, @required BuildContext context}) {
  if (companyName.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterCompanyName);
  } else if (!isNameLengthValid(companyName.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.companyName2To45Chars);
  }
  return null;
}

// Validates  farm name
String farmNameValidator(
    {@required String farmName, @required BuildContext context}) {
  if (farmName.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterFarmName);
  } else if (!isNameLengthValid(farmName.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.farmName2To45Chars);
  }
  return null;
}

// Validates  mill name
String millNameValidator(
    {@required String millName, @required BuildContext context}) {
  if (millName.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterMillName);
  } else if (!isNameLengthValid(millName.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.millName2To45Chars);
  }
  return null;
}

// Validates  cafe store name
String cafeStoreNameValidator(
    {@required String cafeStoreName, @required BuildContext context}) {
  if (cafeStoreName.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterCafeStoreName);
  } else if (!isNameLengthValid(cafeStoreName.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.cafeStoreName2To45Chars);
  }
  return null;
}

// Validates  coop name
String coopNameValidator(
    {@required String coopName, @required BuildContext context}) {
  if (coopName.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterCoopName);
  } else if (!isNameLengthValid(coopName.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.coopName2To45Chars);
  }
  return null;
}

// Validates  contact name
String contactNameValidator(
    {@required String contactName, @required BuildContext context}) {
  if (contactName.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterContactName);
  } else if (!isNameLengthValid(contactName.trim())) {
    return Localization.of(context)
        .trans(LocalizationValues.contactName2To45Chars);
  }
  return null;
}

// Validates  description name
String descriptionValidator(
    {@required String description, @required BuildContext context}) {
  if (description.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.pleaseEnterDescription);
  }
//  else if (!(description.trim().length > 500)) {
//    return Localization.of(context)
//        .trans(LocalizationValues.description500To5000Chars);
//  }
  return null;
}

// Validates  Street address name
String streetAddressValidator(
    {@required String streetAddress, @required BuildContext context}) {
  if (streetAddress.trim().isEmpty) {
    return Localization.of(context)
        .trans(LocalizationValues.enterStreetAddress);
  }

  return null;
}

// Validates  city
String cityValidator({@required String city, @required BuildContext context}) {
  if (city.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterCity);
  }

  return null;
}

// Validates  stateRegion
String stateRegionValidator(
    {@required String stateRegion, @required BuildContext context}) {
  if (stateRegion.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterStateRegion);
  }
  return null;
}

// Validates  zipCode
String zipCodeValidator(
    {@required String zipCode, @required BuildContext context}) {
  if (zipCode.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterZipCode);
  }
  return null;
}

// Validates  country
String countryValidator(
    {@required String country, @required BuildContext context}) {
  if (country.trim().isEmpty) {
    return Localization.of(context).trans(LocalizationValues.enterCountry);
  }
  return null;
}
