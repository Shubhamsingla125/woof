class LocalizationValues {
  LocalizationValues._();

  // Alert error messages
  static const String severNotFoundError = "severNotFoundError";
  static const String phNumberNotValidError = "phNumberNotValidError";
  static const String noInternetError = "noInternetError";
  static const String timeoutError = "timeoutError";
  static const String genericError = "genericError";
  static const String badFormatError = "badFormatError";
  static const String invalidLoginCredentials = "invalidLoginCredentials";
  static const String accountVerificationRequired =
      "accountVerificationRequired";
  static const String invalidOTP = "invalidOTP";
  static const String userNotFound = "userNotFound";
  static const String paymentFailedError = "paymentFailedError";
  static const String unauthorizedError = "unauthorizedError";
  static const String profileUpdateSuccess = "profileUpdateSuccess";
  static const String foundQuoteFormData = "foundQuoteFormData";

  //Validation messages
  static const String enterEmailPhoneNo = "enterEmailPhoneNo";
  static const String enterUserNamePhoneNo = "enterUserNamePhoneNo";
  static const String enterEmail = "enterEmail";
  static const String enterValidEmail = "enterValidEmail";
  static const String enterWebsite = "enterWebsite";
  static const String enterValidWebsite = "enterValidWebsite";
  static const String enterPhoneNo = "enterPhoneNo";
  static const String enterUsername = "enterUsername";
  static const String enterPwd = "enterPwd";
  static const String enterPwdBtw = "enterPwdBtw";
  static const String pwdContainAlphabetAndNum = "pwdContainAlphabetAndNum";
  static const String enterCurrentPwd = "enterCurrentPwd";
  static const String sixCharForPwd = "sixCharForPwd";

  static const String enterFirstName = "enterFirstName";
  static const String noSpecialCharAllowed = "noSpecialCharAllowed";
  static const String enterName = "enterName";
  static const String enterLastName = "enterLastName";
  static const String enterCompanyName = "enterCompanyName";
  static const String enterFarmName = "enterFarmName";
  static const String enterCafeStoreName = "enterCafeStoreName";
  static const String enterContactName = "enterContactName";
  static const String enterStreetAddress = "enterStreetAddress";
  static const String enterCity = "enterCity";
  static const String enterStateRegion = "enterStateRegion";
  static const String enterZipCode = "enterZipCode";
  static const String enterCountry = "enterCountry";
  static const String needToAgreeTerms = "needToAgreeTerms";

  static const String enterDestinationPortName = "enterDestinationPortName";
  static const String enterLoadingPortName = "enterLoadingPortName";
  static const String enterWarehouse = "enterWarehouse";

  //Alert titles
  static const String error = "error";
  static const String ok = "ok";
  static const String retry = "retry";
  static const String comingSoon = "comingSoon";

  // Login
  static const String email = "email";
  static const String userName = "userName";
  static const String login = "login";
  static const String password = "password";
  static const String oldPassword = "oldPassword";
  static const String forgotPassword = "forgotPassword";
  static const String signin = "signin";
  static const String signup = "signup";
  static const String newUser = "newUser";

  // User type
  static const String exporter = "EXPORTER";
  static const String mill = "MILL";
  static const String farmer = "FARMER";
  static const String importer = "IMPORTER";
  static const String roaster = "ROASTER";
  static const String cafeStore = "CAFE/STORE";
  static const String coops = "COOPS";
  static const String customer = "CUSTOMER";

  // Sign up
  static const String name = "name";
  static const String companyName = "Company Name";
  static const String millName = "millName";
  static const String farmName = "Farm Name";
  static const String cafeStoreName = "Cafe/Store Name";
  static const String phoneNumber = "Phone Number";
  static const String contactName = "Contact Name";
  static const String streetAddress = "Street Address";
  static const String farmAddress = "Farm Address";
  static const String city = "City";
  static const String stateregion = "State/Region";
  static const String zipCode = "Zip Code";
  static const String country = "Country";
  static const String signUpLogin = "signUpLogin";
  static const String existingUser = "existingUser";
  static const String signupBtn = "signupBtn";
  static const String as = "as";

  //Forgot Password
  static const String send = "send";
  static const String forgotYourPwd = "forgotYourPwd";
  static const String enterEmailToReceivePwd = "enterEmailToReceivePwd";

  //OTP
  static const String submit = "submit";
  static const String sec = "sec";
  static const String pleaseEnterOTP = "pleaseEnterOTP";
  static const String clickOnResendBtn = "clickOnResendBtn";
  static const String resendOtp = "resendOtp";
  static const String newPassword = "newPassword";
  static const String confirmPassword = "confirmPassword";
  static const String done = "done";
  static const String resetPassword = "resetPassword";
  static const String changePassword = "changePassword";
  static const String warehouse = "warehouse";
  static const String warehouses = "warehouses";
  static const String noWarehousesFound = "noWarehousesFound";
  static const String addWarehouse = "addWarehouse";
  static const String loadingPorts = "loadingPorts";
  static const String noLoadingPortsFound = "noLoadingPortsFound";
  static const String searchLoadingPorts = "searchLoadingPorts";
  static const String destinationPorts = "destinationPorts";
  static const String noDestinationPortsFound = "noDestinationPortsFound";
  static const String searchDestinationPorts = "searchDestinationPorts";
  static const String updateWarehouse = "updateWarehouse";
  static const String destinationPortName = "destinationPortName";
  static const String loadingPortName = "loadingPortName";
  static const String enterNewPasswordBelow = "enterNewPasswordBelow";
  static const String success = "success";
  static const String pwdUpdatedSuccessfully = "pwdUpdatedSuccessfully";
  static const String confirmPwd = "pleaseConfirmPwd";
  static const String confirmPwdSamePwd = "confirmPwdSamePwd";

  //Confirmation User
  static const String loginAsExistingUser = "loginAsExistingUser";
  static const String waitForConfirmationIfinca = "waitForConfirmationIfinca";
  static const String getBackShortly = "getBackShortly";
  static const String iAgree = "iAgree";
  static const String termsCondition = "termsCondition";
  static const String privacyPolicy = "privacyPolicy";

  // Company logo
  static const String uploadLogo = "uploadLogo";
  static const String pleaseSelectImage = "pleaseSelectImage";
  static const String additionalDocs = "additionalDocs";
  static const String additionalPics = "additionalPics";
  static const String noFilesAdded = "noFilesAdded";
  static const String fileSizeCheck = "fileSizeCheck";
  static const String invalidDocFileFormat = "invalidDocFileFormat";
  static const String skipAndUploadLater = "skipAndUploadLater";
  static const String MillsLogo = "MillsLogo";
  static const String CafeStoresLogo = "CafeStoresLogo";
  static const String CompanysLogo = "CompanysLogo";
  static const String Logo = "Logo";

  // Contact us
  static const String contactUs = "contactUs";
  static const String subject = "subject";
  static const String concern = "concern";
  static const String pleaseEnterSubject = "pleaseEnterSubject";
  static const String pleaseEnterConcern = "pleaseEnterConcern";

  // Add asset
  static const String add = "ADD";
  static const String update = "update";

  // Assets
  static const String addAssets = "addAssets";
  static const String skip = "skip";
  static const String exit = "exit";

  // Farmers asset tab
  static const String farmers = "FARMERS";
  static const String noFarmersFound = "noFarmersFound";
  static const String searchFarmers = "searchFarmers";

  // Mills asset tab
  static const String mills = "MILLS";
  static const String noMillsFound = "noMillsFound";
  static const String searchMills = "searchMills";

  // CafeStores asset tab
  static const String noCafeStoresFound = "noCafeStoresFound";
  static const String searchCafeStore = "searchCafeStore";

  // Coops asset tab
  static const String noCoopsFound = "noCoopsFound";
  static const String searchCoops = "searchCoops";

  // Exporters asset tab
  static const String noExportersFound = "noExportersFound";
  static const String searchExporters = "searchExportes";

  // Importers asset tab
  static const String noImportersFound = "noImportersFound";
  static const String searchImporters = "searchImporters";

  // Roasters Assets tab
  static const String noRoasterFound = "noRoasterFound";
  static const String searchRoasters = "searchRoasters";

  // Consumer dashboard
  static const String dashboard = "dashboard";
  static const String ourSocialMedia = "ourSocialMedia";
  static const String ourStories = "ourStories";
  static const String scan = "scan";
  static const String scanHistory = "scanHistory";
  static const String scannedDate = "scannedDate";
  static const String scannedAt = "scannedAt";

  // Settings
  static const String uniqueId = "uniqueId";
  static const String Id = "Id";
  static const String passwordChangeSuccessfully = "passwordChangeSuccessfully";
  static const String cameraNotAuthorised = "cameraNotAuthorised";
  static const String filesNotAuthorised = "filesNotAuthorised";

  // Dashboard
  static const String home = "home";
  static const String orders = "orders";
  static const String assets = "assets";
  static const String settings = "settings";
  static const String HOME = "HOME";
  static const String ORDERS = "ORDERS";
  static const String ASSETS = "ASSETS";
  static const String SETTINGS = "SETTINGS";

  // Edit profile
  static const String EDIT_PROFILE = "EDITPROFILE";
  static const String SAVE = "SAVE";
  static const String CHANGE_PHONE_NUMBER = "CHANGEPHONENUMBER";
  static const String ADD_EMAIL = "ADDEMAIL";
  static const String CHANGE_EMAIL = "CHANGEEMAIL";
  static const String DETAILS = "DETAILS";
  static const String NOTIFICATIONS = "NOTIFICATIONS";
  static const String BLOG_NOTIFICATIONS = "BLOG_NOTIFICATIONS";
  static const String ORDER_DETAILS = "ORDERDETAILS";

  // Farm details
  static const String ADD_DETAILS = "ADDDETAILS";
  static const String coopName = "coopName";
  static const String location = "location";
  static const String description = "description";
  static const String pleaseEnterDescription = "pleaseEnterDescription";
  static const String description500To5000Chars = "description500To5000Chars";
  static const String region = "region";
  static const String pleaseSelectRegion = "pleaseSelectRegion";
  static const String variety = "variety";
  static const String pleaseSelectVariety = "pleaseSelectVariety";
  static const String process = "process";
  static const String pleaseSelectProcess = "pleaseSelectProcess";
  static const String certifications = "certifications";
  static const String pleaseSelectCertifications = "pleaseSelectCertifications";
  static const String select = "select";
  static const String elevationMetres = "elevationMetres";
  static const String enterMinElevation = "enterMinElevation";
  static const String enterMaxElevation = "enterMaxElevation";
  static const String minElevationCannotBeLesser = "minElevationCannotBeLesser";
  static const String minElevationCannotBeGreater =
      "minElevationCannotBeGreater";
  static const String minElevationMustBeSmaller = "minElevationMustBeSmaller";
  static const String maxElevationCannotBeLesser = "maxElevationCannotBeLesser";
  static const String maxElevationCannotBeGreater =
      "maxElevationCannotBeGreater";
  static const String maxElevationMustBeGreater = "maxElevationMustBeGreater";

  static const String farmSizeHectare = "farmSizeHectare";
  static const String farmSize = "farmSize";
  static const String enterFarmSize = "enterFarmSize";
  static const String farmSizeCannotBeLesser = "farmSizeCannotBeLesser";
  static const String farmSizeCannotBeGreater = "farmSizeCannotBeGreater";
  static const String website = "website";
  static const String weblink = "weblink";
  static const String verify = "verify";
  static const String NewPhoneNumber = "NewPhoneNumber";
  static const String newEmail = "newEmail";
  static const String addProfilePicture = "addProfilePicture";
  static const String addFarmPicture = "addFarmPicture";
  static const String addMorePictures = "addMorePictures";
  static const String rescanQRCode = "rescanQRCode";
  static const String scanQRCode = "scanQRCode";
  static const String QRCode = "QRCode";
  static const String enterOrderNumber = "enterOrderNumber";

  // Order tab
  static const String createOrder = "createOrder";
  static const String updateOrder = "updateOrder";
  static const String newOrder = "newOrder";
  static const String totalOrders = "totalOrders";
  static const String inprogress = "inprogress";
  static const String completed = "completed";
  static const String enterQRCode = "enterQRCode";
  static const String searchOrders = "searchOrders";
  static const String newOrderRequests = "newOrderRequests";
  static const String noOrdersYet = "noOrdersYet";
  static const String millDeliveryDate = "millDeliveryDate";
  static const String portDeliveryDate = "portDeliveryDate";
  static const String pleaseEnterQRCode = "pleaseEnterQRCode";
  static const String qrcode = "qrcode";
  static const String orderNumber = "orderNumber";
  static const String userCommingSoon = "userCommingSoon";
  static const String enterLotNumber = "enterLotNumber";
  static const String pleaseEnterLotNumber = "pleaseEnterLotNumber";
  static const String typeLotCode = "typeLotCode";
  static const String lotNumber = "lotNumber";
  static const String haveQRCode = "haveQRCode";
  static const String noRecordofDate = "noRecordofDate";
  static const String noNotificationsFound = "noNotificationsFound";
  static const String noBlogNotificationsFound = "noBlogNotificationsFound";
  static const String greenWeight = "greenWeight";
  static const String noOfSacks = "noOfSacks";
  static const String farmPrice = "farmPrice";
  static const String totalPrice = "totalPrice";
  static const String ifincaBonus = "ifincaBonus";
  static const String buyer = "buyer";
  static const String Exporter = "Exporter";
  static const String roasterDetails = "roasterDetails";
  static const String markAsReceived = "markAsReceived";
  static const String markAsApproved = "markAsApproved";
  static const String markAsComplete = "markAsComplete";
  static const String orderReady = "orderReady";
  static const String markAsShipped = "markAsShipped";
  static const String deliveryDate = "deliveryDate";
  static const String farm = "farm";
  static const String cupScore = "cupScore";
  static const String screenSize = "screenSize";
  static const String secondaryDefects = "secondaryDefects";
  static const String majorDefects = "majorDefects";
  static const String moisture = "moisture";
  static const String noOrderFound = "noOrderFound";
  static const String Processing = "processing";
  static const String atMill = "atMill";
  static const String readytoship = "readytoship";

  static const String minimum = "minimum";
  static const String maximum = "maximum";
  static const String elevation = "elevation";
  static const String change = "change";
  static const String changePhoneNumber = "changePhoneNumber";
  static const String newPhoneNumber = "newPhoneNumber";
  static const String or = "or";

  static const String noOrderDetailFound = "noOrderDetailFound";
  static const String farmerDetails = "farmerDetails";
  static const String exporterDetails = "exporterDetails";
  static const String cafeStoreDetails = "cafeStoreDetails";
  static const String processing = "processing";
  static const String millContact = "millContact";
  static const String millAddress = "millAddress";
  static const String pleaseSelectFarmerFirst = "pleaseSelectFarmerFirst";
  static const String reassign = "reassign";
  static const String all = "all";
  static const String refresh = "refresh";
  static const String addFarmer = "addFarmer";
  static const String noFarmers = "noFarmers";
  static const String selected = "selected";
  static const String assignFarmers = "assignFarmers";
  static const String pleaseSelectFarmersForMill = "pleaseSelectFarmersForMill";
  static const String pleaseSelectDate = "pleaseSelectDate";
  static const String setDeliveryDateMill = "setDeliveryDateMill";
  static const String sendQRCode = "sendQRCode";
  static const String accept = "accept";
  static const String acceptShippingDocs = "acceptShippingDocs";
  static const String warning = "warning";
  static const String confirmation = "confirmation";
  static const String sureToAcceptOrder = "sureToAcceptOrder";
  static const String sureSatisfyWithOrder = "sureSatisfyWithOrder";
  static const String sureToAcceptsShippingDocs = "sureToAcceptsShippingDocs";
  static const String cancel = "cancel";
  static const String reject = "reject";
  static const String sureToRejectOrder = "sureToRejectOrder";
  static const String shipNow = "shipNow";
  static const String coffeeFOB = "coffeeFOB";
  static const String pleaseSpecifyQuantityCanDeliver =
      "pleaseSpecifyQuantityCanDeliver";
  static const String quantityMustGreaterThanZero =
      "quantityMustGreaterThanZero";
  static const String pleaseEnterQuantityLessOrEqualToDeficient =
      "pleaseEnterQuantityLessOrEqualToDeficient";
  static const String listOfAcceptingFarmers = "listOfAcceptingFarmers";
  static const String deliveryByFarmers = "deliveryByFarmers";
  static const String howMuchOf = "howMuchOf";
  static const String canYouFill = "canYouFill";
  static const String youHaveFilled = "youHaveFilled";
  static const String sampleRequest = "sampleRequest";
  static const String pending = "pending";
  static const String rejected = "rejected";
  static const String accepted = "accepted";
  static const String expected = "expected";
  static const String deliveryOn = "deliveryOn";
  static const String deliveryTo = "deliveryTo";
  static const String weightFactor = "weightFactor";
  static const String harvestMonth = "harvestMonth";
  static const String CANCEL_ORDER = "CANCEL_ORDER";
  static const String CHECK_DATA_POINTS = "CHECK_DATA_POINTS";
  static const String received_full_payment = "received_full_payment";
  static const String paymentReceivedIs = "paymentReceivedIs";
  static const String doYouConfirm = "doYouConfirm";
  static const String dataPoints = "dataPoints";
  static const String grossWeight = "grossWeight";
  static const String factor = "factor";
  static const String netWeight = "netWeight";
  static const String pricePaid = "pricePaid";
  static const String checkDataPointsCompleteDelivery =
      "checkDataPointsCompleteDelivery";
  static const String rawWeight = "rawWeight";
  static const String reasonForRejection = "reasonForRejection";
  static const String approve = "approve";
  static const String disapprove = "disapprove";
  static const String pleaseMentionReasonForCancellation =
      "pleaseMentionReasonForCancellation";
  static const String waitingForConfirmation = "waitingForConfirmation";
  static const String Exporters = "Exporters";
  static const String buyers = "buyers";
  static const String recievingDate = "recievingDate";
  static const String noOfFarmers = "noOfFarmers";
  static const String moistureContent = "moistureContent";
  static const String UPDATE_DATA_POINTS = "UPDATE_DATA_POINTS";
  static const String SEND_DATA_POINTS = "SEND_DATA_POINTS";
  static const String REQUEST_APPROVAL = "REQUEST_APPROVAL";
  static const String printQRCode = "printQRCode";
  static const String approved = "approved";
  static const String disapproved = "disapproved";
  static const String farmersExpectedToCome = "farmersExpectedToCome";
  static const String additionalNotes = "additionalNotes";
  static const String millingProcess = "millingProcess";
  static const String importerDetails = "importerDetails";
  static const String language = "language";
  static const String pushNotifications = "pushNotifications";
  static const String logout = "logout";
  static const String appVersion = "appVersion";
  static const String languageChangedSuccessfully =
      "languageChangedSuccessfully";
  static const String noDataFound = "noDataFound";
  static const String selectAll = "selectAll";
  static const String other = "other";
  static const String enter = "enter";
  static const String requiredWeight = "requiredWeight";
  static const String locationNotAuthorized = "locationNotAuthorized";
  static const String noAddressFound = "noAddressFound";
  static const String selectYourPhoneCode = "selectYourPhoneCode";
  static const String search = "search";
  static const String galleryNotAuthorized = "galleryNotAuthorized";
  static const String notNow = "notNow";
  static const String chooseOption = "chooseOption";
  static const String importerFee = "importerFee";
  static const String exporterFee = "exporterFee";
  static const String exchangeRate = "exchangeRate";

  // Validators
  static const String enterCupScore = "enterCupScore";
  static const String enterMillingProcess = "enterMillingProcess";
  static const String enterAdditionalNotes = "enterAdditionalNotes";
  static const String enterGrossWeight = "enterGrossWeight";
  static const String enterParchmentWeight = "enterParchmentWeight";
  static const String netWeightCannotGreater = "netWeightCannotGreater";
  static const String greenWeightCannotGreater = "greenWeightCannotGreater";
  static const String enterHarvestMonth = "enterHarvestMonth";
  static const String enterReason = "enterReason";
  static const String enterReasonForRejection = "enterReasonForRejection";
  static const String enterMoistureContent = "enterMoistureContent";
  static const String enterFactor = "enterFactor";
  static const String selectProfilePicture = "selectProfilePicture";
  static const String atleastOneFarmPic = "atleastOneFarmPic";
  static const String pleaseSelect = "pleaseSelect";
  static const String phone6To15Chars = "phone6To15Chars";
  static const String username4To15Chars = "username4To15Chars";
  static const String usernameTaken = "usernameTaken";
  static const String firstName2To45Chars = "firstName2To45Chars";
  static const String name2To45Chars = "name2To45Chars";
  static const String companyName2To45Chars = "companyName2To45Chars";
  static const String farmName2To45Chars = "farmName2To45Chars";
  static const String enterMillName = "enterMillName";
  static const String millName2To45Chars = "millName2To45Chars";
  static const String cafeStoreName2To45Chars = "cafeStoreName2To45Chars";
  static const String enterCoopName = "enterCoopName";
  static const String coopName2To45Chars = "coopName2To45Chars";
  static const String contactName2To45Chars = "contactName2To45Chars";
  static const String cancelled = "cancelled";
  static const String declined = "declined";

  // Main order status
  static const String exporter_accepted = "exporter_accepted";
  static const String farmer_accepted = "farmer_accepted";
  static const String delivered_at_mill = "delivered_at_mill";
  static const String ready_at_mill = "ready_at_mill";
  static const String shipped_from_mill = "shipped_from_mill";
  static const String received_by_importer = "received_by_importer";
  static const String shipped_by_importer = "shipped_by_importer";
  static const String received_by_roaster = "received_by_roaster";
  static const String shipped_by_roaster = "shipped_by_roaster";
  static const String received_by_cafe = "received_by_cafe";
  static const String expired = "expired";

  // Months
  static const String jan = "jan";
  static const String feb = "feb";
  static const String mar = "mar";
  static const String apr = "apr";
  static const String may = "may";
  static const String jun = "jun";
  static const String jul = "jul";
  static const String aug = "aug";
  static const String sep = "sep";
  static const String oct = "oct";
  static const String nov = "nov";
  static const String dec = "dec";

  // Misc.
  static const String parchmentWeight = "parchmentWeight";
  static const String pricePerCarga = "pricePerCarga";
}
