import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SharedPrefsKeys.dart';

class MemoryManagement {
  static SharedPreferences prefs;

  static Future<bool> init() async {
    prefs = await SharedPreferences.getInstance();
    return true;
  }

  static void setAccessToken({@required String accessToken}) {
    prefs.setString(SharedPrefsKeys.ACCESS_TOKEN, accessToken);
  }

  static String getAccessToken() {
    return prefs.getString(SharedPrefsKeys.ACCESS_TOKEN);
  }

  static void setDeviceId({@required String deviceID}) {
    prefs.setString(SharedPrefsKeys.DEVICE_ID, deviceID);
  }

  static String getDeviceId() {
    return prefs?.getString(SharedPrefsKeys.DEVICE_ID);
  }

  static void setUserInfo({@required String userInfo}) {
    prefs.setString(SharedPrefsKeys.USER_INFO, userInfo);
  }

  static String getUserInfo() {
    return prefs.getString(SharedPrefsKeys.USER_INFO);
  }

  static void setUserLoggedIn({@required bool isUserLoggedin}) {
    prefs.setBool(SharedPrefsKeys.IS_USER_LOGGED_IN, isUserLoggedin);
  }

  static bool getUserLoggedIn() {
    print("prefs---->$prefs");
    return prefs?.getBool(SharedPrefsKeys.IS_USER_LOGGED_IN);
  }

  static void setUserType({@required String userType}) {
    prefs.setString(SharedPrefsKeys.USER_TYPE, userType);
  }

  static String getUserType() {
    return prefs?.getString(SharedPrefsKeys.USER_TYPE);
  }

  static void setUserLanguage({@required String languageCode}) {
     prefs.setString(SharedPrefsKeys.LANGUAGE_CODE, languageCode);
  }

  static String getUserLanguage() {
    return prefs?.getString(SharedPrefsKeys.LANGUAGE_CODE);
  }

  static void setUserPushNotificationsStatus({@required int status}) {
     prefs.setInt(SharedPrefsKeys.PUSH_NOTIFICATIONS_STATUS, status);
  }

  static bool getUserPushNotificationsStatus() {
    return prefs?.getInt(SharedPrefsKeys.PUSH_NOTIFICATIONS_STATUS) == 1;
  }

  static void setDeepLinkTimestamp({@required int timestamp}) {
     prefs.setInt(SharedPrefsKeys.DEEPLINK_TIMESTAMP, timestamp);
  }

  static int getDeepLinkTimestamp() {
    return prefs?.getInt(SharedPrefsKeys.DEEPLINK_TIMESTAMP);
  }


  static void setConfirmationUser({@required bool isConfirmUser}) {
    prefs.setBool(SharedPrefsKeys.CONFIRMATION_USER, isConfirmUser);
  }

  static bool getConfirmationUser() {
    return prefs.getBool(SharedPrefsKeys.CONFIRMATION_USER);
  }


  // Paginating list data storage

  // ASSET_MILLS
  static void setAssetMills({@required String millsData}) {
    prefs.setString(SharedPrefsKeys.ASSET_MILLS, millsData);
  }
  static String getAssetMills() {
    return prefs.getString(SharedPrefsKeys.ASSET_MILLS);
  }

  // ASSET_FARMERS
  static void setAssetFarmers({@required String farmersData}) {
    prefs.setString(SharedPrefsKeys.ASSET_FARMERS, farmersData);
  }
  static String getAssetFarmers() {
    return prefs.getString(SharedPrefsKeys.ASSET_FARMERS);
  }

  // ASSET_EXPORTERS
  static void setAssetExporters({@required String exportersData}) {
    prefs.setString(SharedPrefsKeys.ASSET_EXPORTERS, exportersData);
  }
  static String getAssetExporters() {
    return prefs.getString(SharedPrefsKeys.ASSET_EXPORTERS);
  }

  // ASSET_COOPS
  static void setAssetCoops({@required String coopsData}) {
    prefs.setString(SharedPrefsKeys.ASSET_COOPS, coopsData);
  }
  static String getAssetCoops() {
    return prefs.getString(SharedPrefsKeys.ASSET_COOPS);
  }

  // ASSET_IMPORTERS
  static void setAssetImporters({@required String importersData}) {
    prefs.setString(SharedPrefsKeys.ASSET_IMPORTERS, importersData);
  }
  static String getAssetImporters() {
    return prefs.getString(SharedPrefsKeys.ASSET_IMPORTERS);
  }

  // ASSET_CAFE_STORES
  static void setAssetCafeStores({@required String cafeStoresData}) {
    prefs.setString(SharedPrefsKeys.ASSET_CAFE_STORES, cafeStoresData);
  }
  static String getAssetCafeStores() {
    return prefs.getString(SharedPrefsKeys.ASSET_CAFE_STORES);
  }

  // ASSET_ROASTER
  static void setAssetRoasters({@required String roastersData}) {
    prefs.setString(SharedPrefsKeys.ASSET_ROASTERS, roastersData);
  }
  static String getAssetRoasters() {
    return prefs.getString(SharedPrefsKeys.ASSET_ROASTERS);
  }


  //clear all values from shared preferences
  static void clearMemory() {
    prefs.clear();
  }
}
