import 'package:intl/intl.dart';

class Constants {
  static final DateFormat defaultDisplayDateFormat = DateFormat.yMMMEd();
  static final int itemsPerPage = 10;
}

class Column {
  static const id = "id";
  static const info = "info";
  static const createdBy = "c_by";
  static const updatedBy = "c_by";
  static const createdAt = "c_at";
  static const updatedAt = "u_at";
  static const email = "email";
  static const avatar = "avatar";
  static const name = "name";
  static const title = "title";
  static const detail = "detail";
}
// app specific
