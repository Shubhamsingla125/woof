import 'package:woof_fini/utils/app_remote_config.dart';
import 'package:woof_fini/utils/base_current_session.dart';
import 'local_data.dart';

class CurrentSession extends BaseCurrentSession<LocalData> {
  static final CurrentSession _currentSession = CurrentSession._internal();

  CurrentSession(LocalData localData) : super();

  CurrentSession._internal() : super();

  String userId;

//  UserEntity user;

  AppRemoteConfig remoteConfig;

  @override
  void initLocalData(Map<String, dynamic> data) {
    localData = LocalData.fromJson(data);
    remoteConfig = AppRemoteConfig();
  }

  static CurrentSession get i => _currentSession;

  @override
  Map<String, dynamic> localDataToMap() {
    return localData.toMap();
  }

}
