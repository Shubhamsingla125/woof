
import 'package:woof_fini/utils/base/entities/base_event.dart';

class EventType {
  static final int delete = 1;
  static final int update = 2;
  static final int create = 3;
  static final int read = 4;
  static final int fetchAllItems = 5;
  static final int fetchItemsFromStart = 6;
  static final int fetchMoreItems = 7;
}

/// Basic CRUD operation for Entity
class Event<D> extends BaseEvent<int, D> {
  D _data;
  int _type;

  Event(this._type, this._data);

  Event.type(this._type);

  @override
  D get data => _data;

  @override
  int get type => _type;
}
