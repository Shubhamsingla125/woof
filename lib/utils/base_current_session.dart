import 'dart:convert';
import 'package:woof_fini/utils/preference_helper.dart';

abstract class BaseCurrentSession<T> {
  T localData;
  int _keyLocalData = 99;

  BaseCurrentSession() {
    String data = PreferenceHelper.i.readString(_keyLocalData);
    if (data.isEmpty || data == null) {
      initLocalData(null);
    } else {
      initLocalData(json.decode(data));
    }
  }

  void initLocalData(Map<String, dynamic> data);

  void saveDataLocally() {
    final Map<String, dynamic> data = localDataToMap();
    PreferenceHelper.i.putString(_keyLocalData, json.encode(data));
  }

  Map<String, dynamic> localDataToMap();

  void clearLocalData() {
    initLocalData(null);
    saveDataLocally();
  }
}
