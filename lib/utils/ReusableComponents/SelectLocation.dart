import 'dart:async';
import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart' as current;
import 'package:simple_permissions/simple_permissions.dart';

import '../IfincaColors.dart';
import '../Localization.dart';
import '../LocalizationValues.dart';
import '../UniversalFunctions.dart';
import 'PlacesAutoComplete.dart';

class SelectLocation extends StatefulWidget {
  @override
  State<SelectLocation> createState() => SelectLocationState();
}

class SelectLocationState extends State<SelectLocation> {
  // Data props.
  Address addressSelected;

  Completer<GoogleMapController> _controller = Completer();
  final GlobalKey<ScaffoldState> scaffloadKey = new GlobalKey<ScaffoldState>();

  CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(0, 0),
    zoom: 13,
  );
  CameraPosition cameraPosition;
  String address = "No Address found";
  StreamController<String> _addressStreamController =
      StreamController<String>();
  var location = new current.Location();

  bool enableCurrentLocationButton = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkPermissionAndOpenLocation();
  }

  Future getCurrentLocation() async {
    try {
      current.LocationData currentLocation = await location.getLocation();
      final CameraPosition currentPosition = CameraPosition(
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: 13,
      );
      cameraPosition = currentPosition;
      _goToLocation(currentPosition);
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED' ||
          e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        /// Todo: show alert here
        /// open settings alert, with options: SETTINGS , OK
        showAlert(e.message);
      }
    }
    setState(() {
      enableCurrentLocationButton = true;
    });
  }

  checkPermissionAndOpenLocation() async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      setState(() {
        enableCurrentLocationButton = false;
      });
      final res = await SimplePermissions.requestPermission(
          Permission.WhenInUseLocation);
      if (res == PermissionStatus.denied) {
        showSettingAlert(context, "App not authorized to access location");
      } else {
        Future.delayed(const Duration(milliseconds: 500), () {
          getCurrentLocation();
        });
      }
    } on PlatformException {
//      platformVersion = 'Failed to get platform version.';
    }
  }

  showAlert(String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Platform.isIOS
              ? showIOSAlert(
                  "Location",
                  message,
                )
              : showAndriodAlert(
                  "Location",
                  message,
                );
        });
  }

  Widget showAndriodAlert(
    String title,
    String message,
  ) {
    return AlertDialog(
      title: Text(title ?? ""),
      content: Text(message),
      actions: <Widget>[
        FlatButton(
          child: Text('Setting'),
          onPressed: () {
            Navigator.pop(context);
            SimplePermissions.openSettings();
          },
        ),
        FlatButton(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
  }

  Widget showIOSAlert(String title, String message) {
    return new CupertinoAlertDialog(
      title: new Text(title ?? ""),
      content: new Text(message),
      actions: <Widget>[
        CupertinoDialogAction(
          isDefaultAction: true,
          child: Text('Setting'),
          onPressed: () {
            Navigator.pop(context);
            SimplePermissions.openSettings();
          },
        ),
        CupertinoDialogAction(
          child: Text('Cancel'),
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ],
    );
  }

  // Returns Cancel button
  get _getCancelButton {
    return Padding(
      padding: const EdgeInsets.only(left: 10),
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: new Text(
          Localization.of(context).trans(LocalizationValues.cancel),
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  // Returns Cancel button
  get _getDoneButton {
    return Padding(
      padding: const EdgeInsets.only(right: 10),
      child: InkWell(
        onTap: () {
          Navigator.pop(context, addressSelected);
        },
        child: new Text(
          Localization.of(context).trans(LocalizationValues.done),
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  get _getCustomAppBar {
    return new Align(
      alignment: Alignment.topCenter,
      child: new Container(
        width: getScreenSize(context: context).width,
        height: getScreenSize(context: context).height * 0.1,
        color: IfincaColors.kAppBlack,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _getCancelButton,
            Text(
              getFirstLetterCapitalized(
                      source: Localization.of(context)
                          .trans(LocalizationValues.select)) +
                  " " +
                  getFirstLetterCapitalized(
                      source: Localization.of(context)
                          .trans(LocalizationValues.location)),
              style: TextStyle(
                  fontSize: 17.5,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
            _getDoneButton
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      key: scaffloadKey,
      body: SafeArea(
        child: new Stack(
          children: <Widget>[
            new GoogleMap(
              mapType: MapType.normal,
              myLocationEnabled: false,
              myLocationButtonEnabled: false,
              initialCameraPosition: _kGooglePlex,
              onCameraMove: (position) {
                this.cameraPosition = position;
              },
              onCameraIdle: _getAddressFromCoordinates,
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
            new Align(
              alignment: Alignment.center,
              child: new Icon(
                Icons.location_on,
                size: 40.0,
                color: IfincaColors.kAppBlack,
              ),
            ),
            new Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.only(right: 10.0, bottom: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    new Material(
                      shape: StadiumBorder(),
                      elevation: 2.0,
                      child: InkWell(
                        borderRadius: BorderRadius.circular(30),
                        child: Container(
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(shape: BoxShape.circle),
                          child: Icon(
                            Icons.search,
                            color: Colors.black,
                          ),
                        ),
                        onTap: () {
                          showAutoComplete();
                        },
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    new Material(
                      shape: StadiumBorder(),
                      elevation: 2.0,
                      child: InkWell(
                        borderRadius: BorderRadius.circular(30),
                        child: Container(
                          height: 60,
                          width: 60,
                          decoration: BoxDecoration(shape: BoxShape.circle),
                          child: Icon(
                            Icons.my_location,
                            color: enableCurrentLocationButton
                                ? Colors.black
                                : Colors.grey,
                          ),
                        ),
                        onTap: enableCurrentLocationButton
                            ? () {
                                checkPermissionAndOpenLocation();
                              }
                            : null,
                      ),
                    )

//                    new FloatingActionButton(
//                      backgroundColor: Colors.white,
//                      onPressed: () {
//                        showAutoComplete();
//                      },
//                      child: Icon(
//                        Icons.search,
//                        color: Colors.black,
//                      ),
//                    ),
//                    SizedBox(
//                      height: 15,
//                    ),
//                    new FloatingActionButton(
//                      backgroundColor: Colors.white,
//                      onPressed: () {
//                        checkPermissionAndOpenLocation();
//                      },
//                      child: Icon(
//                        Icons.my_location,
//                        color: Colors.black,
//                      ),
//                    ),
                  ],
                ),
              ),
            ),
            StreamBuilder<String>(
                stream: _addressStreamController.stream,
                initialData: Localization.of(context)
                    .trans(LocalizationValues.noAddressFound),
                builder: (context, snapshot) {
                  return new Align(
                    alignment: Alignment.topCenter,
                    child: Column(
                      children: <Widget>[
                        _getCustomAppBar,
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 10.0, right: 10.0, top: 20.0),
                          child: Container(
                            width: size.width - 20,
                            child: Card(
                              elevation: 2.0,
                              color: Colors.white,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new Text(
                                  snapshot.data,
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }

  Future showAutoComplete() async {
    var data =
        await PlacesAutoComplete.shared.showAutoComplete(context, scaffloadKey);

    if (data != null) {
      var detail = await PlacesAutoComplete.shared.getDetail(data);
      final CameraPosition currentPosition = CameraPosition(
        target: LatLng(detail.result.geometry.location.lat,
            detail.result.geometry.location.lng),
        zoom: 13,
      );
      cameraPosition = currentPosition;
      _goToLocation(currentPosition);
    }
  }

  Future _getAddressFromCoordinates() async {
    if (cameraPosition != null) {
      final coordinates = new Coordinates(
          cameraPosition.target.latitude, cameraPosition.target.longitude);
      var addresses = await Geocoder.google(kGoogleApiKey)
          .findAddressesFromCoordinates(coordinates);
      if (addresses.length > 0) {
        address = addresses?.first?.addressLine;
        _addressStreamController.add(address);
        addressSelected = addresses.first;
      }
    }
  }

  Future<void> _goToLocation(CameraPosition position) async {
    final GoogleMapController controller = await _controller.future;
    controller.moveCamera(CameraUpdate.newCameraPosition(position));
  }
}
