import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:flutter_google_places/flutter_google_places.dart';

import '../UniversalFunctions.dart';

const kGoogleApiKey = "AIzaSyBuaO4PRm_gvv8fV3rDp9BuzK-fQl5IZOA";


class PlacesAutoComplete{

  static final PlacesAutoComplete _singleton = PlacesAutoComplete._internal();
  factory PlacesAutoComplete() => _singleton;
  PlacesAutoComplete._internal();
  static PlacesAutoComplete get shared => _singleton;

  GoogleMapsPlaces places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  GlobalKey<ScaffoldState> homeScaffoldKey;
  BuildContext context;

  Future<Prediction> showAutoComplete(BuildContext context,GlobalKey<ScaffoldState> key) async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    homeScaffoldKey = key;
    this.context = context;

    return  await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      onError: onError,
      mode: Mode.overlay,
      language: "en",
      components: [],
    );
  }


  Future <PlacesDetailsResponse> getDetail(Prediction p)=> places.getDetailsByPlaceId(p.placeId);

  void onError(PlacesAutocompleteResponse response) {

      showAlert(
        context: context,
        titleText: "ERROR",
        message: response.errorMessage,
        actionCallbacks: {
          "OK": () {
          }
        },
      );
   
  }
}
