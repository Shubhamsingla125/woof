import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:simple_permissions/simple_permissions.dart';

import '../IfincaColors.dart';
import '../Localization.dart';
import '../LocalizationValues.dart';
import '../UniversalFunctions.dart';

class FilePickerSection extends StatefulWidget {
  final String label;
  final bool disable;
  final List<File> docs;
  final List<String> existingFilePaths;
  final Function(File) onFileSelected;
  final Function(String) onFilePathDeleted;

  const FilePickerSection({
    Key key,
    @required this.label,
    @required this.disable,
    @required this.docs,
    @required this.existingFilePaths,
    @required this.onFileSelected,
    @required this.onFilePathDeleted,
  }) : super(key: key);

  @override
  _FilePickerSectionState createState() => _FilePickerSectionState();
}

class _FilePickerSectionState extends State<FilePickerSection> {
  // Data props.
  List<File> selectedDocs = [];

  @override
  void initState() {
    selectedDocs = widget.docs;
    super.initState();
  }

  // Builds screen
  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: new EdgeInsets.only(
        top: getScreenSize(context: context).height * 0.01,
      ),
      child: new Stack(
        children: <Widget>[
          new Card(
            elevation: 2.0,
            clipBehavior: Clip.hardEdge,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: new Container(
              padding: const EdgeInsets.only(
                left: 3.0,
              ),
              decoration: new BoxDecoration(
                color: IfincaColors.kOrange,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: new Container(
                color: Colors.white,
                padding: const EdgeInsets.symmetric(
                  vertical: 12.0,
                  horizontal: 16.0,
                ),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Expanded(
                          child: new Text(
                            widget.label,
                            style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 14.0,
                            ),
                          ),
                        ),
                        new Offstage(
                          offstage: widget.disable ?? false,
                          child: new InkWell(
                            child: new Icon(
                              Icons.add_circle,
                              color: IfincaColors.kOrange,
                            ),
                            onTap: () async {
                              File file = await _getFile(
                                context: context,
                                fileSizeInMBs: 5,
                              );
                              print("FILE PICKED: ${file?.path}");
                              if (file != null) {
                                setState(() {
                                  selectedDocs.add(file);
                                });
                                if (widget.onFileSelected != null) {
                                  widget.onFileSelected(file);
                                }
                              }
                            },
                          ),
                        ),
                      ],
                    ),
//                    (selectedDocs?.length ?? 0) > 0 ||
                    (widget.existingFilePaths?.length ?? 0) > 0
                        ? new Padding(
                            padding: const EdgeInsets.only(
                              top: 8.0,
                              bottom: 8.0,
                            ),
                            child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[]..addAll(
                                    new List.generate(
                                      widget.existingFilePaths?.length ?? 0,
                                      (int index) {
                                        String fileName =
                                            widget.existingFilePaths[index];
                                        return new Padding(
                                          padding: const EdgeInsets.symmetric(
                                            vertical: 4.0,
                                          ),
                                          child: new Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: <Widget>[
                                              new Expanded(
                                                child: new Text(
                                                  fileName.split("/").last,
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ),
                                              new Offstage(
                                                offstage:
                                                    widget.disable ?? false,
                                                child: new InkWell(
                                                  child: new Icon(
                                                    Icons.cancel,
                                                    color: IfincaColors.kGrey,
                                                    size: 20.0,
                                                  ),
                                                  onTap: () {
                                                    if (widget
                                                            .onFilePathDeleted !=
                                                        null) {
                                                      widget.onFilePathDeleted(
                                                          widget.existingFilePaths[
                                                              index]);
                                                    }
//                                                    setState(() {
//                                                      widget.existingFilePaths
//                                                          ?.removeAt(index);
//                                                    });
                                                  },
                                                ),
                                              ),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  )
//                                ..addAll(
//                                  new List.generate(
//                                    selectedDocs?.length ?? 0,
//                                    (int index) {
//                                      String fileName =
//                                          selectedDocs[index].path;
//                                      return new Padding(
//                                        padding: const EdgeInsets.symmetric(
//                                          vertical: 4.0,
//                                        ),
//                                        child: new Row(
//                                          crossAxisAlignment:
//                                              CrossAxisAlignment.start,
//                                          children: <Widget>[
//                                            new Expanded(
//                                              child: new Text(
//                                                fileName.split("/").last,
//                                                maxLines: 1,
//                                                overflow: TextOverflow.ellipsis,
//                                              ),
//                                            ),
//                                            new Offstage(
//                                              offstage: widget.disable ?? false,
//                                              child: new InkWell(
//                                                child: new Icon(
//                                                  Icons.cancel,
//                                                  color: IfincaColors.kGrey,
//                                                  size: 20.0,
//                                                ),
//                                                onTap: () {
//                                                  setState(() {
//                                                    selectedDocs
//                                                        .removeAt(index);
//                                                  });
//                                                },
//                                              ),
//                                            ),
//                                          ],
//                                        ),
//                                      );
//                                    },
//                                  ),
//                                ),
                                ),
                          )
                        : new Center(
                            child: new Padding(
                              padding: const EdgeInsets.only(
                                top: 12.0,
                                bottom: 12.0,
                                right: 8.0,
                              ),
                              child: new Text(
                                Localization.of(context)
                                    .trans(LocalizationValues.noFilesAdded),
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
            ),
          ),
          new Positioned(
            left: -1,
            bottom: -1,
            top: -1,
            child: new Container(
              height: 12.0,
              width: 12.0,
              decoration: new BoxDecoration(
                color: IfincaColors.kOrange,
                shape: BoxShape.circle,
                border: new Border.all(
                  color: Colors.white,
                  width: 2.0,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  // Methods

  // Gets file with specific limited file size(defaults to 5mb)
  Future<File> _getFile({
    @required BuildContext context,
    int fileSizeInMBs = 5,
  }) async {
    try {
      alreadyAskingPermission = true;
      final res = isAndroid()
          ? await SimplePermissions.requestPermission(
              Permission.ReadExternalStorage)
          : null;
      alreadyAskingPermission = false;
      if ((res == PermissionStatus.deniedNeverAsk && isAndroid()) ||
          (res == PermissionStatus.denied && !isAndroid())) {
        showSettingAlert(
          context,
          Localization.of(context).trans(
            LocalizationValues.filesNotAuthorised,
          ),
        );
      } else {
        String filePath = await FilePicker.getFilePath(type: FileType.ANY);
        print("FILE PATH: $filePath");
        if (filePath == '' || filePath == null) {
          return null;
        }

        String filePathExt = filePath.toLowerCase();

        if (filePathExt.contains('.pdf') ||
            filePathExt.contains('.doc') ||
            filePathExt.contains('.xls')) {
          final File file = await new File('$filePath').create();
          if (file.lengthSync() < fileSizeInMBs * 1024 * 1024) {
            return file;
          } else {
            showAlert(
              context: context,
              titleText:
                  Localization.of(context).trans(LocalizationValues.error),
              message: Localization.of(context)
                  .trans(LocalizationValues.fileSizeCheck),
              actionCallbacks: {
                Localization.of(context).trans(LocalizationValues.ok): () {}
              },
            );
          }
        } else {
          showAlert(
            context: context,
            titleText: Localization.of(context).trans(LocalizationValues.error),
            message: Localization.of(context)
                .trans(LocalizationValues.invalidDocFileFormat),
            actionCallbacks: {
              Localization.of(context).trans(LocalizationValues.ok): () {}
            },
          );
        }
      }
    } catch (e) {
      print("Error while picking the file: " + e.toString());
    }
    return null;
  }
}
