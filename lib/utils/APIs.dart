class APIs {
  APIs._();

  // Base url for testing
//  static const String socketUrl = "http://js-server.debutinfotech.com:3094/";
//  static const String webUrl = "https://www.ifinca.co";

  // Base url for Live
//   static const String baseUrl = "https://api.ifinca.co/v1";
//   static const String socketUrl = "https://api.ifinca.co/";
//   static const String webUrl = "https://www.ifinca.co";

  // Base url for dev
  static const String baseUrl = "http://64.225.15.161:8000/api";
  static const String socketUrl = "http://js-server.debutinfotech.com:3091/";
  static const String webUrl = "https://www.ifinca.co";

  //Socket Methods
  static const String exporterOrderRequestProgress = "exporterOrderRequest_";
  static const String farmerOrderAcceptProgress = "farmerOrderAccept_";
  static const String farmerApproveDataPoints = "farmerApproveDataPoints_";
  static const String millMarkAsReadyToShip = "millMarkAsReadyToShip_";

  //Terms & Condition
  static const String termsConditionsUrl = "$baseUrl/termsConditions";

  //Privacy Policy
  static const String privacyPolicyUrl = "$baseUrl/privacy_policy";

  //Our stories
  static const String ourStoriesUrl = "https://www.ifinca.co";

  //POST
  static const String postListUrl = "$baseUrl/posts";
  static const String createPostUrl = "$baseUrl/post/create";

  // Auth urls
  static const String loginUrl = "$baseUrl/login";
  static const String petCategoriesUrl = "$baseUrl/pet/categories";
  static const String signUpUrl = "$baseUrl/register";
  static const String verifyOTPUrl = "$baseUrl/signup/otp_verify";
  static const String updateDeviceTokenUrl = "$baseUrl/user/device_token";
  static const String verifyChangePhoneEmailOTPUrl =
      "$baseUrl/user/change_phone_email_verify";
  static const String resendOTPUrl = "$baseUrl/resend_otp";
  static const String forgotPasswordUrl = "$baseUrl/forgot_password";
  static const String forgotOTPVerifyUrl =
      "$baseUrl/forgot_password/otp_verify";
  static const String resetPasswordUrl = "$baseUrl/profile";

  static const String changePasswordUrl = "$baseUrl/user/change_password";
  static const String changeLanguageUrl = "$baseUrl/user/language";
  static const String pushNotificationToggleUrl =
      "$baseUrl/user/push_notification";
  static const String changePhoneEmailUrl = "$baseUrl/user/change_phone_email";
  static const String getProfileUrl = "$baseUrl/user";
  static const String profilePicUrl = "$baseUrl/user/profile_pic";
  static const String skipProfileURL = "$baseUrl/user/profile_completed";
  static const String isUserApproved = "$baseUrl/user/is_user_approved";
  static const String getCategoryUrl = "$baseUrl/categories";
  static const String getWarehousesUrl = "$baseUrl/user/get_ware_hosue";
  static const String addWarehouseUrl = "$baseUrl/user/ware_house_add";
  static const String updateWarehouseUrl = "$baseUrl/user/ware_house_edit";
  static const String updateProfileUrl = "$baseUrl/profile";

  // Universal urls
  static const String allAssetsUrl = "$baseUrl/vendors";
  static const String addGlobalAssetsUrl = "$baseUrl/vendors";
  static const String searchAssetsUrl = "$baseUrl/vendors/search";
  static const String addAssetsUrl = "$baseUrl/vendors/vendor_request";
  static const String addToMyLoadingPortUrl =
      "$baseUrl/vendors/add_loading_port";
  static const String myLoadingPortsUrl = "$baseUrl/vendors/my_loading_port";
  static const String searchLoadingPortsUrl =
      "$baseUrl/vendors/search_loading_port";
  static const String addToMyDestinationPortUrl =
      "$baseUrl/vendors/add_destination_port";
  static const String myDestinationPortsUrl =
      "$baseUrl/vendors/my_destination_port";
  static const String searchDestinationPortsUrl =
      "$baseUrl/vendors/search_destination_port";
  static const String contactUsUrl = "$baseUrl/contact_us";
  static const String notificationsListUrl = "$baseUrl/notifications";
  static const String deleteNotificationUrl = "$baseUrl/notifications";
  static const String notificationsCountUrl = "$baseUrl/notifications/count";
  static const String readNotificationUrl = "$baseUrl/notifications";

  static const String blogNotificationsListUrl = "$baseUrl/blog_notification";
  static const String blogDeleteNotificationUrl = "$baseUrl/blog_notification";
  static const String blogNotificationsCountUrl =
      "$baseUrl/blog_notification/count";
  static const String blogReadNotificationUrl =
      "$baseUrl/blog_notification/readnotfication";

  static const String logoutUrl = "$baseUrl/user/logout";
  static const String checkUsernameUrl = "$baseUrl/check_username";

  //Exporter Order Urls
  static const String getExporterOrderRequestsUrl =
      "$baseUrl/exporter_orders/dashboard";
  static const String acceptRejectOrderByExporterUrl =
      "$baseUrl/exporter_orders";
  static const String pendingOrderDetailOfExporterUrl =
      "$baseUrl/exporter_orders/pending_order_details";
  static const String orderDetailOfExporterUrl =
      "$baseUrl/exporter_orders/details";
  static const String getOrderListOfExporterUrl = "$baseUrl/exporter_orders";
  static const String createOrderExporterUrl = "$baseUrl/exporter_orders";
  static const String updateOrderExporterUrl =
      "$baseUrl/exporter_orders/update_order";
  static const String completeOrderExporterUrl =
      "$baseUrl/exporter_orders/complete";
  static const String updateAdditionalFileUrl =
      "$baseUrl/orders/update_docs";
  static const String removeAdditionalFileUrl =
      "$baseUrl/orders/remove_docs";

  //Farmer Order Urls
  static const String getFarmersOrderRequestsUrl =
      "$baseUrl/farmer_orders/dashboard";
  static const String acceptRejectOrderByFarmerUrl = "$baseUrl/farmer_orders";
  static const String pendingOrderDetailOfFarmerUrl =
      "$baseUrl/farmer_orders/pending_order_details";
  static const String orderDetailOfFarmerUrl = "$baseUrl/farmer_orders/details";
  static const String getOrderListOfFarmerUrl = "$baseUrl/farmer_orders";
  static const String acceptRejectdataPointsByFarmerUrl =
      "$baseUrl/farmer_orders/data_points_action";
  static const String cancelOrderByFarmerUrl = "$baseUrl/farmer_orders/cancel";
  static const String confirmPaymentByFarmerUrl =
      "$baseUrl/farmer_orders/payment_status";

  //Mill Order Urls
  static const String getMillsOrderRequestsUrl =
      "$baseUrl/mill_orders/dashboard";
  static const String acceptRejectOrderByMillUrl = "$baseUrl/mill_orders";
  static const String pendingOrderDetailOfMillUrl =
      "$baseUrl/mill_orders/pending_order_details";
  static const String orderDetailOfMillUrl = "$baseUrl/mill_orders";
  static const String getOrderListOfMillUrl = "$baseUrl/mill_orders";
  static const String addDataPointsUrl = "$baseUrl/mill_orders/data_points";
  static const String dataPointsApprovalUrl =
      "$baseUrl/mill_orders/approve_data_points";
  static const String markCompleteOrderByMillUrl =
      "$baseUrl/mill_orders/complete";
  static const String markOrderReadyByMillUrl = "$baseUrl/mill_orders/ready";

  //Importer URL

  static const String acceptRejectOrderByImporterUrl =
      "$baseUrl/orders/update_status";
  static const String acceptShippingDocsByImporterUrl =
      "$baseUrl/orders/update_shiping_status";
  static const String getOrderListOfImporterUrl = "$baseUrl/orders";
  static const String getOrderDetailOfImporterUrl = "$baseUrl/orders";
  static const String markReceivedOrderByImporterUrl =
      "$baseUrl/orders/mark_as_received";
  static const String markApprovedOrderByRoasterUrl =
      "$baseUrl/orders/update_roaster_status";
  static const String markApprovedOrderByCafeUrl =
      "$baseUrl/orders/update_cafe_status";
  static const String markCompleteOrderByImporterUrl =
      "$baseUrl/orders/mark_as_complete";
  static const String getPendingOrderRequesForImporterUrl =
      "$baseUrl/orders/pending";

  // Consumer URL
  static const String scannedOrderDetailsUrl = "$webUrl";
  static const String addToScanHistoryUrl = "$baseUrl/orders/scan_history";
  static const String scanHistoryUrl = "$baseUrl/orders/scan_history";

  // Social media
  static const String domainUrl = "www.ifinca.com";
  static const String instagramSchemaUrl =
      "instagram://user?username=ifinca_coffee";
  static const String instagramWebUrl =
      "https://www.instagram.com/ifinca_coffee/";
  static const String iosFacebookSchemaUrl = "fb://profile?id=iFinca";
  static const String androidFacebookSchemaUrl = "fb://page/362796657635072";
  static const String facebookWebUrl = "https://www.facebook.com/iFinca/";
  static const String linkedInSchemaUrl = "linkedin://company/ifinca/";
  static const String linkedInWebUrl =
      "https://www.linkedin.com/company/ifinca";
  static const String twitterSchemaUrl =
      "twitter://user?screen_name=iFinca_coffee";
  static const String twitterWebUrl = "https://twitter.com/iFinca_coffee";
}
