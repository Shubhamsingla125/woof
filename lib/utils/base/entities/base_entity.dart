abstract class BaseEntity<K> {
  K get uniqueId;

  BaseEntity.update();

  BaseEntity.fromMap(Map<String, dynamic> map) : this.update();

  Map<String, dynamic> toMap();

  /// Used for searching, It will check weather entity contains query
  bool search(String searchQuery);
}

abstract class BaseTitleEntity {
  String get title;
}

abstract class BaseTitleDescriptionEntity {
  String get title;
  String get
  detail;
}

abstract class BaseAvatarEntity {
  String get avatar;
}

abstract class BasePercentageEntity {
  String get percentage;
}

abstract class BaseValueEntity {
  String getValue();
}

abstract class BaseDbEntity {
  String get createdBy;
  String get updatedBy;
  DateTime get created;
  DateTime get updated;
}
