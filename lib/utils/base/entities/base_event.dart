abstract class BaseEvent<E, D> {
  E get type;
  D get data;
}
