abstract class BaseErrorResponse {
  int get code;
  String get error;
  String get description;
}
