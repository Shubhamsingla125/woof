class AssetStrings {
  AssetStrings._();

  // Fonts
  static const String sourceSansProFont = "SourceSansPro";
  static const String houschkaFont = "Houschka";

  // Images
  static const String logoImage = "assets/woof_logo.png";
  static const String woofTextImage = "assets/woof_text.png";
  static const String splashImage = "Assets/images/splash.png";
  static const String splashAppNameLogoImage = "Assets/images/app_name_logo.png";
  static const String innerLogoImage = "Assets/images/logo-inner@3x.png";
  static const String splashInnerLogoImage = "Assets/images/LogoSplashAnimationImages/img";
  static const String bgImage = "Assets/images/bg-img@3x.webp";
  static const String resendImage = "Assets/images/resend@3x.png";
  static const String envelopImage = "Assets/images/enve@3x.png";
  static const String selectUserBGImage = "Assets/images/selectUserBG.webp";
  static const String editProfileBGImage = "Assets/images/editProfileBG.png";

  // Icons
  static const String aboutIcon = "Assets/images/icon_about.png";
  static const String lockIcon = "Assets/images/lock@3x.png";
  static const String forgotLockIcon = "Assets/images/forgot.png";
  static const String resetIcon = "Assets/images/reset.png";
  static const String userIcon = "Assets/images/user@3x.png";
  static const String phoneCallIcon = "Assets/images/phone-call-g@3x.png";
  static const String envelopeIcon = "Assets/images/envelope-g@3x.png";
  static const String websiteGlobeIcon = "Assets/images/website_globe.png";
  static const String verifiedIcon = "Assets/images/verified.png";
  static const String cancelIcon = "Assets/images/cancel.png";
  static const String placeholderIcon = "Assets/images/placeholder@3x.png";
  static const String backIcon = "Assets/images/back@3x.png";
  static const String languageIcon = "Assets/images/language.png";
  static const String forwardArrowIcon = "Assets/images/forward-arrow.png";
  static const String logoutIcon = "Assets/images/logout.png";
  static const String smallCalendarIcon = "Assets/images/smallCalendar.png";
  static const String farmerAssignIcon = "Assets/images/farmerAssign.png";
  static const String waitingIcon = "Assets/images/waitingIcon.png";
  static const String noOrderIcon = "Assets/images/noOrder.png";
  static const String qrCodeIcon = "Assets/images/qrCode.png";
  static const String scanQRCodeIcon = "Assets/images/scan_qr_icon.png";
  static const String coffeeChainIcon = "Assets/images/coffee_chain.png";

  static const String twitterIcon = "Assets/images/twitter.png";
  static const String facebookIcon = "Assets/images/facebook.png";
  static const String instagramIcon = "Assets/images/instagram.png";
  static const String linkedinIcon = "Assets/images/linkedin.png";
  static const String scanIcon = "Assets/images/scan.png";
  static const String scanHistoryIcon = "Assets/images/scan_history.png";
  static const String ourStoryIcon = "Assets/images/ourStory.png";

  //Tabbar Icons
  static const  String activeHomeIcon = "Assets/images/activeHomeTabBar.png";
  static const  String inactiveHomeIcon = "Assets/images/inactiveHomeTabBar.png";
  static const  String activeOrderIcon = "Assets/images/activeOrderTabBar.png";
  static const  String inactiveOrderIcon = "Assets/images/inactiveOrderTabBar.png";
  static const  String activeAssetsIcon = "Assets/images/activeAssetsTabBar.png";
  static const  String inactiveAssetsIcon = "Assets/images/inactiveAssetTabBar.png";
  static const  String activeSettingsIcon = "Assets/images/activeSettingsTabBar.png";
  static const  String inactiveSettingsIcon = "Assets/images/inactiveSettingsTabBar.png";

  //Select User
  static const String farmerIcon = "Assets/images/farmer.png";
  static const String consumerCoffeeIcon = "Assets/images/consumerCoffee.png";
  static const String cafeStoreIcon = "Assets/images/cafeStore.png";
  static const String roasterIcon = "Assets/images/roaster.png";
  static const String importerIcon = "Assets/images/importer.png";
  static const String exporterIcon = "Assets/images/exporter.png";
  static const String millIcon = "Assets/images/mill.png";
  static const String coopIcon = "Assets/images/coop.png";
  static const String mapIcon = "Assets/images/map.png";


  //Company Logo
  static const  String companyLogo = "Assets/images/add-logo@3x.png";




}
