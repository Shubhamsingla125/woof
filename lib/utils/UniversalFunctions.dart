import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:woof_fini/BLocs/AuthBLoc.dart';
import 'package:woof_fini/Model/Login/LoginResponse.dart';
import 'package:woof_fini/Network/UniversalAPI.dart';
import 'package:woof_fini/Providers/AuthProvider.dart';

import 'package:woof_fini/Utils/Constants/UserType.dart';
import 'package:woof_fini/Utils/IfincaColors.dart';
import 'package:woof_fini/Utils/Localization.dart';
import 'package:woof_fini/Utils/LocalizationValues.dart';
import 'package:woof_fini/Utils/Messages.dart';
import 'package:woof_fini/Utils/ReusableComponents/PlacesAutoComplete.dart';
import 'package:woof_fini/Utils/UniversalProperties.dart';
import 'package:woof_fini/Utils/memory_management.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simple_permissions/simple_permissions.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';

//import 'package:intl/intl.dart';
//import 'package:url_launcher/url_launcher.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'APIs.dart';
import 'ReusableComponents/CustomNavigationTranstions.dart';
import 'ReusableComponents/WebViewScaffold.dart';

// Returns screen size
Size getScreenSize({@required BuildContext context}) {
  return MediaQuery.of(context).size;
}

// Returns status bar height
double getStatusBarHeight({@required BuildContext context}) {
  return MediaQuery.of(context).padding.top;
}

// Returns bottom padding for round edge screens
double getSafeAreaBottomPadding({@required BuildContext context}) {
  return MediaQuery.of(context).padding.bottom;
}

// Returns Keyboard size
double getKeyboardSize({@required BuildContext context}) {
  return MediaQuery.of(context).viewInsets.bottom;
}

// Show alert dialog
void showAlert(
    {@required BuildContext context,
    String titleText,
    Widget title,
    String message,
    Widget content,
    Map<String, VoidCallback> actionCallbacks}) {
  Widget titleWidget = titleText == null
      ? title
      : new Text(
          titleText.toUpperCase(),
          textAlign: TextAlign.center,
          style: new TextStyle(
            color: dialogContentColor,
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
          ),
        );
  Widget contentWidget = message == null
      ? content != null ? content : new Container()
      : new Text(
          message,
          textAlign: TextAlign.center,
          style: new TextStyle(
            color: dialogContentColor,
            fontWeight: FontWeight.w400,
//            fontFamily: Constants.contentFontFamily,
          ),
        );

  OverlayEntry alertDialog;
  // Returns alert actions
  List<Widget> _getAlertActions(Map<String, VoidCallback> actionCallbacks) {
    List<Widget> actions = [];
    actionCallbacks.forEach((String title, VoidCallback action) {
      actions.add(
        new ButtonTheme(
          minWidth: 0.0,
          child: new CupertinoDialogAction(
            child: new Text(title,
                style: new TextStyle(
                  color: dialogContentColor,
                  fontSize: 16.0,
//                        fontFamily: Constants.contentFontFamily,
                )),
            onPressed: () {
              action();
              alertDialog?.remove();
              alertAlreadyActive = false;
            },
          ),
//          child: defaultTargetPlatform != TargetPlatform.iOS
//              ? new FlatButton(
//                  child: new Text(
//                    title,
//                    style: new TextStyle(
//                      color: IfincaColors.kPrimaryBlue,
////                      fontFamily: Constants.contentFontFamily,
//                    ),
//                    maxLines: 2,
//                  ),
//                  onPressed: () {
//                    action();
//                  },
//                )
//              :
// new CupertinoDialogAction(
//                  child: new Text(title,
//                      style: new TextStyle(
//                        color: IfincaColors.kPrimaryBlue,
//                        fontSize: 16.0,
////                        fontFamily: Constants.contentFontFamily,
//                      )),
//                  onPressed: () {
//                    action();
//                  },
//                ),
        ),
      );
    });
    return actions;
  }

  List<Widget> actions =
      actionCallbacks != null ? _getAlertActions(actionCallbacks) : [];

  OverlayState overlayState;
  overlayState = Overlay.of(context);

  alertDialog = new OverlayEntry(builder: (BuildContext context) {
    return new Positioned.fill(
      child: new Container(
        color: Colors.black.withOpacity(0.7),
        alignment: Alignment.center,
        child: new WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: new Dialog(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(10.0),
            ),
            child: new Material(
              borderRadius: new BorderRadius.circular(10.0),
              color: IfincaColors.kWhite,
              child: new Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 16.0,
                ),
                child: new Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    new Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 8.0,
                      ),
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          new Padding(
                            padding: const EdgeInsets.symmetric(
                              vertical: 20.0,
                            ),
                            child: titleWidget,
                          ),
                          contentWidget,
                        ],
                      ),
                    ),
                    new Container(
                      height: 0.6,
                      margin: new EdgeInsets.only(
                        top: 24.0,
                      ),
                      color: dialogContentColor,
                    ),
                    new Row(
                      children: <Widget>[]..addAll(
                          new List.generate(
                            actions.length +
                                (actions.length > 1 ? (actions.length - 1) : 0),
                            (int index) {
                              return index.isOdd
                                  ? new Container(
                                      width: 0.6,
                                      height: 30.0,
                                      color: dialogContentColor,
                                    )
                                  : new Expanded(
                                      child: actions[index ~/ 2],
                                    );
                            },
                          ),
                        ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  });

  if (!alertAlreadyActive) {
    alertAlreadyActive = true;
    overlayState.insert(alertDialog);
  }
}

// Checks Internet connection for "POST" method
Future<void> checkInternetForPostMethod(
    {@required BuildContext context,
    @required bool mounted,
    @required Function onSuccess,
    @required Function onFail,
    bool canShowAlert = true}) async {
  try {
    final result = await InternetAddress.lookup('google.com')
        .timeout(const Duration(seconds: 5));
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      onSuccess();
    } else {
      onFail();
      if (canShowAlert) {
        showAlert(
            context: context,
            titleText: Localization.of(context).trans(LocalizationValues.error),
            message: Messages.noInternetError,
            actionCallbacks: {
              Localization.of(context).trans(LocalizationValues.ok): () {}
            });
      }
    }
  } catch (_) {
    onFail();
    showAlert(
        context: context,
        titleText: Localization.of(context).trans(LocalizationValues.error),
        message: Messages.noInternetError,
        actionCallbacks: {
          Localization.of(context).trans(LocalizationValues.ok): () {}
        });
  }
}

// Closes keyboard by clicking any where on screen
void closeKeyboard({
  @required BuildContext context,
  @required VoidCallback onClose,
}) {
  if (getKeyboardSize(context: context) > 0.0) {
    FocusScope.of(context).requestFocus(new FocusNode());
    try {
      onClose();
    } catch (e) {}
  }
}

// Checks Internet connection
Future<bool> hasInternetConnection({
  @required BuildContext context,
  bool mounted,
  @required Function onSuccess,
  @required Function onFail,
  Function onFailOk,
  bool canShowAlert = true,
}) async {
  try {
    final result = await InternetAddress.lookup('google.com')
        .timeout(const Duration(seconds: 5));
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      onSuccess();
      return true;
    } else {
      if (canShowAlert) {
        if (onFail != null) {
          onFail();
        }
        showAlert(
          context: context,
          titleText: Localization.of(context).trans(LocalizationValues.error),
          message: Messages.noInternetError,
          actionCallbacks: {
            Localization.of(context).trans(LocalizationValues.ok): () {
              if (onFailOk != null) {
                onFailOk();
              }
              return false;
            }
          },
        );
      }
    }
  } catch (_) {
    if (onFail != null) {
      onFail();
    }
    showAlert(
        context: context,
        titleText: Localization.of(context).trans(LocalizationValues.error),
        message: Messages.noInternetError,
        actionCallbacks: {
          Localization.of(context).trans(LocalizationValues.ok): () {
            if (onFailOk != null) {
              onFailOk();
            }
            return false;
          }
        });
  }
  return false;
}

// Sets focus node
void setFocusNode({
  @required BuildContext context,
  @required FocusNode focusNode,
}) {
  FocusScope.of(context).requestFocus(focusNode);
}

// Launches url
launchURL({
  @required String url,
  @required BuildContext context,
  @required bool mounted,
}) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    showAlert(
        context: context,
        titleText: Localization.of(context).trans(LocalizationValues.error),
        message: Messages.genericError,
        actionCallbacks: {
          Localization.of(context).trans(LocalizationValues.ok): () {}
        });
  }
}

// Returns formatted date string
String getFormattedDateString({
  String format,
  @required DateTime dateTime,
}) {
  return dateTime != null
      ? new DateFormat(format ?? "dd MMM yyyy").format(dateTime)
      : "-";
}

// Returns formatted date string from millisecond epoch
String getFormattedDateStringFromMSEpoch({
  String format,
  @required int millisecondEpoch,
}) {
  DateTime dateTime;
  try {
    dateTime = new DateTime.fromMillisecondsSinceEpoch(
      millisecondEpoch,
    );
  } catch (e) {}

  return dateTime != null
      ? new DateFormat(format ?? "MMM dd, y").format(dateTime)
      : "-";
}

/// 1) Parses string to date
/// 2) Returns formatted date string
String parseGetFormattedDateString({
  String format,
  @required String dateString,
}) {
  try {
    return new DateFormat(format ?? "MMM dd, y")
        .format(DateTime.parse(dateString).toLocal());
  } catch (e) {
    return "-";
  }
}

// Returns datetime parsing string of format "MM/dd/yy"
DateTime getDateFromString({
  @required String dateString,
}) {
  try {
    return DateTime.parse(dateString);
  } catch (e) {
    return DateTime.now();
  }
}

// CONVERTS DOUBLE INTO RADIANS
num getRadians({@required double value}) {
  return value * (3.14 / 180);
}

/// 1) Logs out user
/// 2) Navigates to login screen
//void logout({
//  @required BuildContext context,
//}) {
//  String rememberedEmail = MemoryManagement.getRememberedEmail();
//  MemoryManagement.clearMemory();
//  if (rememberedEmail != null) {
//    MemoryManagement.setRememberedEmail(
//        CurrentIfincaUser.instance.user.email);
//  }
//  isUserSignedIn = false;
//  CurrentIfincaUser.instance.user = null;
//
//  Navigator.pushAndRemoveUntil(
//    context,
//    new CupertinoPageRoute(
//      builder: (BuildContext context) {
//        return new LoginProvider(
//            child: new LoginScreen(),
//            loginBloc: LoginBloc(LoginServiceApi()));
//      },
//    ),
//        (route) => false,
//  );
//}

//AddBadge
void addBadge(int value) async {
  try {
    bool res = await FlutterAppBadger.isAppBadgeSupported();
    if (res) {
      FlutterAppBadger.updateBadgeCount((unreadNotificationsCount ?? 0) +
          (unreadBlogNotificationsCount ?? 0));
//      FlutterAppBadger.updateBadgeCount(value);
    }
  } catch (e) {}
}

//Remove Badge
void removeBadge() async {
  try {
    bool res = await FlutterAppBadger.isAppBadgeSupported();
    if (res) {
      FlutterAppBadger.removeBadge();
    }
  } catch (e) {}
}

// Clears memory on logout success
Future onLogoutSuccess({
  @required BuildContext context,
}) async {
  MemoryManagement.clearMemory();
  removeBadge();
  unreadNotificationsCount = 0;
  if (isAndroid()) {
//    Fluttertoast.clearAllNotifications();
  }
  isUserSignedIn = false;
//  customPushAndRemoveUntilSplash(
//    context: context,
//    screen: new SelectUser(),
//  );
}

// Logs out user
void logoutUser({
  @required BuildContext context,
}) {
//  UniversalAPI.logout(
//      context: context,
//      mounted: null,
//      requestBody: {},
//      onSuccess: () {
//        onLogoutSuccess(context: context);
//      });
}

// Checks target platform
bool isAndroid() {
  return defaultTargetPlatform == TargetPlatform.android;
}

// Can return image
Future checkPermissionAndOpenCamera(BuildContext context) async {
  // Platform messages may fail, so we use a try/catch PlatformException.
  try {
    alreadyAskingPermission = true;
    final res = await SimplePermissions.requestPermission(Permission.Camera);
    alreadyAskingPermission = false;
    if ((res == PermissionStatus.deniedNeverAsk && isAndroid()) ||
        (res == PermissionStatus.denied && !isAndroid())) {
      showSettingAlert(
          context,
          Localization.of(context)
              .trans(LocalizationValues.cameraNotAuthorised));
    } else {
      if (res == PermissionStatus.denied) {
        Navigator.of(
          context,
          rootNavigator: true,
        ).pop(null);
      } else {
        var cameraObj = await ImagePicker.pickImage(
          source: ImageSource.camera,
        );

        Navigator.of(
          context,
          rootNavigator: true,
        ).pop(cameraObj);
      }
    }
  } on PlatformException {
//      platformVersion = 'Failed to get platform version.';
  } catch (e) {}
}

Future checkPermissionAndOpenGallery(BuildContext context) async {
  bool cameraPermission;
  // Platform messages may fail, so we use a try/catch PlatformException.
  try {
    alreadyAskingPermission = true;
    final res =
        await SimplePermissions.requestPermission(Permission.PhotoLibrary);
    alreadyAskingPermission = false;

    if ((res == PermissionStatus.deniedNeverAsk && isAndroid()) ||
        (res == PermissionStatus.denied && !isAndroid())) {
      showSettingAlert(
          context,
          Localization.of(context)
              .trans(LocalizationValues.galleryNotAuthorized));
    } else {
      var galleryObj = await ImagePicker.pickImage(
        source: ImageSource.gallery,
      );
      Navigator.of(
        context,
        rootNavigator: true,
      ).pop(galleryObj);
    }
  } on PlatformException {
//      platformVersion = 'Failed to get platform version.';
  } catch (e) {}
}

showSettingAlert(BuildContext context, String message) {
  showAlert(
    context: context,
    titleText: Localization.of(context).trans(LocalizationValues.error),
    message: message,
    actionCallbacks: {
      Localization.of(context).trans(LocalizationValues.notNow): () {},
      Localization.of(context).trans(LocalizationValues.settings): () async {
        await Future.delayed(new Duration(
          milliseconds: 200,
        ));
        SimplePermissions.openSettings();
      },
    },
  );
}

bool alreadyAskingPermission = false;

Future<File> showMediaAlert({@required BuildContext context}) async {
  File file = await showDialog<File>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new CupertinoAlertDialog(
          title: new Container(),
          content: new Text(
            Localization.of(context).trans(LocalizationValues.chooseOption),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () async {
                if (!alreadyAskingPermission) {
                  var result = await checkPermissionAndOpenCamera(context);
                }
              },
              child: Text('Camera'),
            ),
            FlatButton(
              onPressed: () async {
                if (!alreadyAskingPermission) {
                  var result = await checkPermissionAndOpenGallery(context);
                }
              },
              child: Text('Gallery'),
            ),
            FlatButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                Localization.of(context).trans(LocalizationValues.cancel),
                style: TextStyle(color: Colors.red),
              ),
            ),
          ],
        );
      });
  return file;
}

LoginResponse getUserInfo() {
  var getUserString = MemoryManagement.getUserInfo();
  return getUserString != null
      ? LoginResponse.fromJson(json.decode(getUserString))
      : null;
}

// Navigates to home screen

// Asks for exit
Future<bool> askForExit() async {
  if (canExitApp) {
    exit(0);
    return Future.value(true);
  } else {
    canExitApp = true;
    Fluttertoast.showToast(
      msg: "Please click BACK again to exit",
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.BOTTOM,
    );
    new Future.delayed(
        const Duration(
          seconds: 2,
        ), () {
      canExitApp = false;
    });
    return Future.value(false);
  }
}

// Clears Paginating lists Cached data
void clearPaginatingListCachedData() {
  MemoryManagement.setAssetCafeStores(cafeStoresData: null);
  MemoryManagement.setAssetCoops(coopsData: null);
  MemoryManagement.setAssetExporters(exportersData: null);
  MemoryManagement.setAssetFarmers(farmersData: null);
  MemoryManagement.setAssetMills(millsData: null);
  MemoryManagement.setAssetRoasters(roastersData: null);
}

// Returns no data view
Widget getNoDataView({
  @required String msg,
  @required BuildContext context,
  TextStyle messageTextStyle,
  @required VoidCallback onRetry,
}) {
  return new Center(
    child: new Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new Text(
          msg,
          style: messageTextStyle ??
              const TextStyle(
                fontSize: 18.0,
              ),
        ),
        new InkWell(
          onTap: onRetry ?? () {},
          child: new Text(
            Localization.of(context).trans(LocalizationValues.refresh),
            style: const TextStyle(
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ],
    ),
  );
}

//Method is used to get data from server via socket.


// Returns platform specific back button icon
IconData getPlatformSpecificBackIcon() {
  return defaultTargetPlatform == TargetPlatform.iOS
      ? Icons.arrow_back_ios
      : Icons.arrow_back;
}


List buyerList(List roasterList, List cafeStoreList) {
  if (roasterList.isNotEmpty && cafeStoreList.isNotEmpty) {
    return cafeStoreList;
  } else if (roasterList.isNotEmpty) {
    return roasterList;
  } else {
    return cafeStoreList;
  }
}


// Returns user specific dashboard

Future<List<Prediction>> getAutoCompleteAddress(String query) async {
  var lang = MemoryManagement.getUserLanguage() ?? 'en';
  var addressUrl = "https://maps.googleapis.com/maps/api/place/"
      "autocomplete/json?"
      "input=$query&"
      "language=$lang&"
      "key=$kGoogleApiKey";
  var addresses = await _send(addressUrl);
//  List<String> addressLineList = [];
//  addresses.forEach((address) {
//    if (address.description != null) {
//      addressLineList.add(address.description);
//    }
//  });
  return addresses;
}

Future<List<Prediction>> _send(String url) async {
  HttpClient _httpClient = HttpClient();
  final uri = Uri.parse(url);
  final request = await _httpClient.getUrl(uri);
  final response = await request.close();
  final responseBody = await response.transform(utf8.decoder).join();
  var data = jsonDecode(responseBody);
  List predictions = data["predictions"];
  if (predictions == null) return null;
  List<Prediction> addressList = [];
  predictions.forEach((map) {
    addressList.add(Prediction.fromJson(map));
  });
  return addressList;
}

//Navigate as per user type to detail screen.

// NavigateToFarmerDetailScreen

// Navigates to Logo screen

// Custom Push And Remove Until Splash
void customPushAndRemoveUntilSplash({
  @required BuildContext context,
  @required Widget screen,
}) {
  Navigator.popUntil(
    context,
    (route) {
      return route.runtimeType == SplashPageRoute;
    },
  );

//  if (screen is Dashboard) {
//    Navigator.push(
//      context,
//      new DashboardPageRoute(
//        widget: screen,
//      ),
//    );
//  } else {
//    Navigator.push(
//      context,
//      new FadeTransitionRoute(
//        widget: screen,
//      ),
//    );
//  }
}

// Custom pop Until Dashboard
void customPopUntilDashboard({
  @required BuildContext context,
}) {
  Navigator.popUntil(
    context,
    (route) {
      return route.runtimeType == DashboardPageRoute;
    },
  );
}

// Returns first letter capitalized of the string
String getFirstLetterCapitalized({@required String source}) {
  if (source == null) {
    return "";
  } else {
    String result = source.toUpperCase().substring(0, 1);
    if (source.length > 1) {
      result = result + source.toLowerCase().substring(1, source.length);
    }
    return result;
  }
}

// Sets tab Count
String setTabCount({@required int count}) {
  return count == null
      ? ""
      : count > 0 ? count > 99 ? "(99+)" : "($count)" : "";
}

// Navigates to terms and conditions
checkInternetConnectTermsCondition({
  @required BuildContext context,
  @required bool mounted,
}) async {
  bool gotInternetConnection = await hasInternetConnection(
    context: context,
    mounted: mounted,
    canShowAlert: true,
    onFail: () {},
    onSuccess: () {},
  );
  if (gotInternetConnection) {
    Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => new WebViewScaffold(
          url: APIs.termsConditionsUrl,
          title:
              Localization.of(context).trans(LocalizationValues.termsCondition),
        ),
      ),
    );
  }
}

// Navigates to privacy policy
checkInternetConnectPrivacyPolicy({
  @required BuildContext context,
  @required bool mounted,
}) async {
  bool gotInternetConnection = await hasInternetConnection(
    context: context,
    mounted: mounted,
    canShowAlert: true,
    onFail: () {},
    onSuccess: () {},
  );
  if (gotInternetConnection) {
    Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => new WebViewScaffold(
          url: APIs.privacyPolicyUrl,
          title:
              Localization.of(context).trans(LocalizationValues.privacyPolicy),
        ),
      ),
    );
  }
}

// Returns package info
//Future<PackageInfo> getPackageInfo() async {
//  PackageInfo packageInfo = await PackageInfo.fromPlatform();
//  return packageInfo;
//}

/// 1. Opens barcode scanner
/// 2. Returns barcode



