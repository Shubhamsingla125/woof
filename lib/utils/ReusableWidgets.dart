import 'dart:convert';
import 'dart:io';
import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dialog.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:woof_fini/utils/ReusableComponents/CustomNavigationTranstions.dart';

import 'AssetStrings.dart';
import 'Clippers.dart';
import 'IfincaColors.dart';
import 'Localization.dart';
import 'LocalizationValues.dart';
import 'ReusableComponents/CheckBoxListPopUpMenu.dart';
import 'ReusableComponents/PlacesAutoComplete.dart';
import 'ReusableComponents/SelectLocation.dart';
import 'ThirdPartyComponents/Flutter_Typeahead/flutter_typeahead.dart';
import 'UniversalFunctions.dart';
import 'UniversalProperties.dart';

// Returns app themed background one
Widget getAppThemedBGOne() {
  return Positioned.fill(
    child: new Image.asset(
      AssetStrings.bgImage,
      fit: BoxFit.cover,
    ),
  );
}

// Returns "App themed text field" centered label
Widget appThemedTextFormFieldOne({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  int maxLength,
  int maxLines,
  bool enabled = true,
  bool showCounter,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  String suffixAsset,
  Widget suffixSecondaryAsset,
  Widget prefixOneAsset,
  Function onPrefixOneTap,
  Widget prefixTwoAsset,
  Function onPrefixTwoTap,
  TextInputAction inputAction,
  TextCapitalization textCapitalization,
}) {
  // Defaults
  const TextStyle defaultLabelStyle = const TextStyle(
    color: Colors.white,
  );

  const TextStyle defaultTextStyle = const TextStyle(
    color: Colors.white,
  );

  const Color defaultBorderColor = Colors.white;

  return new Stack(
    children: <Widget>[
      new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              suffixAsset != null
                  ? new Container(
                      width: getScreenSize(context: context).width * 0.105,
                      height: getScreenSize(context: context).width * 0.1,
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.only(
                        bottom: 6.0,
                        left: 8.0,
                      ),
                      child: new Image.asset(
                        suffixAsset,
                        width: getScreenSize(context: context).width * 0.04,
                        height: getScreenSize(context: context).width * 0.05,
                        fit: BoxFit.contain,
                        color: Colors.white,
                      ),
                    )
                  : new Container(),
              new Container(
                height: getScreenSize(context: context).width * 0.074,
                child: suffixSecondaryAsset ?? new Container(),
              ),
              new Offstage(
                offstage: suffixSecondaryAsset == null,
                child: getSpacer(
                  width: 4.0,
                ),
              ),
              new Expanded(
                child: new InkWell(
                  onTap: () {
                    FocusScope.of(context).requestFocus(focusNode);
                  },
                  child: new Container(
                    constraints: new BoxConstraints(
                      maxHeight: getScreenSize(context: context).height * 0.3,
                    ),
                    child: new TextFormField(
                      textCapitalization:
                          textCapitalization ?? TextCapitalization.none,
                      textInputAction: inputAction ?? TextInputAction.next,
                      controller: controller,
                      obscureText: obscureText ?? false,
                      focusNode: focusNode,
                      keyboardType: keyboardType,
                      validator: validator,
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(maxLength ?? 1000),
                      ]..addAll(inputFormatters ?? []),
                      maxLength: maxLength,
                      onFieldSubmitted: onFieldSubmitted,
                      enabled: enabled,
                      maxLines: maxLines == 3 ? null : 1,
                      style: textStyle ?? defaultTextStyle,
                      decoration: new InputDecoration(
                        border: InputBorder.none,
                        contentPadding: const EdgeInsets.only(
                          top: 0.0,
                          bottom: 0.0,
                        ),
                        labelText: label,
                        labelStyle: labelStyle ?? defaultLabelStyle,
                        errorStyle: new TextStyle(
                          fontSize: 10.0,
//                          color: labelStyle?.color ?? defaultLabelStyle?.color,
                          color: Colors.red,
                        ),
                        helperStyle: new TextStyle(
                          fontSize: 0.0,
                          color: labelStyle?.color ?? defaultLabelStyle?.color,
                        ),
                        counterStyle: new TextStyle(
                          fontSize: showCounter ?? false ? 10.0 : 0.0,
                          color: labelStyle?.color ?? defaultLabelStyle?.color,
                        ),
                        isDense: true,
                      ),
                    ),
                  ),
                ),
              ),
              new Offstage(
                offstage: prefixOneAsset == null,
                child: new InkWell(
                  onTap: onPrefixOneTap,
                  child: new Container(
                    width: getScreenSize(context: context).width * 0.105,
                    height: getScreenSize(context: context).width * 0.1,
                    padding: const EdgeInsets.only(
                      bottom: 6.0,
                      right: 8.0,
                    ),
                    child: prefixOneAsset == null
                        ? new Container()
                        : new Container(
                            child: prefixOneAsset,
                            width: getScreenSize(context: context).width * 0.04,
                            height:
                                getScreenSize(context: context).width * 0.05,
                          ),
                  ),
                ),
              ),
              new Offstage(
                offstage: prefixTwoAsset == null,
                child: new InkWell(
                  onTap: onPrefixTwoTap,
                  child: new Container(
//                  color: Colors.amber,
                    width: getScreenSize(context: context).width * 0.07,
                    height: getScreenSize(context: context).width * 0.1,
                    padding: const EdgeInsets.only(
                      bottom: 6.0,
                      right: 8.0,
                    ),
                    child: prefixTwoAsset == null
                        ? new Container()
                        : new Container(
                            child: prefixTwoAsset,
                            width: getScreenSize(context: context).width * 0.04,
                            height:
                                getScreenSize(context: context).width * 0.05,
                          ),
                  ),
                ),
              ),
            ],
          ),
          getSpacer(
            height: 4.0,
          ),
          new Container(
            height: 1.0,
            color: borderColor ?? defaultBorderColor,
          ),
        ],
      ),
      new Positioned.fill(
        child: new Offstage(
          offstage: enabled ?? true,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Returns "App themed text field" centered label
Widget appThemedTextFieldOne({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  int maxLength,
  int maxLines,
  bool enabled,
  bool showCounter,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  String suffixAsset,
  Widget suffixSecondaryAsset,
  Widget prefixOneAsset,
  Function onPrefixOneTap,
  Widget prefixTwoAsset,
  Function onPrefixTwoTap,
  Function onUpdate,
  TextInputAction inputAction,
  TextCapitalization textCapitalization,
}) {
  // Defaults
  const TextStyle defaultLabelStyle = const TextStyle(
    color: Colors.white,
  );

  const TextStyle defaultTextStyle = const TextStyle(
    color: Colors.white,
  );

  const Color defaultBorderColor = Colors.white;
  String validationMessage;
  return new Stack(
    children: <Widget>[
      new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              suffixAsset != null
                  ? new Container(
                      width: getScreenSize(context: context).width * 0.105,
                      height: getScreenSize(context: context).width * 0.1,
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.only(
                        bottom: 6.0,
                        left: 8.0,
                      ),
                      child: new Image.asset(
                        suffixAsset,
                        width: getScreenSize(context: context).width * 0.04,
                        height: getScreenSize(context: context).width * 0.05,
                        fit: BoxFit.fill,
                      ),
                    )
                  : new Container(),
              new Container(
                height: getScreenSize(context: context).width * 0.074,
                child: suffixSecondaryAsset ?? new Container(),
              ),
              new Offstage(
                offstage: suffixSecondaryAsset == null,
                child: getSpacer(
                  width: 4.0,
                ),
              ),
              new Expanded(
                child: new InkWell(
                  onTap: () {
                    FocusScope.of(context).requestFocus(focusNode);
                  },
                  child: new Container(
                    constraints: new BoxConstraints(
                      maxHeight: getScreenSize(context: context).height * 0.3,
                    ),
                    child: new TextField(
                      textCapitalization:
                          textCapitalization ?? TextCapitalization.none,
                      textInputAction: inputAction ?? TextInputAction.next,
                      controller: controller,
                      obscureText: obscureText ?? false,
                      focusNode: focusNode,
                      keyboardType: keyboardType,
                      onChanged: (value) {
                        if (validator != null) {
                          validationMessage = validator(value);
                          if (onUpdate != null) {
                            onUpdate();
                          }
                        }
                      },
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(maxLength ?? 1000),
                      ]..addAll(inputFormatters ?? []),
                      maxLength: maxLength,
                      onSubmitted: onFieldSubmitted,
                      enabled: enabled,
                      maxLines: maxLines == 3 ? null : 1,
                      style: textStyle ?? defaultTextStyle,
                      decoration: new InputDecoration(
                        border: InputBorder.none,
                        contentPadding: const EdgeInsets.only(
                          top: 0.0,
                          bottom: 4.0,
                        ),
                        labelText: label,
                        labelStyle: labelStyle ?? defaultLabelStyle,
                        errorStyle: new TextStyle(
                          fontSize: 10.0,
//                          color: labelStyle?.color ?? defaultLabelStyle?.color,
                          color: Colors.red,
                        ),
                        helperStyle: new TextStyle(
                          fontSize: 0.0,
                          color: labelStyle?.color ?? defaultLabelStyle?.color,
                        ),
                        counterStyle: new TextStyle(
                          fontSize: showCounter ?? false ? 10.0 : 0.0,
                          color: labelStyle?.color ?? defaultLabelStyle?.color,
                        ),
                        isDense: true,
                      ),
                    ),
                  ),
                ),
              ),
              new Offstage(
                offstage: prefixOneAsset == null,
                child: new InkWell(
                  onTap: onPrefixOneTap,
                  child: new Container(
                    width: getScreenSize(context: context).width * 0.105,
                    height: getScreenSize(context: context).width * 0.1,
                    padding: const EdgeInsets.only(
                      bottom: 6.0,
                      right: 8.0,
                    ),
                    child: prefixOneAsset == null
                        ? new Container()
                        : new Container(
                            child: prefixOneAsset,
                            width: getScreenSize(context: context).width * 0.04,
                            height:
                                getScreenSize(context: context).width * 0.05,
                          ),
                  ),
                ),
              ),
              new Offstage(
                offstage: prefixTwoAsset == null,
                child: new InkWell(
                  onTap: onPrefixTwoTap,
                  child: new Container(
//                  color: Colors.amber,
                    width: getScreenSize(context: context).width * 0.07,
                    height: getScreenSize(context: context).width * 0.1,
                    padding: const EdgeInsets.only(
                      bottom: 6.0,
                      right: 8.0,
                    ),
                    child: prefixTwoAsset == null
                        ? new Container()
                        : new Container(
                            child: prefixTwoAsset,
                            width: getScreenSize(context: context).width * 0.04,
                            height:
                                getScreenSize(context: context).width * 0.05,
                          ),
                  ),
                ),
              ),
            ],
          ),
          new Offstage(
            offstage: validationMessage == null,
            child: new Text(
              validationMessage ?? "",
              style: new TextStyle(
                fontSize: 10.0,
//              color: labelStyle?.color ?? defaultLabelStyle?.color,
                color: Colors.red,
              ),
            ),
          ),
          getSpacer(
            height: 4.0,
          ),
          new Container(
            height: 1.0,
            color: borderColor ?? defaultBorderColor,
          ),
        ],
      ),
      new Positioned.fill(
        child: new Offstage(
          offstage: enabled ?? true,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Returns "App themed image picker text field" centered label
Widget appThemedImagePickerTextFieldOne({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  int maxLength,
  int maxLines,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  @required String suffixAsset,
  Widget suffixSecondaryAsset,
  Widget prefixOneAsset,
  Function onPrefixOneTap,
  Widget prefixTwoAsset,
  Function onPrefixTwoTap,
  TextInputAction inputAction,
  TextCapitalization textCapitalization,
  @required bool mounted,
  @required Function(File) onFileSelected,
  @required Function onCancel,
}) {
  return new Stack(
    children: <Widget>[
      appThemedTextFormFieldOne(
        focusNode: focusNode,
        context: context,
        suffixAsset: suffixAsset,
        controller: controller,
        label: label,
        validator: validator,
        obscureText: obscureText,
        onPrefixOneTap: () {
          controller.clear();
          if (onCancel != null) {
            onCancel();
          }
        },
        prefixOneAsset: controller.text.isEmpty
            ? new Container()
            : new Align(
                alignment: Alignment.bottomRight,
                child: new Icon(
                  Icons.cancel,
                  color: labelStyle?.color ?? Colors.white,
                  size: 20.0,
                ),
              ),
        prefixTwoAsset: prefixTwoAsset != null
            ? new Align(
                alignment: Alignment.bottomRight,
                child: prefixTwoAsset,
              )
            : prefixTwoAsset,
        onPrefixTwoTap: onPrefixTwoTap,
        onFieldSubmitted: onFieldSubmitted,
        labelStyle: labelStyle,
        borderColor: borderColor,
        enabled: true,
        inputAction: inputAction,
        textStyle: textStyle,
      ),
      new Positioned(
        right: prefixTwoAsset != null
            ? controller.text.isEmpty
                ? getScreenSize(context: context).width * 0.105
                : 2 * getScreenSize(context: context).width * 0.105
            : controller.text.isEmpty
                ? 0.0
                : getScreenSize(context: context).width * 0.105,
        top: 0.0,
        left: 0.0,
        bottom: 0.0,
        child: new InkWell(
          onTap: () async {
            File file = await showMediaAlert(context: context);
            if (file != null) {
              if (file.lengthSync() <= 40000000) {
                controller.text = file.path.split("/").last;
                if (onFileSelected != null) {
                  onFileSelected(file);
                }
              } else {
                showAlert(
                  context: context,
                  titleText:
                      Localization.of(context).trans(LocalizationValues.error),
                  message: Localization.of(context)
                      .trans(LocalizationValues.fileSizeCheck),
                  actionCallbacks: {
                    Localization.of(context).trans(LocalizationValues.ok):
                        () {},
                  },
                );
              }
            }
          },
          child: new Container(
            color: Colors.white.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Returns "App themed Phone text field " centered label
Widget appThemedPhoneTextFieldOne({
  @required String label,
  @required Country initialSelectedCountry,
  @required TextEditingController controller,
  @required BuildContext context,
  @required Function(Country) onCountrySelected,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  int maxLength,
  String suffixAsset,
  Widget prefixAsset,
  Function onPrefixTap,
  TextInputAction inputAction,
}) {
  Widget _buildDialogItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text("+${country.phoneCode}"),
          SizedBox(width: 8.0),
          Flexible(child: Text(country.name))
        ],
      );

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => new CountryPickerDialog(
          titlePadding: EdgeInsets.all(8.0),
          searchCursorColor: IfincaColors.kAppYellow,
          searchInputDecoration: InputDecoration(
              hintText:
                  Localization.of(context).trans(LocalizationValues.search) +
                      '...'),
          isSearchable: true,
          title: new Text(Localization.of(context)
              .trans(LocalizationValues.selectYourPhoneCode)),
          onValuePicked: onCountrySelected,
          itemBuilder: _buildDialogItem,
        ),
      );

  return new Stack(
    children: <Widget>[
      appThemedTextFormFieldOne(
        focusNode: focusNode,
        context: context,
        suffixAsset: suffixAsset,
        suffixSecondaryAsset: new InkWell(
          child: new Container(
//        color: Colors.blue,
            child: new Row(
              children: <Widget>[
                new Text(
                  "+${initialSelectedCountry.phoneCode}",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    color: textStyle?.color ?? Colors.white,
                    fontSize: 16.0,
                  ),
                ),
                new Icon(
                  Icons.expand_more,
                  color: textStyle?.color ?? Colors.white,
                  size: 15,
                ),
              ],
            ),
          ),
          onTap: _openCountryPickerDialog,
        ),
        controller: controller,
        label: label,
        validator: validator,
        obscureText: obscureText,
        inputFormatters: [
          WhitelistingTextInputFormatter(RegExp("[0-9]")),
          LengthLimitingTextInputFormatter(maxLength ?? 1000),
        ]..addAll(inputFormatters ?? []),
        keyboardType: TextInputType.phone,
        onPrefixOneTap: onPrefixTap,
        prefixOneAsset: prefixAsset,
        onFieldSubmitted: onFieldSubmitted,
        labelStyle: labelStyle,
        borderColor: borderColor,
        enabled: enabled,
        inputAction: inputAction,
        textStyle: textStyle,
      ),
      new Positioned.fill(
        child: new Offstage(
          offstage: enabled ?? true,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      )
    ],
  );
}

// Returns "App themed Phone text field " centered label
Widget appThemedPhoneTextFieldThree({
  @required String label,
  @required Country initialSelectedCountry,
  @required TextEditingController controller,
  @required BuildContext context,
  @required Function(Country) onCountrySelected,
  bool obscureText = false,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  int maxLength,
  String suffixAsset,
  Widget prefixAsset,
  Function onPrefixTap,
  TextInputAction inputAction,
}) {
  Widget _buildDialogItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text("+${country.phoneCode}"),
          SizedBox(width: 8.0),
          Flexible(child: Text(country.name))
        ],
      );

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => new CountryPickerDialog(
          titlePadding: EdgeInsets.all(8.0),
          searchCursorColor: IfincaColors.kAppYellow,
          searchInputDecoration: InputDecoration(
              hintText:
                  Localization.of(context).trans(LocalizationValues.search) +
                      '...'),
          isSearchable: true,
          title: new Text(Localization.of(context)
              .trans(LocalizationValues.selectYourPhoneCode)),
          onValuePicked: onCountrySelected,
          itemBuilder: _buildDialogItem,
        ),
      );

  return new Stack(
    children: <Widget>[
      new TextFormField(
        focusNode: focusNode,
        controller: controller,
        validator: validator,
        decoration: new InputDecoration(
          prefix: new InkWell(
            child: new Container(
              padding: const EdgeInsets.only(right: 4.0),
              child: new Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  new Text(
                    "+${initialSelectedCountry.phoneCode}",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: textStyle?.color ?? Colors.black,
                      fontSize: 14.0,
                    ),
                  ),
                  new Icon(
                    Icons.expand_more,
                    color: textStyle?.color ?? Colors.black,
                    size: 15,
                  ),
                ],
              ),
            ),
            onTap: _openCountryPickerDialog,
          ),
          labelText: label,
        ),
        style: new TextStyle(fontSize: 14.0, color: Colors.black),
        textInputAction: inputAction,
        obscureText: obscureText,
        inputFormatters: [
          WhitelistingTextInputFormatter(RegExp("[0-9]")),
          LengthLimitingTextInputFormatter(maxLength ?? 1000),
        ]..addAll(inputFormatters ?? []),
        keyboardType: TextInputType.phone,
        onFieldSubmitted: onFieldSubmitted,
        enabled: enabled,
      ),
      new Positioned.fill(
        child: new Offstage(
          offstage: enabled ?? true,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      )
    ],
  );
}

// Returns "App themed Email Phone text field " centered label
Widget appThemedEmailPhoneTextFieldOne({
  @required String label,
  @required Country initialSelectedCountry,
  @required TextEditingController controller,
  @required BuildContext context,
  @required Function(Country) onCountrySelected,
  @required bool isEmail,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  int maxLength,
  String suffixAsset,
  Widget prefixAsset,
  Function onPrefixTap,
  TextInputAction inputAction,
}) {
  Widget _buildDialogItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text("+${country.phoneCode}"),
          SizedBox(width: 8.0),
          Flexible(child: Text(country.name))
        ],
      );

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => new CountryPickerDialog(
          titlePadding: EdgeInsets.all(8.0),
          searchCursorColor: IfincaColors.kAppYellow,
          searchInputDecoration: InputDecoration(
              hintText:
                  Localization.of(context).trans(LocalizationValues.search) +
                      '...'),
          isSearchable: true,
          title: new Text(Localization.of(context)
              .trans(LocalizationValues.selectYourPhoneCode)),
          onValuePicked: onCountrySelected,
          itemBuilder: _buildDialogItem,
        ),
      );

  return new Stack(
    children: <Widget>[
      appThemedTextFormFieldOne(
        focusNode: focusNode,
        context: context,
        suffixAsset: suffixAsset,
        suffixSecondaryAsset: isEmail
            ? new Container()
            : new InkWell(
                child: new Container(
//        color: Colors.blue,
                  child: new Row(
                    children: <Widget>[
                      new Text(
                        "+${initialSelectedCountry.phoneCode}",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: textStyle?.color ?? Colors.white,
                          fontSize: 16.0,
                        ),
                      ),
                      new Icon(
                        Icons.expand_more,
                        color: textStyle?.color ?? Colors.white,
                        size: 15,
                      ),
                    ],
                  ),
                ),
                onTap: _openCountryPickerDialog,
              ),
        controller: controller,
        label: label,
        validator: validator,
        obscureText: obscureText,
        inputFormatters: [
          LengthLimitingTextInputFormatter(maxLength ?? 1000),
        ]..addAll(inputFormatters ?? []),
        keyboardType: TextInputType.text,
        onPrefixOneTap: onPrefixTap,
        prefixOneAsset: prefixAsset,
        onFieldSubmitted: onFieldSubmitted,
        labelStyle: labelStyle,
        borderColor: borderColor,
        enabled: enabled,
        inputAction: inputAction,
        textStyle: textStyle,
      ),
      new Positioned.fill(
        child: new Offstage(
          offstage: enabled ?? true,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      )
    ],
  );
}

// Returns "App themed Username Phone text field " centered label
Widget appThemedUserNamePhoneTextFieldOne({
  @required String label,
  @required Country initialSelectedCountry,
  @required TextEditingController controller,
  @required BuildContext context,
  @required Function(Country) onCountrySelected,
  @required bool isUserName,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  int maxLength,
  String suffixAsset,
  Widget prefixAsset,
  Function onPrefixTap,
  TextInputAction inputAction,
}) {
  Widget _buildDialogItem(Country country) => Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text("+${country.phoneCode}"),
          SizedBox(width: 8.0),
          Flexible(child: Text(country.name))
        ],
      );

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => new CountryPickerDialog(
          titlePadding: EdgeInsets.all(8.0),
          searchCursorColor: IfincaColors.kAppYellow,
          searchInputDecoration: InputDecoration(
              hintText:
                  Localization.of(context).trans(LocalizationValues.search) +
                      '...'),
          isSearchable: true,
          title: new Text(Localization.of(context)
              .trans(LocalizationValues.selectYourPhoneCode)),
          onValuePicked: onCountrySelected,
          itemBuilder: _buildDialogItem,
        ),
      );

  return new Stack(
    children: <Widget>[
      appThemedTextFormFieldOne(
        focusNode: focusNode,
        context: context,
        suffixAsset: suffixAsset,
        suffixSecondaryAsset: isUserName
            ? new Container()
            : new InkWell(
                child: new Container(
//        color: Colors.blue,
                  child: new Row(
                    children: <Widget>[
                      new Text(
                        "+${initialSelectedCountry.phoneCode}",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: textStyle?.color ?? Colors.white,
                          fontSize: 16.0,
                        ),
                      ),
                      new Icon(
                        Icons.expand_more,
                        color: textStyle?.color ?? Colors.white,
                        size: 15,
                      ),
                    ],
                  ),
                ),
                onTap: _openCountryPickerDialog,
              ),
        controller: controller,
        label: label,
        validator: validator,
        obscureText: obscureText,
        inputFormatters: [
          LengthLimitingTextInputFormatter(maxLength ?? 1000),
        ]..addAll(inputFormatters ?? []),
        keyboardType: TextInputType.text,
        onPrefixOneTap: onPrefixTap,
        prefixOneAsset: prefixAsset,
        onFieldSubmitted: onFieldSubmitted,
        labelStyle: labelStyle,
        borderColor: borderColor,
        enabled: enabled,
        inputAction: inputAction,
        textStyle: textStyle,
      ),
      new Positioned.fill(
        child: new Offstage(
          offstage: enabled ?? true,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      )
    ],
  );
}

// Returns "App themed address AutoComplete text field" centered label
Widget appThemedAddressAutoCompleteTextFieldOne({
  @required String label,
  @required bool mounted,
  @required GlobalKey<ScaffoldState> parentScaffoldKey,
  @required TextEditingController controller,
  @required BuildContext context,
  @required Function(Prediction) onGotLocation,
  bool obscureText,
  @required FocusNode focusNode,
  @required VoidCallback updateUI,
  @required ScrollController parentScrollController,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  double parentScrollOffset,
  String suffixAsset,
  Widget prefixAsset,
  Function onPrefixTap,
  TextInputAction inputAction,
  TextCapitalization textCapitalization,
}) {
  // Defaults
  const TextStyle defaultLabelStyle = const TextStyle(
    color: Colors.white,
  );

  const TextStyle defaultTextStyle = const TextStyle(
    color: Colors.white,
  );

  const Color defaultBorderColor = Colors.white;

  List<Prediction> addressList = [];
  return new Stack(
    children: <Widget>[
      new Column(
        children: <Widget>[
          new Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              suffixAsset != null
                  ? new Container(
                      width: getScreenSize(context: context).width * 0.105,
                      height: getScreenSize(context: context).width * 0.1,
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.only(
                        bottom: 6.0,
                        left: 8.0,
                      ),
                      child: new Image.asset(
                        suffixAsset,
                        width: getScreenSize(context: context).width * 0.04,
                        height: getScreenSize(context: context).width * 0.05,
                        fit: BoxFit.fill,
                      ),
                    )
                  : new Container(),
              new Expanded(
                child: new TypeAheadFormField(
                  textFieldConfiguration: TextFieldConfiguration(
                    focusNode: focusNode,
                    maxLines: null,
                    textCapitalization: textCapitalization,
                    decoration: new InputDecoration(
                      labelText: label,
                      labelStyle: labelStyle ?? defaultLabelStyle,
                      contentPadding: EdgeInsets.zero,
                      border: InputBorder.none,
                      errorStyle: new TextStyle(
                        fontSize: 10.0,
//                        color: labelStyle?.color ?? defaultLabelStyle?.color,
                        color: Colors.red,
                      ),
                    ),
                    style: textStyle ?? defaultTextStyle,
                    controller: controller,
                  ),
                  suggestionsCallback: (pattern) async {
                    addressList = await getAutoCompleteAddress(pattern);

                    List<String> addressLineList = [];
                    addressList.forEach((address) {
                      addressLineList.add(address.description);
                    });

                    return addressLineList;
                  },
                  itemBuilder: (context, suggestion) {
                    return new ListTile(
                      title: Text(suggestion),
                    );
                  },
                  transitionBuilder: (context, suggestionsBox, controller) {
                    return suggestionsBox;
                  },
                  onSuggestionSelected: (suggestion, int selectedIndex) {
                    controller.text = suggestion;
                    if (addressList.isNotEmpty) {
                      onGotLocation(addressList[selectedIndex]);
                    }
                  },
                  validator: validator,
                  onSaved: (value) {},
                  label: label,
                  context: context,
                  suffixAsset: suffixAsset,
                  prefixOneAsset: prefixAsset,
                ),
              ),
            ],
          ),
          getSpacer(
            height: 4.0,
          ),
          new Container(
            height: 1.0,
            color: borderColor ?? defaultBorderColor,
          ),
        ],
      ),
      new Positioned.fill(
        child: new GestureDetector(
          onTap: () async {
            if (!focusNode.hasFocus) {
              controller.clear();
              parentScrollController.animateTo(
                0.0,
                duration: new Duration(
                  milliseconds: 200,
                ),
                curve: Curves.bounceIn,
              );

              await Future.delayed(new Duration(
                milliseconds: 400,
              ));

              setFocusNode(context: context, focusNode: focusNode);

              await Future.delayed(new Duration(
                milliseconds: 600,
              ));
              parentScrollController.animateTo(
                parentScrollController.offset + (parentScrollOffset ?? 150.0),
                duration: new Duration(
                  milliseconds: 200,
                ),
                curve: Curves.ease,
              );
            }
          },
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Returns "App themed AutoComplete text field" centered label
Widget appThemedAutoCompleteDropDownTextFieldTwo({
  GlobalKey key,
  @required String label,
  @required bool mounted,
  @required GlobalKey<ScaffoldState> parentScaffoldKey,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  @required VoidCallback updateUI,
  @required ScrollController parentScrollController,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled = true,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  Color dropDownIconColor,
  double parentScrollOffset,
  String suffixAsset,
  Widget prefixAsset,
  Function onPrefixTap,
  TextInputAction inputAction,
  TextCapitalization textCapitalization,
  @required SuggestionSelectionCallback onSuggestionSelected,
  @required ItemBuilder itemBuilder,
  @required SuggestionsCallback suggestionsCallback,
  bool autoValidate,
  int maxLines,
  int maxLength,
  Widget suffix,
  Widget prefix,
  Function onSuffixTap,
  Function onSetFocus,
  bool fixedHeight = true,
}) {
  // Defaults
  const TextStyle defaultLabelStyle = const TextStyle(
    color: Colors.black,
    fontSize: 14.0,
  );
  final TextStyle defaultTextStyle = new TextStyle(
    //    fontWeight: FontWeight.w600,
    color: enabled ? Colors.black : Colors.black.withOpacity(0.6),
//    fontSize: 18.0,
  );
  const Color defaultBorderColor = IfincaColors.kGrey;

  final double _height = getScreenSize(context: context).height * 0.067;
  final double defaultHeight = _height > minimumDefaultButtonHeight
      ? _height
      : minimumDefaultButtonHeight;

  // Sets field ready
  Future _setFieldReady() async {
    if (!focusNode.hasFocus) {
      // Clearing field and setting focus
      controller.clear();

      if (onSetFocus != null) {
        onSetFocus();
      }

      setFocusNode(
        context: context,
        focusNode: focusNode,
      );

      await Future.delayed(new Duration(
        milliseconds: 500,
      ));

      // Getting position
      final RenderBox renderBox = key.currentContext.findRenderObject();
      final position = renderBox.localToGlobal(Offset.zero);

      num textFieldBottomPadding =
          getScreenSize(context: context).height - position.dy;

      // 170- suggestion box height
      // Scrolling parent if required
      if (textFieldBottomPadding < 170.0) {
        await Future.delayed(new Duration(
          milliseconds: 500,
        ));

        parentScrollController.animateTo(
          parentScrollController.offset + 170.0,
          duration: new Duration(
            milliseconds: 200,
          ),
          curve: Curves.ease,
        );
      }
    }
  }

  return new Card(
    key: key,
    elevation: 2.0,
    shape: new RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(defaultHeight / 2.0),
      ),
    ),
    child: new Stack(
      children: <Widget>[
        new ConstrainedBox(
          constraints: new BoxConstraints(
            minHeight: defaultHeight,
          ),
          child: new Container(
            height: fixedHeight ? defaultHeight : null,
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(
              horizontal: 24.0,
            ),
            child: new Row(
              children: <Widget>[
                prefix ?? new Container(),
                new Expanded(
                  child: new Stack(
                    children: <Widget>[
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              suffixAsset != null
                                  ? new Container(
                                      width: getScreenSize(context: context)
                                              .width *
                                          0.105,
                                      height: getScreenSize(context: context)
                                              .width *
                                          0.1,
                                      alignment: Alignment.centerLeft,
                                      padding: const EdgeInsets.only(
                                        bottom: 6.0,
                                        left: 8.0,
                                      ),
                                      child: new Image.asset(
                                        suffixAsset,
                                        width: getScreenSize(context: context)
                                                .width *
                                            0.04,
                                        height: getScreenSize(context: context)
                                                .width *
                                            0.05,
                                        fit: BoxFit.fill,
                                      ),
                                    )
                                  : new Container(),
                              new Expanded(
                                child: new TypeAheadFormField(
                                  textFieldConfiguration:
                                      TextFieldConfiguration(
                                    focusNode: focusNode,
                                    maxLines: null,
                                    textCapitalization: textCapitalization ??
                                        TextCapitalization.none,
                                    decoration: new InputDecoration(
                                      labelText: label,
                                      labelStyle:
                                          labelStyle ?? defaultLabelStyle,
                                      contentPadding: EdgeInsets.zero,
                                      border: InputBorder.none,
                                      errorStyle: new TextStyle(
                                        fontSize: 10.0,
//                                        color: labelStyle?.color ??
//                                            defaultLabelStyle?.color,
                                        color: Colors.red,
                                      ),
                                    ),
                                    style: textStyle ?? defaultTextStyle,
                                    controller: controller,
                                  ),
                                  getImmediateSuggestions: true,
                                  suggestionsCallback: suggestionsCallback,
                                  itemBuilder: itemBuilder,
                                  transitionBuilder:
                                      (context, suggestionsBox, controller) {
                                    return suggestionsBox;
                                  },
                                  loadingBuilder: (context) {
                                    return getAppThemedLoader(
                                      context: context,
                                      bgColor: Colors.white,
                                      color: Colors.black,
                                    );
                                  },
//                                  noItemsFoundBuilder: (context) {
//                                    return new Padding(
//                                      padding: const EdgeInsets.symmetric(vertical: 16.0),
//                                      child: new Text(
//                                        "Nothing found",
//                                      ),
//                                    );
//                                  },
                                  onSuggestionSelected: onSuggestionSelected,
                                  validator: validator,
                                  onSaved: (value) {},
                                  label: label,
                                  context: context,
                                  suffixAsset: suffixAsset,
                                  prefixOneAsset: prefixAsset,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      new Positioned.fill(
                        child: new GestureDetector(
                          onTap: () async {
                            await _setFieldReady();
                          },
                          child: new Container(
                            color: Colors.cyanAccent.withOpacity(0.0),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                new Offstage(
                  offstage: !enabled,
                  child: new InkWell(
                    child: suffix ??
                        new Icon(
                          Icons.keyboard_arrow_down,
                          color: dropDownIconColor ?? Colors.white,
                        ),
                    onTap: () async {
                      await _setFieldReady();
                      if (onSuffixTap != null) {
                        onSuffixTap();
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ),
        new Positioned.fill(
          child: new Offstage(
            offstage: enabled,
            child: new Container(
              color: Colors.cyanAccent.withOpacity(0.0),
            ),
          ),
        ),
      ],
    ),
  );
}

// Returns "App themed Map text field" centered label
Widget appThemedMapTextFieldOne({
  @required String label,
  @required bool mounted,
  @required TextEditingController controller,
  @required BuildContext context,
  @required Function(Address) onGotLocation,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  String suffixAsset,
  Widget prefixAsset,
  Function onPrefixTap,
  TextInputAction inputAction,
  TextCapitalization textCapitalization,
}) {
  return new Stack(
    children: <Widget>[
      appThemedTextFormFieldOne(
        focusNode: focusNode,
        context: context,
        suffixAsset: suffixAsset,
        controller: controller,
        label: label,
        validator: validator,
        obscureText: obscureText,
        textCapitalization: textCapitalization,
        onPrefixOneTap: () async {
          bool gotInternetConnection = await hasInternetConnection(
            context: context,
            mounted: mounted,
            canShowAlert: true,
            onFail: () {
              //hide loader
            },
            onSuccess: () {},
          );
//
          if (gotInternetConnection) {
            Address selectedAddress = await Navigator.push(
              context,
              new FadeTransitionRoute<Address>(
                widget: new SelectLocation(),
              ),
            );

            if (selectedAddress != null && onGotLocation != null) {
              onGotLocation(selectedAddress);
            }
          }
        },
        prefixOneAsset: prefixAsset,
        onFieldSubmitted: onFieldSubmitted,
        labelStyle: labelStyle,
        borderColor: borderColor,
//        enabled: false,
        inputAction: inputAction,
        textStyle: textStyle,
        maxLines: 3,
      ),
//      new Positioned.fill(
//        child: new InkWell(
//          onTap: () async {
//            bool gotInternetConnection = await hasInternetConnection(
//              context: context,
//              mounted: mounted,
//              canShowAlert: true,
//              onFail: () {
//                //hide loader
//              },
//              onSuccess: () {},
//            );
////
//            if (gotInternetConnection) {
//              Address selectedAddress = await Navigator.push(
//                context,
//                new FadeTransitionRoute<Address>(
//                  widget: new SelectLocation(),
//                ),
//              );
//
//              if (selectedAddress != null && onGotLocation != null) {
//                onGotLocation(selectedAddress);
//              }
//            }
//          },
//          child: new Container(
//            color: Colors.white.withOpacity(0.0),
//          ),
//        ),
//      ),
    ],
  );
}

// Returns "App themed Map text field" centered label
Widget appThemedMapTextFieldTwo({
  @required String label,
  @required bool mounted,
  @required TextEditingController controller,
  @required BuildContext context,
  @required Function(Address) onGotLocation,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  Widget suffix,
  Widget prefix,
  Function onPrefixTap,
  TextInputAction inputAction,
  TextCapitalization textCapitalization,
}) {
  return new Stack(
    children: <Widget>[
      appThemedTextFieldTwo(
        focusNode: focusNode,
        context: context,
        suffix: suffix,
        controller: controller,
        label: label,
        validator: validator,
        obscureText: obscureText,
        textCapitalization: textCapitalization,
        onSuffixTap: () async {
          bool gotInternetConnection = await hasInternetConnection(
            context: context,
            mounted: mounted,
            canShowAlert: true,
            onFail: () {
              //hide loader
            },
            onSuccess: () {},
          );
//
          if (gotInternetConnection) {
            Address selectedAddress = await Navigator.push(
              context,
              new FadeTransitionRoute<Address>(
                widget: new SelectLocation(),
              ),
            );

            if (selectedAddress != null && onGotLocation != null) {
              onGotLocation(selectedAddress);
            }
          }
        },
        prefix: prefix,
        onFieldSubmitted: onFieldSubmitted,
        labelStyle: labelStyle,
        borderColor: borderColor,
//        enabled: false,
        inputAction: inputAction,
        textStyle: textStyle,
        maxLines: 3,
      ),
//      new Positioned.fill(
//        child: new InkWell(
//          onTap: () async {
//            bool gotInternetConnection = await hasInternetConnection(
//              context: context,
//              mounted: mounted,
//              canShowAlert: true,
//              onFail: () {
//                //hide loader
//              },
//              onSuccess: () {},
//            );
////
//            if (gotInternetConnection) {
//              Address selectedAddress = await Navigator.push(
//                context,
//                new FadeTransitionRoute<Address>(
//                  widget: new SelectLocation(),
//                ),
//              );
//
//              if (selectedAddress != null && onGotLocation != null) {
//                onGotLocation(selectedAddress);
//              }
//            }
//          },
//          child: new Container(
//            color: Colors.white.withOpacity(0.0),
//          ),
//        ),
//      ),
    ],
  );
}

// Returns "App themed text field" left sided label
Widget appThemedTextFieldTwo({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled = true,
  bool autoValidate,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  int maxLines,
  int maxLength,
  Widget suffix,
  Widget prefix,
  Function onSuffixTap,
  TextInputAction inputAction,
  TextCapitalization textCapitalization,
  bool fixedHeight = true,
}) {
  // Defaults
  const TextStyle defaultLabelStyle = const TextStyle(
    color: Colors.black,
    fontSize: 14.0,
  );
  final TextStyle defaultTextStyle = new TextStyle(
    //    fontWeight: FontWeight.w600,
    color: enabled ? Colors.black : Colors.black.withOpacity(0.6),
//    fontSize: 18.0,
  );
  const Color defaultBorderColor = IfincaColors.kGrey;

  final double _height = getScreenSize(context: context).height * 0.067;
  final double defaultHeight = _height > minimumDefaultButtonHeight
      ? _height
      : minimumDefaultButtonHeight;

  return new Card(
    elevation: 2.0,
    shape: new RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(defaultHeight / 2.0),
      ),
    ),
    child: new Stack(
      children: <Widget>[
        new ConstrainedBox(
          constraints: new BoxConstraints(
            minHeight: defaultHeight,
          ),
          child: new Container(
            height: fixedHeight ? defaultHeight : null,
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(
              horizontal: 24.0,
            ),
            child: new Row(
              children: <Widget>[
                prefix ?? new Container(),
                new Expanded(
                  child: new TextFormField(
                    textInputAction: inputAction ?? TextInputAction.next,
                    controller: controller,
                    textCapitalization:
                        textCapitalization ?? TextCapitalization.none,
                    obscureText: obscureText ?? false,
                    focusNode: focusNode,
                    keyboardType: keyboardType,
                    validator: validator,
                    autovalidate: autoValidate ?? false,
                    inputFormatters: [
                      LengthLimitingTextInputFormatter(maxLength ?? 1000),
                    ]..addAll(inputFormatters ?? []),
                    onFieldSubmitted: onFieldSubmitted,
                    style: textStyle ?? defaultTextStyle,
                    maxLines: fixedHeight ? maxLines ?? 1 : null,
                    decoration: new InputDecoration(
                      labelText: label,
                      labelStyle: labelStyle ?? defaultLabelStyle,
                      border: InputBorder.none,
                      contentPadding:
//                      fixedHeight
//                          ?
                          EdgeInsets.zero
//                          : const EdgeInsets.symmetric(
//                              vertical: 10.5,
//                            )
                      ,
                      errorStyle: new TextStyle(
                        fontSize: 10.0,
//                        color: labelStyle?.color ?? defaultLabelStyle?.color,
                        color: Colors.red,
                      ),
                      helperStyle: new TextStyle(
                        fontSize: 0.0,
                        color: Colors.black,
                      ),
                      isDense: true,
                    ),
                  ),
                ),
                new InkWell(
                  child: suffix ?? new Container(),
                  onTap: onSuffixTap,
                )
              ],
            ),
          ),
        ),
        new Positioned.fill(
          child: new Offstage(
            offstage: enabled,
            child: new Container(
              color: Colors.cyanAccent.withOpacity(0.0),
            ),
          ),
        ),
      ],
    ),
  );
}

// Returns "App themed Phone text field" centered label
Widget appThemedPhoneTextFieldTwo({
  @required String label,
  @required Country initialSelectedCountry,
  @required TextEditingController controller,
  @required BuildContext context,
  @required Function(Country) onCountrySelected,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled = true,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  Widget prefixAsset,
  Function onPrefixTap,
  TextInputAction inputAction,
  int maxLength,
}) {
  Widget _buildDialogItem(Country country) => new Row(
        children: <Widget>[
          CountryPickerUtils.getDefaultFlagImage(country),
          SizedBox(width: 8.0),
          Text("+${country.phoneCode}"),
          SizedBox(width: 8.0),
          Flexible(child: Text(country.name))
        ],
      );

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => new CountryPickerDialog(
          titlePadding: EdgeInsets.all(8.0),
          searchCursorColor: IfincaColors.kAppYellow,
          searchInputDecoration: InputDecoration(
              hintText:
                  "Search" +
                      '...'),
          isSearchable: true,
          title: Text(Localization.of(context)
              .trans(LocalizationValues.selectYourPhoneCode)),
          onValuePicked: onCountrySelected,
          itemBuilder: _buildDialogItem,
        ),
      );

  return appThemedTextFieldTwo(
    context: context,
    label: label,
    labelStyle: labelStyle,
    borderColor: borderColor,
    textStyle: textStyle,
    prefix: new InkWell(
      child: new Container(
//        color: Colors.cyanAccent,
        child: new Padding(
          padding: const EdgeInsets.only(
            bottom: 1.5,
          ),
          child: new Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Text("+${initialSelectedCountry.phoneCode}",
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 15.0,
                    fontWeight: FontWeight.w600,
                  )),
              new Icon(
                Icons.expand_more,
                color: Colors.black,
                size: 15,
              ),
            ],
          ),
        ),
      ),
      onTap: _openCountryPickerDialog,
    ),
    inputFormatters: [
      WhitelistingTextInputFormatter(RegExp("[0-9]")),
      LengthLimitingTextInputFormatter(maxLength ?? 1000),
    ]..addAll(inputFormatters ?? []),
    keyboardType: TextInputType.phone,
    controller: controller,
    obscureText: obscureText ?? false,
    focusNode: focusNode,
    validator: validator,
    onFieldSubmitted: onFieldSubmitted,
    enabled: enabled,
  );
}

// Returns "App themed text field" left sided label
Widget appThemedTextFieldThree({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled = true,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  int maxLines,
  int maxLength,
  Widget suffix,
  Widget prefix,
}) {
  // Defaults
  const TextStyle defaultLabelStyle = const TextStyle(
    color: Colors.black,
    fontSize: 14.0,
  );
  final TextStyle defaultTextStyle = new TextStyle(
//    fontWeight: FontWeight.w600,
    color: enabled ? Colors.black : Colors.black.withOpacity(0.6),
    fontSize: 18.0,
  );
  const Color defaultBorderColor = IfincaColors.kGrey;

  return new Theme(
    data: new ThemeData(
      hintColor: IfincaColors.kAppBlack,
      primaryColor: IfincaColors.kAppBlack,
    ),
    child: new TextFormField(
      controller: controller,
      obscureText: obscureText ?? false,
      focusNode: focusNode,
      keyboardType: keyboardType,
      validator: validator,
      inputFormatters: [
        LengthLimitingTextInputFormatter(maxLength ?? 1000),
      ]..addAll(inputFormatters ?? []),
      onFieldSubmitted: onFieldSubmitted,
      enabled: enabled,
      style: textStyle ?? defaultTextStyle,
      maxLines: maxLines ?? 1,
      decoration: new InputDecoration(
        labelText: label,
        labelStyle: labelStyle ?? defaultLabelStyle,
//      border: InputBorder.none,
        contentPadding: const EdgeInsets.only(
          top: 5.0,
          bottom: 0.0,
        ),
        errorStyle: new TextStyle(
          fontSize: 10.0,
          color: Colors.red,
        ),
        helperStyle: new TextStyle(
          fontSize: 0.0,
        ),
        isDense: true,
        suffixIcon: suffix,
        prefixIcon: prefix,
      ),
    ),
  );
}

// Returns Ifinca themed Outline button
Widget getIfincaOutlineButton({
  @required VoidCallback onPressed,
  @required BuildContext context,
  Widget title,
  Color titleTextColor,
  String titleText,
  double height,
  Color backGroundColor,
  double borderRadius,
  double borderWidth,
  Color borderColor,
  bool inheritCc,
}) {
  // Defaults
  const double defaultBorderRadius = 30.0;
  const Color defaultBorderColor = Colors.white;
  const Color defaultBackGroundColor = Colors.transparent;
  const Color defaultDisabledBackgroundColor = Colors.grey;
  const double defaultBorderWidth = 0.8;
  final double _height = getScreenSize(context: context).height * 0.067;
  final double defaultHeight = _height > minimumDefaultButtonHeight
      ? _height
      : minimumDefaultButtonHeight;

  return new Material(
    color: onPressed != null
        ? backGroundColor ?? defaultBackGroundColor
        : defaultDisabledBackgroundColor,
    shape: StadiumBorder(),
    elevation: 0.0,
    clipBehavior: Clip.hardEdge,
    child: new InkWell(
      onTap: onPressed,
      child: new Container(
        height: height ?? defaultHeight,
        width: getScreenSize(context: context).width,
        alignment: Alignment.center,
        decoration: new BoxDecoration(
          border: new Border.all(
            color: borderColor ?? defaultBorderColor,
            width: 1.0,
          ),
          borderRadius: BorderRadius.circular((height ?? defaultHeight) / 2.0),
        ),
        padding: new EdgeInsets.symmetric(
          horizontal: getScreenSize(context: context).width * 0.03,
        ),
        child: title ??
            new Text(
              titleText,
              textAlign: TextAlign.center,
              style: new TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 15.0,
                color: titleTextColor ?? Colors.white,
              ),
            ),
      ),
    ),
  );
}

// Returns Ifinca themed Raised button
Widget getIfincaRaisedButton({
  @required Widget title,
  @required VoidCallback onPressed,
  @required BuildContext context,
  Color backGroundColor,
}) {
  // Defaults

  const Color defaultBackGroundColor = IfincaColors.kOrange;
  const Color _defaultDisabledBG = IfincaColors.kGrey;
  final double _height = getScreenSize(context: context).height * 0.067;
  final double defaultHeight = _height > minimumDefaultButtonHeight
      ? _height
      : minimumDefaultButtonHeight;

  return new Material(
    color: onPressed != null
        ? backGroundColor ?? defaultBackGroundColor
        : _defaultDisabledBG,
//    borderRadius: new BorderRadius.circular(10.0),
    shape: StadiumBorder(),
    elevation: 3,
    clipBehavior: Clip.hardEdge,
    child: new InkWell(
      onTap: onPressed,
      child: new Container(
        height: defaultHeight,
        width: getScreenSize(context: context).width,
        alignment: Alignment.center,
        child: title,
      ),
    ),
  );
}

// Returns Ifinca themed flat button
Widget getIfincaFlatButton({
  @required BuildContext context,
  Widget title,
  @required String titleText,
  @required VoidCallback onPressed,
  double height,
  Color backGroundColor,
}) {
  // Defaults
  const Color defaultBackGroundColor = IfincaColors.kAppYellow;

  final double _height = getScreenSize(context: context).height * 0.067;
  final double defaultHeight = _height > minimumDefaultButtonHeight
      ? _height
      : minimumDefaultButtonHeight;
  const Color defaultDisabledBackgroundColor =
      IfincaColors.kDisabledButtonColor;

  return Container(
    height: height ?? defaultHeight,
    width: getScreenSize(context: context).width,
    child: new FlatButton(
      shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(30.0),
      ),
      onPressed: onPressed,
      child: new Text(
            titleText,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 15.0,
            ),
          ) ??
          title,
      color: backGroundColor ?? defaultBackGroundColor,
      disabledColor: defaultDisabledBackgroundColor,
    ),
  );
}

// Returns spacer
Widget getSpacer({double height, double width}) {
  return new SizedBox(
    height: height ?? 0.0,
    width: width ?? 0.0,
  );
}

// Returns cached image
Widget getCachedNetworkImage({@required String url, BoxFit fit}) {
  return new CachedNetworkImage(
    width: double.infinity,
    height: double.infinity,
    imageUrl: url ?? "",
    matchTextDirection: true,
    fit: fit,
    placeholder: (context, String val) {
      return new Center(
        child: new CupertinoActivityIndicator(),
      );
    },
    errorWidget: (BuildContext context, String error, Object obj) {
      return new Center(
        child: new Image.asset(
          AssetStrings.logoImage,
          fit: BoxFit.fill,
          color: Colors.grey,
          height: 24.0,
        ),
      );
    },
  );
}

// Returns ifinca themed Hexagon clip
Widget getHexagonClip(
    {@required Widget child, double elevation, Color backgroundColor}) {
  return new PhysicalShape(
    clipper: new HexagonClip(),
    color: backgroundColor ?? Colors.transparent,
    elevation: elevation ?? 0.0,
    child: new ClipPath(
      clipper: new HexagonClip(),
      child: child,
    ),
  );
}

// Returns ifinca themed Sloped bottom clip
Widget getSlopedBottomClip({@required Widget child}) {
  return new ClipPath(
    clipper: new SlopedBottomClip(),
    child: child,
  );
}

// Returns app themed list view loader
Widget getChildLoader({
  Color color,
  double strokeWidth,
}) {
  return new Center(
    child: new CircularProgressIndicator(
      backgroundColor: Colors.transparent,
      valueColor: new AlwaysStoppedAnimation<Color>(
        color ?? Colors.white,
      ),
      strokeWidth: strokeWidth ?? 6.0,
    ),
  );
}

// Returns app themed loader
Widget getAppThemedLoader({
  @required BuildContext context,
  Color bgColor,
  Color color,
  double strokeWidth,
}) {
  return new Container(
    color: bgColor ?? const Color.fromRGBO(1, 1, 1, 0.6),
    height: getScreenSize(context: context).height,
    width: getScreenSize(context: context).width,
    child: getChildLoader(
      color: color ?? IfincaColors.kAppYellow,
      strokeWidth: strokeWidth,
    ),
  );
}

// Returns app themed loader
Widget getFullScreenLoader({
  @required Stream<bool> stream,
  @required BuildContext context,
  Color bgColor,
  Color color,
  double strokeWidth,
}) {
  return new StreamBuilder<bool>(
    stream: stream,
    initialData: false,
    builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
      bool status = snapshot.data;
      return status
          ? getAppThemedLoader(
              context: context,
            )
          : new Container();
    },
  );
}

Widget appThemedDatePickerTextField({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  @required DateTime initialDate,
  DateTime lastDate,
  DateTime firstDate,
  @required Function(DateTime) onDatePicked,
  String dateFormat,
  @required String suffixAsset,
}) {
  return new GestureDetector(
    onTap: enabled
        ? () async {
            DateTime _today = ((DateTime time) => new DateTime.utc(
                time.year, time.month, time.day))(DateTime.now());

            DateTime _picked = await showDatePicker(
              context: context,
              firstDate: firstDate ?? _today,
              lastDate:
                  lastDate ?? _today.add(new DateTime.now().timeZoneOffset),
              initialDate: initialDate ?? firstDate,
            );

            if (_picked != null) {
              controller.text = getFormattedDateString(
                format: "MMM dd, y",
                dateTime: _picked,
              );
              onDatePicked(_picked);
            }
          }
        : null,
    child: new Stack(
      children: <Widget>[
        appThemedTextFormFieldOne(
          context: context,
          focusNode: focusNode,
          validator: validator,
          controller: controller,
          label: label,
          suffixAsset: suffixAsset,
          enabled: enabled,
        ),
        new Positioned(
          top: 0.0,
          bottom: 0.0,
          left: 0.0,
          right: 0.0,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(
              0.0,
            ),
          ),
        ),
      ],
    ),
  );
}

//Widget appThemedTimePickerTextField({
//  @required String label,
//  @required TextEditingController controller,
//  @required BuildContext context,
//  bool obscureText,
//  @required FocusNode focusNode,
//  TextInputType keyboardType,
//  Function(String) validator,          // todo: acc time callback
//
//  List<TextInputFormatter> inputFormatters,
//  Function(String) onFieldSubmitted,
//  bool enabled,
//  TextStyle textStyle,
//  TextStyle labelStyle,
//  Color borderColor,
//  @required Function(TimeOfDay) onTimePicked,
//  String dateFormat,
//}) {
//  return new GestureDetector(
//    onTap: () async {
//      DateTime _today = ((DateTime time) =>
//          new DateTime.utc(time.year, time.month, time.day))(DateTime.now());
//      TimeOfDay _currentTime = new TimeOfDay.now();
//
//      TimeOfDay _picked = await showTimePicker(
//        initialTime: _currentTime,
//        context: context,
//      );
//
//      if (_picked != null) {
//        DateTime tempDate = new DateTime(2019,1,12,_picked.hour,_picked.minute);
//        controller.text = new DateFormat.jm().format(tempDate);
//        onTimePicked(_picked);
//      }
//    },
//    child: new Stack(
//      children: <Widget>[
//        appThemedTextFieldTwo(
//          context: context,
//          focusNode: focusNode,
//          validator: validator,
//          controller: controller,
//          label: label,
//        ),
//        new Positioned(
//          top: 0.0,
//          bottom: 0.0,
//          left: 0.0,
//          right: 0.0,
//          child: new Container(
//            color: Colors.cyanAccent.withOpacity(
//              0.0,
//            ),
//          ),
//        ),
//      ],
//    ),
//  );
//}

// Returns app themed pop up menu text field one
Widget appThemedCheckListPopUpTextFieldOne({
  @required String label,
  @required String popUpTitle,
  @required TextEditingController controller,
  @required BuildContext context,
  @required List<int> selectedOptions,
  @required String otherSelectedOption,
  bool obscureText,
  bool multipleSelection = true,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  Color dropDownIconColor,
  @required Map<String, String> options,
  Function(Map<String, String>) onOptionsRefresh,
  Future<Map<String, String>> Function() refreshOptions,
  bool allowInput = false,
  @required Function(List<int>, {String other}) onDone,
}) {
  List<String> _options = options.keys.toList();
  List<String> _values = options.values.toList();

  // Sets selected options string in text field
  void setSelectedString({
    @required List<int> selectedOptions,
    @required String other,
  }) {
    String selectedOptionsString = "";
    if (selectedOptions != null && (_options?.isNotEmpty ?? false)) {
      for (int index = 0; index < selectedOptions.length; index++) {
        if (selectedOptions[index] != -1) {
          selectedOptionsString =
              (selectedOptionsString + _options[selectedOptions[index]]) +
                  (index == selectedOptions.length - 1 ? "" : ", ");
        }
      }
    }
    if (other != null && other != "") {
      selectedOptionsString = selectedOptionsString +
          (selectedOptionsString.isEmpty ? "" : ", ") +
          other;
    }
    controller?.text = selectedOptionsString;
  }

  setSelectedString(
    selectedOptions: selectedOptions,
    other: otherSelectedOption,
  );

  return new Stack(
    children: <Widget>[
      appThemedTextFormFieldOne(
        focusNode: focusNode,
        context: context,
        suffixAsset: null,
        controller: controller,
        label: label,
        validator: validator,
        obscureText: obscureText,
        onFieldSubmitted: onFieldSubmitted,
        labelStyle: labelStyle,
        borderColor: borderColor,
        enabled: false,
        maxLines: 3,
        prefixTwoAsset: new Align(
          alignment: Alignment.bottomRight,
          child: new Icon(
            Icons.keyboard_arrow_down,
            color: dropDownIconColor ?? Colors.white,
          ),
        ),
        textStyle: textStyle,
      ),
      new Positioned.fill(
        child: new InkWell(
          onTap: () async {
            showDialog(
                context: context,
                builder: (context) {
                  return new Dialog(
                    child: new CheckBoxListPopUpMenu(
                      options: options,
                      title: popUpTitle,
                      onOptionsRefresh: onOptionsRefresh,
                      refresh: refreshOptions,
                      label: label,
                      multipleSelection: multipleSelection,
                      selectedOptions: selectedOptions,
                      otherSelectedOption: otherSelectedOption,
                      onDone: (newSelectedOptions,
                          {String other,
                          Map<String, String> updatedOptionsList}) {
                        if (updatedOptionsList.isNotEmpty) {
                          _options = updatedOptionsList.keys.toList();
                        }
                        String _other = other ?? "";
                        if (newSelectedOptions != null) {
                          setSelectedString(
                            selectedOptions: newSelectedOptions,
                            other: _other,
                          );
                        }
                        if (onDone != null) {
                          onDone(
                            newSelectedOptions ?? [],
                            other: _other,
                          );
                        }
                      },
                    ),
                  );
                });
          },
          child: new Container(
            color: Colors.white.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Returns app themed drop down menu text field Two
Widget appThemedDropDownTextFieldTwo({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  Color dropDownIconColor,
  @required Map<dynamic, dynamic> options,
  bool allowInput = false,
  Function(String) onValueChanged,
}) {
  List _options = options?.values?.toList() ?? [];
  List _values = options?.keys?.toList() ?? [];

  return new Stack(
    children: <Widget>[
      new PopupMenuButton<String>(
        child: appThemedTextFieldTwo(
          focusNode: focusNode,
          context: context,
          fixedHeight: false,
          controller: controller,
          label: label,
//        maxLines: 1,
          validator: validator,
          obscureText: obscureText,
          onFieldSubmitted: onFieldSubmitted,
          labelStyle: labelStyle,
          borderColor: borderColor,
          enabled: false,
          suffix: new Align(
            alignment: Alignment.center,
            child: new Icon(
              Icons.keyboard_arrow_down,
              color: enabled
                  ? dropDownIconColor ?? Colors.black
                  : Colors.transparent,
            ),
          ),
          textStyle: textStyle,
        ),
        onSelected: (String result) {
          controller?.text = result;
          if (onValueChanged != null) {
            onValueChanged(result);
          }
        },
        itemBuilder: (BuildContext context) =>
            <PopupMenuEntry<String>>[]..addAll(
                new List.generate(
                  options.keys.length,
                  (int index) {
                    return new PopupMenuItem<String>(
                      value: _options[index],
                      child: new Text(_values[index]),
                    );
                  },
                ),
              ),
      ),
      new Positioned(
        top: 0.0,
        bottom: 0.0,
        left: 0.0,
        right: 0.0,
        child: new Offstage(
          offstage: options?.isNotEmpty ?? false,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Returns app themed pop up menu text field Two
Widget appThemedCheckListPopUpTextFieldTwo({
  @required String label,
  @required String popUpTitle,
  @required TextEditingController controller,
  @required BuildContext context,
  @required List<int> selectedOptions,
  @required String otherSelectedOption,
  bool obscureText,
  bool multipleSelection = true,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  Color dropDownIconColor,
  @required Map<String, String> options,
  Function(Map<String, String>) onOptionsRefresh,
  Future<Map<String, String>> Function() refreshOptions,
  bool allowInput = false,
  @required Function(List<int>, {String other}) onDone,
}) {
  List<String> _options = options.keys.toList();
  List<String> _values = options.values.toList();

  // Sets selected options string in text field
  void setSelectedString({
    @required List<int> selectedOptions,
    @required String other,
  }) {
    String selectedOptionsString = "";

    if (selectedOptions != null && (_options?.isNotEmpty ?? false)) {
      for (int index = 0; index < selectedOptions.length; index++) {
        if (selectedOptions[index] != -1) {
          selectedOptionsString =
              (selectedOptionsString + _options[selectedOptions[index]]) +
                  (index == selectedOptions.length - 1 ? "" : ", ");
        }
      }
    }
    if (other != null) {
      selectedOptionsString = selectedOptionsString +
          (selectedOptionsString.isEmpty ? "" : ", ") +
          other;
    }
    controller.text = selectedOptionsString;
  }

  setSelectedString(
    selectedOptions: selectedOptions,
    other: otherSelectedOption,
  );

  return new Stack(
    children: <Widget>[
      appThemedTextFieldTwo(
        focusNode: focusNode,
        context: context,
        fixedHeight: false,
        controller: controller,
        label: label,
//        maxLines: 1,
        validator: validator,
        obscureText: obscureText,
        onFieldSubmitted: onFieldSubmitted,
        labelStyle: labelStyle,
        borderColor: borderColor,
        enabled: false,
        suffix: new Align(
          alignment: Alignment.center,
          child: new Icon(
            Icons.keyboard_arrow_down,
            color: enabled
                ? dropDownIconColor ?? Colors.black
                : Colors.transparent,
          ),
        ),
        textStyle: textStyle,
      ),
      new Positioned.fill(
        child: new InkWell(
          onTap: enabled ?? true
              ? () async {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return new Dialog(
                          child: new CheckBoxListPopUpMenu(
                            options: options,
                            title: popUpTitle,
                            onOptionsRefresh: onOptionsRefresh,
                            refresh: refreshOptions,
                            label: label,
                            multipleSelection: multipleSelection,
                            selectedOptions: selectedOptions,
                            otherSelectedOption: otherSelectedOption,
                            onDone: (newSelectedOptions,
                                {String other,
                                Map<String, String> updatedOptionsList}) {
                              if (updatedOptionsList.isNotEmpty) {
                                _options = updatedOptionsList.keys.toList();
                              }
                              String _other = other ?? "";
                              if (newSelectedOptions != null) {
                                setSelectedString(
                                  selectedOptions: newSelectedOptions,
                                  other: _other,
                                );
                              }
                              if (onDone != null) {
                                onDone(
                                  newSelectedOptions ?? [],
                                  other: _other,
                                );
                              }
                            },
                          ),
                        );
                      });
                }
              : null,
          child: new Container(
            color: Colors.white.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Returns app themed pop up textfield one
Widget appThemedPopUpTextFieldOne({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled = true,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  Color dropDownIconColor,
  @required Map<String, String> options,
  bool allowInput = false,
  VoidCallback onValueChanged,
}) {
  List _options = options.keys.toList();
  List _values = options.values.toList();

  return new Stack(
    children: <Widget>[
      new PopupMenuButton<String>(
        child: new Stack(
          children: <Widget>[
            appThemedTextFormFieldOne(
              focusNode: focusNode,
              context: context,
              suffixAsset: null,
              controller: controller,
              label: label,
              validator: validator,
              obscureText: obscureText,
              onFieldSubmitted: onFieldSubmitted,
              labelStyle: labelStyle,
              borderColor: borderColor,
              enabled: false,
              prefixTwoAsset: new Align(
                alignment: Alignment.bottomRight,
                child: options.isEmpty
                    ? new CupertinoActivityIndicator(
                        radius: 8.0,
                      )
                    : new Icon(
                        Icons.keyboard_arrow_down,
                        color: dropDownIconColor ?? Colors.white,
                      ),
              ),
              textStyle: textStyle,
            ),
            new Positioned.fill(
              child: new Container(
                color: Colors.cyanAccent.withOpacity(0.0),
              ),
            ),
          ],
        ),
        onSelected: (String result) {
          controller.text = result;
          if (onValueChanged != null) {
            onValueChanged();
          }
        },
        itemBuilder: (BuildContext context) =>
            <PopupMenuEntry<String>>[]..addAll(
                new List.generate(
                  options.keys.length,
                  (int index) {
                    return new PopupMenuItem<String>(
                      value: _options[index],
                      child: new Text(_values[index]),
                    );
                  },
                ),
              ),
      ),
      new Positioned.fill(
        child: new Visibility(
          visible: !enabled || options.isEmpty,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Returns app themed pop up textfield two
Widget appThemedPopUpTextFieldTwo({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  @required Map<String, String> options,
  bool allowInput = false,
  Function onValueChanged,
}) {
  Map<String, String> optionsList = enabled ? options : {};
  List _options = optionsList.keys.toList();
  List _values = optionsList.values.toList();

  return new Stack(
    children: <Widget>[
      new PopupMenuButton<String>(
        child: new Stack(
          children: <Widget>[
            appThemedTextFieldTwo(
              focusNode: focusNode,
              context: context,
              controller: controller,
              label: label,
              validator: validator,
              obscureText: obscureText,
              onFieldSubmitted: onFieldSubmitted,
              labelStyle: labelStyle,
              borderColor: borderColor,
              enabled: false,
              textStyle: textStyle,
            ),
            new Positioned.fill(
              child: new Container(
                color: Colors.cyanAccent.withOpacity(0.0),
              ),
            ),
          ],
        ),
        onSelected: (String result) {
          controller.text = result;
          if (onValueChanged != null) {
            onValueChanged(result);
          }
        },
        itemBuilder: (BuildContext context) =>
            <PopupMenuEntry<String>>[]..addAll(
                new List.generate(
                  optionsList.keys.length,
                  (int index) {
                    return new PopupMenuItem<String>(
                      value: _options[index],
                      child: new Text(_values[index]),
                    );
                  },
                ),
              ),
      ),
      new Positioned(
        top: 0.0,
        bottom: 0.0,
        left: 0.0,
        right: 0.0,
        child: new Offstage(
          offstage: optionsList.isNotEmpty,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Returns app themed Localized pop up month textfield two
Widget appThemedLocalizedMonthPopUpTextFieldTwo({
  @required String label,
  @required TextEditingController controller,
  @required BuildContext context,
  bool obscureText,
  @required FocusNode focusNode,
  TextInputType keyboardType,
  Function(String) validator,
  List<TextInputFormatter> inputFormatters,
  Function(String) onFieldSubmitted,
  bool enabled,
  TextStyle textStyle,
  TextStyle labelStyle,
  Color borderColor,
  @required Map<String, String> options,
  bool allowInput = false,
  Function onValueChanged,
}) {
  Map<String, String> optionsList = enabled ? options : {};
  List _options = optionsList.keys.toList();
  List _values = optionsList.values.toList();

  return new Stack(
    children: <Widget>[
      new PopupMenuButton<String>(
        child: new Stack(
          children: <Widget>[
            appThemedTextFieldTwo(
              focusNode: focusNode,
              context: context,
              controller: controller,
              label: label,
              validator: validator,
              obscureText: obscureText,
              onFieldSubmitted: onFieldSubmitted,
              labelStyle: labelStyle,
              borderColor: borderColor,
              enabled: false,
              textStyle: textStyle,
            ),
            new Positioned.fill(
              child: new Container(
                color: Colors.cyanAccent.withOpacity(0.0),
              ),
            ),
          ],
        ),
        onSelected: (String result) {
          // Localized month name
          controller.text = Localization?.of(context)?.trans(result) ?? "¬";
          if (onValueChanged != null) {
            onValueChanged(result);
          }
        },
        itemBuilder: (BuildContext context) =>
            <PopupMenuEntry<String>>[]..addAll(
                new List.generate(
                  optionsList.keys.length,
                  (int index) {
                    return new PopupMenuItem<String>(
                      value: _options[index],
                      child: new Text(
                        // Localized month name
                        Localization?.of(context)?.trans(_values[index]) ?? "",
                      ),
                    );
                  },
                ),
              ),
      ),
      new Positioned(
        top: 0.0,
        bottom: 0.0,
        left: 0.0,
        right: 0.0,
        child: new Offstage(
          offstage: optionsList.isNotEmpty,
          child: new Container(
            color: Colors.cyanAccent.withOpacity(0.0),
          ),
        ),
      ),
    ],
  );
}

// Shows auto complete dialog
Future<Prediction> showAutoComplete({
  @required BuildContext context,
  @required GlobalKey<ScaffoldState> parentScaffoldKey,
}) async {
  Prediction data = await PlacesAutoComplete.shared.showAutoComplete(
    context,
    parentScaffoldKey,
  );
  return data;
}
