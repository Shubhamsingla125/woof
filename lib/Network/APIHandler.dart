import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:woof_fini/Model/Network/APIError.dart';
import 'package:woof_fini/Utils/Localization.dart';
import 'package:woof_fini/Utils/LocalizationValues.dart';
import 'package:woof_fini/Utils/UniversalFunctions.dart';
import 'package:woof_fini/Utils/UniversalProperties.dart';
import 'package:woof_fini/Utils/memory_management.dart';
import 'package:meta/meta.dart';

enum MethodType { POST, GET, PUT, DELETE }

class APIHandler {
  static Map<String, String> defaultHeaders = {
    "Content-Type": "application/json",
    "device_type": isAndroid() ? "1" : "2",
    "app_version": appPackageInfo?.version ?? "1.0.0",
  };

  // POST method
  static Future<dynamic> post({
    @required dynamic requestBody,
    @required BuildContext context,
    @required String url,
    Map<String, String> additionalHeaders = const {},
  }) async {
    return await _hitApi(
      context: context,
      url: url,
      methodType: MethodType.POST,
      requestBody: requestBody,
      additionalHeaders: additionalHeaders,
    );
  }

  // GET method
  static Future<dynamic> get({
    @required String url,
    @required BuildContext context,
    Map<String, String> additionalHeaders = const {},
  }) async {
    return await _hitApi(
      context: context,
      url: url,
      methodType: MethodType.GET,
      additionalHeaders: additionalHeaders,
    );
  }

  // PUT method
  static Future<dynamic> put({
    @required dynamic requestBody,
    @required BuildContext context,
    @required String url,
    Map<String, String> additionalHeaders = const {},
  }) async {
    return await _hitApi(
      context: context,
      url: url,
      methodType: MethodType.PUT,
      requestBody: requestBody,
      additionalHeaders: additionalHeaders,
    );
  }

  // DELETE method
  static Future<dynamic> delete({
    @required BuildContext context,
    @required String url,
    Map<String, String> additionalHeaders = const {},
  }) async {
    return await _hitApi(
      context: context,
      url: url,
      methodType: MethodType.DELETE,
      additionalHeaders: additionalHeaders,

    );
  }



  // Generic HTTP method
  static Future<dynamic> _hitApi({
    @required BuildContext context,
    @required MethodType methodType,
    @required String url,
    dynamic requestBody,
    Map<String, String> additionalHeaders = const {},
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    try {
      Map<String, String> headers = {};
      headers.addAll(defaultHeaders);
      headers.addAll(additionalHeaders);
      headers["token"] = authAccessToken;
      var lang = MemoryManagement.getUserLanguage() ?? 'en';
      var deviceID = MemoryManagement.getDeviceId();
      headers["device_id"] = deviceID;
      headers["language"] = lang;

      var response;

      print("requestBody : ${requestBody} ");
      print("url: ${url} ");
      print("headers: ${headers} ");
      print("authAccessToken: ${authAccessToken} ");

      switch (methodType) {
        case MethodType.POST:
          response = await http
              .post(
                url,
                body: json.encode(requestBody),
                headers: headers,
              )
              .timeout(timeoutDuration);
          break;
        case MethodType.GET:
          response = await http
              .get(
                url,
                headers: headers,
              )
              .timeout(timeoutDuration);
          break;
        case MethodType.PUT:
          response = await http
              .put(
                url,
                body: json.encode(requestBody),
                headers: headers,
              )
              .timeout(timeoutDuration);
          break;
        case MethodType.DELETE:
          response = await http
              .delete(
                url,
                headers: headers,

              )
              .timeout(timeoutDuration);
          break;
      }

      var responseBody = jsonDecode(response.body);

      print("responseBody : ${jsonEncode(responseBody)} ");
      print("response.statusCode : ${response.statusCode} ");

//      showDialog(
//          context: context,
//          builder: (context) {
//            return new Padding(
//              padding: const EdgeInsets.all(32),
//              child: new Material(
//                child: new SingleChildScrollView(
//                    child: new Container(
//                        child: new Text(
//                  responseBody.toString(),
////                  controller:
////                      new TextEditingController(text: responseBody.toString()),
//                ))),
//              ),
//            );
//          });

      if (response.statusCode == 200 || response.statusCode == 201) {
        final Map successResponseBody = responseBody;
        completer.complete(successResponseBody);
      } else if (response.statusCode == 401) {
        APIError apiError = new APIError(
          error: responseBody["message"],
          onAlertPop: () {
            onLogoutSuccess(
              context: context,
            );
          },
        );
        completer.complete(apiError);
      } else {
        APIError apiError = new APIError(error: responseBody["error"]);
        completer.complete(apiError);
      }
    } on FormatException {
      APIError apiError = new APIError(
          error: Localization.of(context)
              .trans(LocalizationValues.badFormatError));
      completer.complete(apiError);
    } on TimeoutException {
      APIError apiError = new APIError(
          error:
              Localization.of(context).trans(LocalizationValues.timeoutError));
      completer.complete(apiError);
    } catch (e) {
      print("e: $e");
      APIError apiError = new APIError(
          error:
              Localization.of(context).trans(LocalizationValues.genericError));
      completer.complete(apiError);
    }
    return completer.future;
  }
}
