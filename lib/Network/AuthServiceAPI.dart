import 'dart:async';

import 'package:flutter/src/widgets/framework.dart';

import 'package:woof_fini/Model/Login/LoginRequest.dart';
import 'package:woof_fini/Model/PetCategory/PetCategory.dart';
import 'package:woof_fini/Model/SignUp/SignUpRequest.dart';
import 'package:woof_fini/Model/UpdateProfile/UpdateProfileRequest.dart';
import 'package:woof_fini/Model/UserDetail/UserDetailRequest.dart';

import '../AbstractApi/AuthAPI.dart';
import '../Model/Login/LoginResponse.dart';
import '../Model/Network/APIError.dart';
import '../utils/APIs.dart';
import 'APIHandler.dart';

//import 'package:flutter/material.dart';
//import 'package:woof_fini/AbstractApi/AuthAPI.dart';
//import 'package:woof_fini/Model/ForgotPassword/ForgotPasswordRequest.dart';
//import 'package:woof_fini/Model/ForgotPassword/ForgotPasswordResponse.dart';
//import 'package:woof_fini/Model/Login/LoginRequest.dart';
//import 'package:woof_fini/Model/Login/LoginResponse.dart';
//import 'package:woof_fini/Model/Network/APIError.dart';
//import 'package:woof_fini/Model/SignUp/SignUpRequest.dart';
//import 'package:woof_fini/Model/UserDetail/UserDetailRequest.dart';
//import 'package:woof_fini/Network/APIHandler.dart';
//import 'package:woof_fini/Utils/APIs.dart';
//
class AuthServiceAPI extends AuthAPI {
  @override
  Future loginUser({
    LoginRequest requestBody,
    BuildContext context,
  }) async{
    // TODO: implement loginUser
        Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.loginUrl,
    );
        print("response------->${response}");
    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {

      LoginResponse loginResponse = new LoginResponse.fromJson(response);
      completer.complete(loginResponse);
      return completer.future;
    }
  }

  @override
  Future petCategories({
    @required BuildContext context,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.get(
      url: APIs.petCategoriesUrl,
      context: context,
    );
    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      PetCategory petCategoryResponse = new PetCategory.fromJson(response);
      completer.complete(petCategoryResponse);
      return completer.future;
    }
  }

  Future<dynamic> signUpUser({
    @required BuildContext context,
    @required SignUpRequest requestBody,
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();

    var response = await APIHandler.post(
      context: context,
      requestBody: requestBody,
      url: APIs.signUpUrl,
    );

    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      LoginResponse loginResponse = new LoginResponse.fromJson(response);
      completer.complete(loginResponse);
      return completer.future;
    }
  }

  // Hits forgotPassword Otp verify api
  Future<dynamic> resetPasswordUser({
    @required BuildContext context,
    @required Map requestBody,
    @required String id
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.put(
      context: context,
      requestBody: requestBody,
      url: "${APIs.resetPasswordUrl}/$id/reset-password",
    );
    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      completer.complete(response);
      return completer.future;
    }
  }

  @override
  Future updateProfile({
    @required UpdateProfileRequest updateProfileRequest,
    @required BuildContext context,
    @required String id
  }) async {
    Completer<dynamic> completer = new Completer<dynamic>();
    var response = await APIHandler.put(
      url: "${APIs.updateProfileUrl}/$id/update",
      context: context,
      requestBody: updateProfileRequest,
    );
    if (response is APIError) {
      completer.complete(response);
      return completer.future;
    } else {
      completer.complete(response);
      return completer.future;
    }
  }

//  @override
//  // Hits Login api
//  Future<dynamic> loginUser({
//    @required BuildContext context,
//    @required LoginRequest requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.loginUrl,
//    );
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      LoginResponse loginResponse = new LoginResponse.fromJson(response);
//      completer.complete(loginResponse);
//      return completer.future;
//    }
//  }
//
//  // Hits signup user api
//  Future<dynamic> signUpUser({
//    @required BuildContext context,
//    @required SignUpRequest requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.signUpUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      LoginResponse loginResponse = new LoginResponse.fromJson(response);
//      completer.complete(loginResponse);
//      return completer.future;
//    }
//  }
//
//  // Hits signup otp verify Otp api
//  Future<dynamic> signUpOTPVerifyUser({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.verifyOTPUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      LoginResponse loginResponse = new LoginResponse.fromJson(response);
//      completer.complete(loginResponse);
//      return completer.future;
//    }
//  }
//
//  // Hits change phone email otp verify Otp api
//  Future<dynamic> changePhoneEmailOTPVerifyUser({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.verifyChangePhoneEmailOTPUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
//
//  // Hits forgotPassword api
//  Future<dynamic> forgotPasswordUser({
//    @required BuildContext context,
//    @required ForgotPasswordRequest requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.forgotPasswordUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      ForgotPasswordResponse forgotPasswordResponse =
//          new ForgotPasswordResponse.fromJson(response);
//      completer.complete(forgotPasswordResponse);
//      return completer.future;
//    }
//  }
//
//  // Hits forgotPassword Otp verify api
//  Future<dynamic> forgotOTPVerifyUser({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.forgotOTPVerifyUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      ForgotOTPVerifyResponse forgotOTPVerifyResponse =
//          new ForgotOTPVerifyResponse.fromJson(response);
//      completer.complete(forgotOTPVerifyResponse);
//      return completer.future;
//    }
//  }
//
//  // Hits forgotPassword Otp verify api
//  Future<dynamic> resetPasswordUser({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.resetPasswordUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
//
//  // Hits change Password api
//  Future<dynamic> changePasswordUser({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.changePasswordUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
//
//  // Hits change language api
//  Future<dynamic> changeLanguage({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.changeLanguageUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
//
//  // Hits push notification toggle api
//  Future<dynamic> pushNotificationToggle({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.pushNotificationToggleUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
//
//  // Hits change phone-email api
//  Future<dynamic> changePhoneEmail({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.changePhoneEmailUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
//
//  // Hits resend Otp api
//  Future<dynamic> resendOTPUser({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.resendOTPUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      ResendOTPResponse resendOtpResponse =
//          new ResendOTPResponse.fromJson(response);
//      completer.complete(resendOtpResponse);
//      return completer.future;
//    }
//  }
//
//  // Hits upload Company logo api
//  Future<dynamic> companyLogoUser({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.profilePicUrl,
//    );
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits skip profile
//  Future<dynamic> skipUserProfile({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.skipProfileURL,
//    );
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  @override
//  Future isUserApproved({
//    @required BuildContext context,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//    var response = await APIHandler.get(
//      url: APIs.isUserApproved,
//      context: context,
//    );
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      LoginResponse loginResponse = new LoginResponse.fromJson(response);
//      completer.complete(loginResponse);
//      return completer.future;
//    }
//  }
//
//  @override
//  Future getProfile({
//    @required BuildContext context,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      url: APIs.getProfileUrl,
//      context: context,
//    );
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      LoginResponse loginResponse = new LoginResponse.fromJson(response);
//      completer.complete(loginResponse);
//      return completer.future;
//    }
//  }
//
//  @override
//  Future getCategories({
//    @required BuildContext context,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      url: APIs.getCategoryUrl,
//      context: context,
//    );
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      Category category = new Category.fromJson(response);
//      completer.complete(category);
//      return completer.future;
//    }
//  }
//
//  @override
//  Future updateProfile({
//    @required UserDetailRequest userDetailRequest,
//    @required BuildContext context,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.put(
//      url: APIs.updateProfileUrl,
//      context: context,
//      requestBody: userDetailRequest,
//    );
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
//
//  // Returns warehouses
//  @override
//  Future getWarehouses({
//    @required BuildContext context,
//    @required int pageNumber,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      url: APIs.getWarehousesUrl + "?page=$pageNumber&all=0",
//      context: context,
//    );
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      AllWarehouseListResponse allWarehouseListResponse =
//          new AllWarehouseListResponse.fromJson(response);
//      completer.complete(allWarehouseListResponse);
//      return completer.future;
//    }
//  }
//
//  // Hits add warehouse api
//  Future<dynamic> addWarehouse({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.addWarehouseUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
//
//  // Hits update warehouse api
//  Future<dynamic> updateWarehouse({
//    @required BuildContext context,
//    @required Map requestBody,
//    @required String warehouseId,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.updateWarehouseUrl + "/$warehouseId",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
}
