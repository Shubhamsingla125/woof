import 'dart:async';
import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:woof_fini/utils/APIs.dart';
import 'package:path/path.dart';

class UniversalAPI {

  static Future<http.StreamedResponse> uploadPost(String title,String id,_image) async {
    print("UPLOADING IMAGE");
    var url = Uri.parse('${APIs.createPostUrl}'); //get url
//    String token = await SharedPref.getToken;

    Map<String, String> headers = {
//header
      "Content-Type": "multipart/form-data",
//      "Token": token ?? "",
      "Accept": "application/json"
    };

    http.MultipartRequest request =
    new http.MultipartRequest("POST", url); //changed

    request.headers.addAll(headers);
    request.fields["title"] = title;
    request.fields["user_id"] = id;

    final fileName = basename(_image.path);
    var bytes = await _image.readAsBytes();

    request.files.add(new http.MultipartFile.fromBytes(
      "image",
      bytes,
      filename: fileName,
    ));
    print("UPLOADING");
    http.StreamedResponse response = await request.send();
    print("UPLOADED");
    print("STATUS CODE: ${response.statusCode}");
    return response;
  }
//  // Gets regions, varieties, processes and certifications
//  static Future getCategories({
//    @required BuildContext context,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      url: APIs.getCategoryUrl,
//      context: context,
//    );
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      Category category = new Category.fromJson(response);
//      completer.complete(category);
//      return completer.future;
//    }
//  }
//
//  // Hits add assets
//  static Future<dynamic> addAsset({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.addAssetsUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits add assets
//  static Future<dynamic> addToMyAsset({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.addGlobalAssetsUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits all assets API
//  static Future<dynamic> getAllAssets({
//    @required BuildContext context,
//    @required int assetTypeCode,
//    @required int pageNumber,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.allAssetsUrl + "?vendor_type=$assetTypeCode&page=$pageNumber",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits notifications list API
//  static Future<dynamic> getNotifications({
//    @required BuildContext context,
//    @required int pageNumber,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.notificationsListUrl + "?page=$pageNumber",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits notification delete API
//  static Future<dynamic> deleteNotification({
//    @required BuildContext context,
//    @required String notificationId,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.delete(
//      context: context,
//      url: APIs.deleteNotificationUrl + "/$notificationId",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits notifications count API
//  static Future<dynamic> getNotificationsCount({
//    @required BuildContext context,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.notificationsCountUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits mark notification as read API
//  static Future<dynamic> markNotificationAsRead({
//    @required BuildContext context,
//    @required String notificationId,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.readNotificationUrl + "/$notificationId",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits search assets API
//  static Future<dynamic> getSearchedAssets({
//    @required BuildContext context,
//    @required int assetTypeCode,
//    @required String keyword,
//    @required int pageNumber,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.searchAssetsUrl +
//          "?page=$pageNumber&vendor_type=$assetTypeCode&keyword=$keyword",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits contact us
//  static Future<dynamic> contactUsAdmin({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.contactUsUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits signup otp verify Otp api
//  static Future<dynamic> updateDeviceToken({
//    @required BuildContext context,
//    @required Map request,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.put(
//      context: context,
//      requestBody: request,
//      url: APIs.updateDeviceTokenUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//      return completer.future;
//    } else {
//      completer.complete(response);
//      return completer.future;
//    }
//  }
//
//  // Hits logout API
//  static void logout({
//    @required BuildContext context,
//    @required bool mounted,
//    @required Map requestBody,
//    @required Function onSuccess,
//  }) async {
//    // Showing loader
//    OverlayState overlayState;
//    overlayState = Overlay.of(context);
//    OverlayEntry loader = new OverlayEntry(builder: (BuildContext context) {
//      return new Positioned.fill(
//        child: new Container(
//          color: Colors.black.withOpacity(0.5),
//          alignment: Alignment.center,
//          child: getAppThemedLoader(context: context),
//        ),
//      );
//    });
//    overlayState.insert(loader);
//
//    bool gotInternetConnection = await hasInternetConnection(
//      context: context,
//      mounted: mounted,
//      canShowAlert: true,
//      onFail: () {
//        loader.remove(); //hide loader
//      },
//      onSuccess: () {},
//    );
//
//    if (gotInternetConnection) {
//      var response = await APIHandler.put(
//        context: context,
//        url: APIs.logoutUrl,
//        requestBody: requestBody,
//      );
//
//      loader.remove(); //hide loader
//      //logged in successfully
//      if (response != null && !(response is APIError)) {
//        if (onSuccess != null) {
//          onSuccess();
//        }
//      } else {
//        APIError apiError = response;
//        showAlert(
//          context: context,
//          titleText: Localization.of(context).trans(LocalizationValues.error),
//          message: apiError.error,
//          actionCallbacks: {
//            Localization.of(context).trans(LocalizationValues.ok): () {
//              if (apiError.onAlertPop != null) {
//                apiError.onAlertPop();
//              }
//            }
//          },
//        );
//      }
//    }
//  }
//
//  // Consumer
//  // Hits scan history list API
//  static Future<dynamic> getScanHistory({
//    @required BuildContext context,
//    @required int pageNumber,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.scanHistoryUrl + "?page=$pageNumber",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits add to scan history API
//  static Future<dynamic> addToScanHistory({
//    @required BuildContext context,
//    @required dynamic requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      url: APIs.addToScanHistoryUrl,
//      requestBody: requestBody,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits checks username availability API
//  static Future<dynamic> checkUsername({
//    @required BuildContext context,
//    @required dynamic requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      url: APIs.checkUsernameUrl,
//      requestBody: requestBody,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits add loading port
//  static Future<dynamic> addToMyLoadingPorts({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.addToMyLoadingPortUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits my loading ports API
//  static Future<dynamic> getMyLoadingPorts({
//    @required BuildContext context,
//    @required int pageNumber,
//    @required int assetTypeCode,
//    String keyword = "",
//    String showOption = "0",
//    String noPagination = "0",
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.myLoadingPortsUrl +
//          "?vendor_type=$assetTypeCode"
//              "&page=$pageNumber"
//              "&keyword=$keyword"
//              "&show_option=$showOption"
//              "&all=$noPagination",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits search loading ports API
//  static Future<dynamic> getSearchedLoadingPorts({
//    @required BuildContext context,
//    @required String keyword,
//    @required int pageNumber,
//    @required int assetTypeCode,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.searchLoadingPortsUrl +
//          "?vendor_type=$assetTypeCode&keyword=$keyword&page=$pageNumber",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits add destination port
//  static Future<dynamic> addToMyDestinationPorts({
//    @required BuildContext context,
//    @required Map requestBody,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.post(
//      context: context,
//      requestBody: requestBody,
//      url: APIs.addToMyDestinationPortUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits my destination ports API
//  static Future<dynamic> getMyDestinationPorts({
//    @required BuildContext context,
//    @required int pageNumber,
//    @required int assetTypeCode,
//    String keyword = "",
//    String showOption = "0",
//    String noPagination = "0",
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.myDestinationPortsUrl +
//          "?vendor_type=$assetTypeCode"
//              "&page=$pageNumber"
//              "&keyword=$keyword"
//              "&show_option=$showOption"
//              "&all=$noPagination",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits search Destination ports API
//  static Future<dynamic> getSearchedDestinationPorts({
//    @required BuildContext context,
//    @required String keyword,
//    @required int pageNumber,
//    @required int assetTypeCode,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.searchDestinationPortsUrl +
//          "?vendor_type=$assetTypeCode&keyword=$keyword&page=$pageNumber",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits my warehouses API
//  static Future<dynamic> getMyWarehouses({
//    @required BuildContext context,
//    @required int pageNumber,
//    String keyword = "",
//    String noPagination = "1",
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.getWarehousesUrl +
//          "?page=$pageNumber"
//              "&keyword=$keyword"
//              "&all=$noPagination",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits blog notifications list API
//  static Future<dynamic> getBlogNotifications({
//    @required BuildContext context,
//    @required int pageNumber,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.blogNotificationsListUrl + "?page=$pageNumber",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits blog notification delete API
//  static Future<dynamic> deleteBlogNotification({
//    @required BuildContext context,
//    @required String notificationId,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.delete(
//      context: context,
//      url: APIs.blogDeleteNotificationUrl + "/$notificationId",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits blog notifications count API
//  static Future<dynamic> getBlogNotificationsCount({
//    @required BuildContext context,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.blogNotificationsCountUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits mark blog notification as read API
//  static Future<dynamic> markBlogNotificationAsRead({
//    @required BuildContext context,
//    @required String notificationId,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.blogReadNotificationUrl + "/$notificationId",
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//
//  // Hits mark blog notifications as read API
//  static Future<dynamic> markBlogNotificationsAsRead({
//    @required BuildContext context,
//  }) async {
//    Completer<dynamic> completer = new Completer<dynamic>();
//
//    var response = await APIHandler.get(
//      context: context,
//      url: APIs.blogReadNotificationUrl,
//    );
//
//    if (response is APIError) {
//      completer.complete(response);
//    } else {
//      completer.complete(response);
//    }
//    return completer.future;
//  }
//}
//
//// Universal API methods
//
//// Gets notifications badge count
//void getNotificationBadgeCount({
//  @required BuildContext context,
//  @required VoidCallback updateScreen,
//  bool mounted,
//}) async {
//  bool gotInternetConnection = await hasInternetConnection(
//    context: context,
//    mounted: mounted,
//    canShowAlert: false,
//    onFail: () {},
//    onSuccess: () {},
//  );
//
//  if (gotInternetConnection) {
//    var response = await UniversalAPI.getNotificationsCount(
//      context: context,
//    );
//    //logged in successfully
//    if (response != null && !(response is APIError)) {
//      unreadNotificationsCount = response["count"];
//      addBadge(unreadNotificationsCount);
//      if (updateScreen != null) {
//        updateScreen();
//      }
//
//      getBlogNotificationBadgeCount(
//        context: context,
//        mounted: mounted,
//        updateScreen: () {
//          if (updateScreen != null) {
//            updateScreen();
//          }
//        },
//      );
//    } else {
//      APIError apiError = response;
//    }
//  }
//}
//
//// Gets blog notifications badge count
//void getBlogNotificationBadgeCount({
//  @required BuildContext context,
//  @required VoidCallback updateScreen,
//  bool mounted,
//}) async {
//  bool gotInternetConnection = await hasInternetConnection(
//    context: context,
//    mounted: mounted,
//    canShowAlert: false,
//    onFail: () {},
//    onSuccess: () {},
//  );
//
//  if (gotInternetConnection) {
//    var response = await UniversalAPI.getBlogNotificationsCount(
//      context: context,
//    );
//    //logged in successfully
//    if (response != null && !(response is APIError)) {
//      unreadBlogNotificationsCount = response["count"];
//      addBadge(unreadBlogNotificationsCount);
//      if (updateScreen != null) {
//        updateScreen();
//      }
//    } else {
//      APIError apiError = response;
//    }
//  }
}
