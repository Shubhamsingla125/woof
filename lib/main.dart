import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:woof_fini/screens/Splash/splash_screen.dart';
import 'package:woof_fini/utils/app_localizations.dart';
import 'package:woof_fini/utils/preference_helper.dart';
import 'package:woof_fini/utils/provider_setup.dart';
import 'package:woof_fini/utils/theme_state.dart';

import 'utils/Localization.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized(); // After upgrading flutter need to call this before any code...
  SharedPreferences.getInstance().then((value) {
    PreferenceHelper.init(value);
    runApp(MyApp());
  });
}
/// Only has MyInheritedState as field.

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {



  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: Consumer<ThemeState>(
        builder: (context, model, _) => MultiBlocProvider(
          providers: [
         /*   BlocProvider<AuthenticationBloc>(builder: (_) {
              return AuthenticationBloc(
                  userRepository: UserRepository.global());
            }),*/
          ],
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'WOOF FINI',
            theme: ThemeData(),

            supportedLocales: [const Locale('en')],
            // These delegates make sure that the localization data for the proper language is loaded
            localizationsDelegates: [
              // A class which loads the translations from JSON files
              const AppLocalizationsDelegate(),
              // Built-in localization of basic text for Material widgets
              GlobalMaterialLocalizations.delegate,
              // Built-in localization for text direction LTR/RTL
              GlobalWidgetsLocalizations.delegate,
            ],
            home: new SplashScreen(),
            builder: (context, child) {
              return MediaQuery(
                child: child,
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              );
            },

          ),
        ),
      ),
    );
  }
}
