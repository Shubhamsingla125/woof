import 'dart:async';
import 'package:flutter/material.dart';
import 'package:woof_fini/Model/ForgotPassword/ForgotPasswordRequest.dart';
import 'package:woof_fini/Model/Login/LoginRequest.dart';
import 'package:woof_fini/Model/SignUp/SignUpRequest.dart';
import 'package:woof_fini/Model/UpdateProfile/UpdateProfileRequest.dart';
import 'package:woof_fini/Model/UserDetail/UserDetailRequest.dart';

abstract class AuthAPI {
  Future<dynamic> loginUser({
    @required LoginRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> petCategories({
    @required BuildContext context,
  });

  Future<dynamic> signUpUser({
    @required SignUpRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> resetPasswordUser({
    @required String id,
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> updateProfile({
    @required UpdateProfileRequest updateProfileRequest,
    @required BuildContext context,
    @required String id
  });

  /*Future<dynamic> signUpUser({
    @required SignUpRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> forgotPasswordUser({
    @required ForgotPasswordRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> forgotOTPVerifyUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> resendOTPUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> resetPasswordUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changePasswordUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changeLanguage({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> pushNotificationToggle({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changePhoneEmail({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> signUpOTPVerifyUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changePhoneEmailOTPVerifyUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> companyLogoUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> skipUserProfile({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> isUserApproved({
    @required BuildContext context,
  });

  Future<dynamic> getProfile({
    @required BuildContext context,
  });

  Future<dynamic> getCategories({
    @required BuildContext context,
  });

  Future<dynamic> updateProfile({
    @required UserDetailRequest userDetailRequest,
    @required BuildContext context,
  });

  Future<dynamic> getWarehouses({
    @required BuildContext context,
    @required int pageNumber,
  });

  Future<dynamic> addWarehouse({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> updateWarehouse({
    @required Map requestBody,
    @required BuildContext context,
    @required String warehouseId,

  });
*/
}
