import 'dart:async';
import 'package:flutter/material.dart';
import 'package:woof_fini/Model/ForgotPassword/ForgotPasswordRequest.dart';
import 'package:woof_fini/Model/Login/LoginRequest.dart';
import 'package:woof_fini/Model/SignUp/SignUpRequest.dart';
import 'package:woof_fini/Model/UserDetail/UserDetailRequest.dart';

abstract class PostAPI {
  Future<dynamic> postList({
    @required BuildContext context,
  });


  /*Future<dynamic> signUpUser({
    @required SignUpRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> forgotPasswordUser({
    @required ForgotPasswordRequest requestBody,
    @required BuildContext context,
  });

  Future<dynamic> forgotOTPVerifyUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> resendOTPUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> resetPasswordUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changePasswordUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changeLanguage({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> pushNotificationToggle({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changePhoneEmail({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> signUpOTPVerifyUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> changePhoneEmailOTPVerifyUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> companyLogoUser({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> skipUserProfile({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> isUserApproved({
    @required BuildContext context,
  });

  Future<dynamic> getProfile({
    @required BuildContext context,
  });

  Future<dynamic> getCategories({
    @required BuildContext context,
  });

  Future<dynamic> updateProfile({
    @required UserDetailRequest userDetailRequest,
    @required BuildContext context,
  });

  Future<dynamic> getWarehouses({
    @required BuildContext context,
    @required int pageNumber,
  });

  Future<dynamic> addWarehouse({
    @required Map requestBody,
    @required BuildContext context,
  });

  Future<dynamic> updateWarehouse({
    @required Map requestBody,
    @required BuildContext context,
    @required String warehouseId,

  });
*/
}
