import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:core';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:woof_fini/BLocs/AuthBLoc.dart';
import 'package:woof_fini/Model/Login/LoginRequest.dart';
import 'package:woof_fini/Model/Login/LoginResponse.dart';
import 'package:woof_fini/Model/Network/APIError.dart';
import 'package:woof_fini/Model/PetCategory/PetCategory.dart';
import 'package:woof_fini/Model/SignUp/SignUpRequest.dart';
import 'package:woof_fini/Providers/AuthProvider.dart';
import 'package:woof_fini/utils/ReusableWidgets.dart';
import 'package:woof_fini/utils/memory_management.dart';

import '../../utils/UniversalFunctions.dart';
import '../../utils/ValidatorFunctions.dart';
import '../../utils/ValidatorFunctions.dart';
import '../../utils/ValidatorFunctions.dart';
import '../../utils/ValidatorFunctions.dart';
import '../onboarding_1_screen.dart';

class SignUpTwoScreen extends StatefulWidget {
  final SignUpRequest request;

  const SignUpTwoScreen({Key key, this.request}) : super(key: key);
  @override
  _SignUpTwoScreenState createState() => _SignUpTwoScreenState();
}

class _SignUpTwoScreenState extends State<SignUpTwoScreen> {
  TextEditingController birthdayController = new TextEditingController();
  TextEditingController breedController = new TextEditingController();

  int genderSelector = 1;
  double slider = 4.5;

//  List<SignUpChip> chips = [];
  int selected = 0;
  DateTime selectedDate = DateTime.now().subtract(Duration(days: 365 * 12));
  // Global keys
  final _signupFormKey = GlobalKey<FormState>();

  final StreamController<bool> _loaderStreamController =
      new StreamController<bool>();

  final StreamController<PetCategory> _petCategoryStreamController =
      new StreamController<PetCategory>();
  PetCategory _petCategory;
  // Data props.
  AuthBLoc authBLoc;

  @override
  void initState() {
    // TODO: implement initState

//    chips.add(SignUpChip('assets/dog.png', 'Dogs'));
//    chips.add(SignUpChip('assets/set_cat.png', 'Cats'));
//    chips.add(SignUpChip('assets/set_bird.png', 'Birds'));
//    chips.add(SignUpChip('assets/set_rabbit.png', 'Bunnies'));
//    chips.add(SignUpChip('assets/set_fish.png', 'Others'));
    getPetCategories();
    super.initState();
//    '09 May 2017'
  }

  Future<Null> _selectDate(BuildContext context) async {
    var firstDate = DateTime.now().subtract(Duration(days: 365 * 100));
    var lastDate = DateTime.now().subtract(Duration(days: 365 * 12));
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: firstDate,
        lastDate: lastDate);
    if (picked != null && picked != selectedDate)
      setState(() {
        birthdayController.text = getFormattedDateString(dateTime: picked);
        selectedDate = picked;
      });
  }

  @override
  Widget build(BuildContext context) {
    authBLoc = AuthProvider.of(context);
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Opacity(
              opacity: 0.15,
              child: Image.asset(
                'assets/splash_bg.png',
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
                //color: Color(0xff707070).withOpacity(0.1),
              ),
            ),
          ),
          Form(
            key: _signupFormKey,
            child: ListView(
              children: <Widget>[
                Container(
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 15),
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: InkWell(
                    onTap: () {
                      _selectDate(context);
                    },
                    child: TextFormField(
                      validator: (value) {
                        return dobValidator(dob: value, context: context);
                      },
                      enabled: false,
                      controller: birthdayController,
                      textAlign: TextAlign.start,
                      style: TextStyle(color: Colors.black, fontSize: 18),
                      decoration: InputDecoration(
                        labelText: 'Birthday',
                        errorStyle: TextStyle(
                          color: Colors.red,
                          // or any other color
                        ),
                        labelStyle:
                            TextStyle(color: Color(0xff74d7df), fontSize: 15),
                        disabledBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xff95989a), width: 1),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xff95989a), width: 1),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide:
                              BorderSide(color: Color(0xff95989a), width: 1),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 40),
                Container(
                  height: 85,
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(left: 15),
                  child: _allChips(),
                ),
                SizedBox(height: 15),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: TextFormField(
                    validator: (value) {
                      return breedValidator(breed: value, context: context);
                    },
                    controller: breedController,
                    textAlign: TextAlign.start,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                    decoration: InputDecoration(
                      labelText: 'Breed',
                      labelStyle:
                          TextStyle(color: Color(0xff74d7df), fontSize: 15),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Radio(
                            groupValue: genderSelector,
                            value: 1,
                            onChanged: (val) {
                              setState(() {
                                genderSelector = val;
                              });
                            },
                            activeColor: Color(0xff74d7df),
                          ),
                          Text(
                            'Male',
                            style: TextStyle(
                              color: Color(0xff808285),
                              fontSize: 18,
                            ),
                          ),
                          SizedBox(width: 10),
                          Image.asset(
                            'assets/signup_arrow1.png',
                            height: 15,
                            width: 15,
                            fit: BoxFit.cover,
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Radio(
                            groupValue: genderSelector,
                            value: 2,
                            onChanged: (val) {
                              setState(() {
                                genderSelector = val;
                              });
                            },
                            activeColor: Color(0xff74d7df),
                          ),
                          Text(
                            'Female',
                            style: TextStyle(
                              color: Color(0xff808285),
                              fontSize: 18,
                            ),
                          ),
                          SizedBox(width: 10),
                          Image.asset(
                            'assets/signup_arrow1.png',
                            height: 15,
                            width: 15,
                            fit: BoxFit.cover,
                          ),
                          SizedBox(width: 10),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 30),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: Text(
                    'Weight',
                    style: TextStyle(
                      color: Color(0xff74d7df),
                      fontSize: 16,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50, vertical: 8),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        '3.6',
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                      Text(
                        slider.toStringAsFixed(1),
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 25),
                  child: Slider(
                    value: slider,
                    min: 3.5,
                    max: 7,
                    onChanged: (val) {
                      setState(() {
                        slider = val;
                      });
                    },
                    activeColor: Color(0xff74d7df),
                    inactiveColor: Color(0xff707070),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          if (_signupFormKey.currentState.validate()) {
                            signUpUser();
                          }
                        },
                        child: Text(
                          'Continue',
                          style: TextStyle(
                            fontSize: 23,
                            color: Color(0xff74d7df),
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          if (_signupFormKey.currentState.validate()) {
                            signUpUser();
                          }
//                          Navigator.of(context).push(MaterialPageRoute(
//                              builder: (context) => OnboardingOneScreen()));
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(40)),
                            color: Color(0xff74d7df),
                          ),
                          padding: EdgeInsets.all(15),
                          child: Image.asset(
                            'assets/paw.png',
                            color: Colors.white,
                            height: 40,
                            width: 40,
                            fit: BoxFit.cover,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 40),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 40),
                  child: Column(
                    children: <Widget>[
                      Text(
                        'By tapping Sign up, you agree to our',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xff808285).withOpacity(0.8),
                          fontSize: 16,
                        ),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          'Term of Service & Privacy Policy',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0xff58595b),
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 40),
              ],
            ),
          ),
          new Center(
            child: _getLoader,
          ),
        ],
      ),
    );
  }

  get _getLoader {
    return getFullScreenLoader(
      stream: _loaderStreamController.stream,
      context: context,
    );
  }

  _allChips() {
    return StreamBuilder<PetCategory>(
        stream: _petCategoryStreamController.stream,
        builder: (context, snapshot) {
          var data = snapshot.data;
          return (data != null && data.data.pets.length > 0)
              ? ListView.builder(
                  shrinkWrap: true,
                  itemCount: data?.data?.pets?.length ?? 0,
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.zero,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Column(
                        children: <Widget>[
                          ActionChip(
                            onPressed: () {
                              setState(() {
                                selected = index;
                              });
                            },
                            padding: EdgeInsets.zero,
                            labelPadding: EdgeInsets.zero,
                            label: Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(15)),
                                  color: index == selected
                                      ? Color(0xff74d7df)
                                      : Colors.white,
                                ),
                                padding: EdgeInsets.all(5),
                                height: 50,
                                width: 50,
                                child: getCachedNetworkImage(
                                    url: data.data?.pets[index]?.image ?? "")
//                      Image.asset(
//                        data.data?.pets[index]?.image ?? "assets/dog.png",
//                        color:
//                        index == selected ? Colors.black : Color(0xff707070).withOpacity(0.3),
//                      ),
                                ),
                          ),
                          SizedBox(height: 5),
                          AutoSizeText(
                            data.data?.pets[index]?.name ?? "",
                            style: TextStyle(
                              color: Color(0xff707070),
                              fontSize: 16,
                            ),
                            maxLines: 1,
                            minFontSize: 8,
                          ),
                        ],
                      ),
                    );
                  },
                )
              : new Container();
        });
  }

  // get pet categories user in
  void getPetCategories() async {
    _loaderStreamController.add(true); //show loader

    bool gotInternetConnection = await hasInternetConnection(
      context: context,
      mounted: mounted,
      canShowAlert: true,
      onFail: () {
        _loaderStreamController.add(false); //hide loader
      },
      onSuccess: () {},
    );

    if (gotInternetConnection) {
      var response = await authBLoc.petCategories(
        context: context,
      );
      _loaderStreamController.add(false); //hide loader
      //logged in successfully
      if (response != null && response is PetCategory) {
        _petCategoryStreamController.add(response);
        _petCategory = response;
      } else {
        APIError apiError = response;
        showAlert(
          context: context,
          titleText: "Error",
          message: apiError.error,
          actionCallbacks: {"OK": () {}},
        );
      }
    }
  }

  void signUpUser() async {
    _loaderStreamController.add(true); //show loader

    bool gotInternetConnection = await hasInternetConnection(
      context: context,
      mounted: mounted,
      canShowAlert: true,
      onFail: () {
        _loaderStreamController.add(false); //hide loader
      },
      onSuccess: () {},
    );

    if (gotInternetConnection) {
      var request = SignUpRequest(
        phoneNumber: widget.request.phoneNumber,
        name: widget.request.name,
        email: widget.request.email,
        breedName: breedController.text,
        dob: getFormattedDateString(
            dateTime: selectedDate, format: "yyyy-MM-dd"),
        gender: genderSelector == 1 ? "male" : "female",
        weight: slider.toString(),
        categoryId: _petCategory?.data?.pets[selected]?.sId ?? "",
        password: widget.request.password,
        countryCode: widget.request.countryCode,
      );
      var response = await authBLoc.signUpUser(
        request: request,
        context: context,
      );
      _loaderStreamController.add(false); //hide loader
      //logged in successfully
      if (response != null && response is LoginResponse) {
        MemoryManagement.setUserLoggedIn(isUserLoggedin: true);
        MemoryManagement.setUserInfo(userInfo: json.encode(response));
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => OnboardingOneScreen()),
            (route) => false);
      } else {
        APIError apiError = response;
        showAlert(
          context: context,
          titleText: "Error",
          message: apiError.error,
          actionCallbacks: {"OK": () {}},
        );
      }
    }
  }
}

class SignUpChip {
  String image;
  String name;

  SignUpChip(this.image, this.name);
}
