import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:woof_fini/BLocs/AuthBLoc.dart';
import 'package:woof_fini/Model/SignUp/SignUpRequest.dart';
import 'package:woof_fini/Network/AuthServiceAPI.dart';
import 'package:woof_fini/Providers/AuthProvider.dart';
import 'package:woof_fini/custom/country.dart';
import 'package:woof_fini/custom/country_picker.dart';
import 'package:woof_fini/screens/Signp/signup_two_screen.dart';

import '../../utils/ValidatorFunctions.dart';


class SignUpOneScreen extends StatefulWidget {
  @override
  _SignUpOneScreenState createState() => _SignUpOneScreenState();
}

class _SignUpOneScreenState extends State<SignUpOneScreen> {
  TextEditingController nameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController mobileController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();

  Country _selected;
  // Global keys
  final _signUpFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
//    nameController.text = 'Bold Pilot';
//    emailController.text = 'rocky.morissette@stephanie.biz';
//    mobileController.text = '9167890655';
//    passwordController.text = '12345678';
//    confirmPasswordController.text = '12345678';
    _selected = Country(
      asset: "assets/flags/us_flag.png",
      dialingCode: "1",
      isoCode: "US",
      name: "United States",
    );
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Opacity(
              opacity: 0.15,
              child: Image.asset(
                'assets/splash_bg.png',
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                fit: BoxFit.cover,
                //color: Color(0xff707070).withOpacity(0.1),
              ),
            ),
          ),
          Form(
            key: _signUpFormKey,
            child: ListView(
              children: <Widget>[
                Container(
                  margin:
                      EdgeInsets.only(left: 20, right: 20, top: 30, bottom: 15),
                  child: Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 40),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Tell us about your pet',
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
                SizedBox(height: 40),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: TextFormField(
                    validator: (value) {
                      return nameValidator(name: value, context: context);
                    },
                    controller: nameController,
                    textAlign: TextAlign.start,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                    decoration: InputDecoration(
                      labelText: 'Name',
                      labelStyle:
                          TextStyle(color: Color(0xff74d7df), fontSize: 15),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: TextFormField(
                    validator: (value) {
                      return emailValidator(email: value, context: context);
                    },
                    controller: emailController,
                    textAlign: TextAlign.start,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                    decoration: InputDecoration(
                      labelText: 'Email Address',
                      labelStyle:
                          TextStyle(color: Color(0xff74d7df), fontSize: 15),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: TextFormField(
                    validator: (value) {
                      return phoneNumberValidator(
                          phoneNumber: value, context: context);
                    },
                    controller: mobileController,
                    textAlign: TextAlign.start,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                    decoration: InputDecoration(
                      labelText: 'Phone Number',
                      labelStyle:
                          TextStyle(color: Color(0xff74d7df), fontSize: 15),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                      prefix: CountryPicker(
                        dense: false,
                        showFlag: false, //displays flag, true by default
                        showDialingCode:
                            true, //displays dialing code, false by default
                        showName:
                            false, //displays country name, true by default
                        onChanged: (Country country) {
                          setState(() {
                            _selected = country;
                          });
                        },
                        selectedCountry: _selected,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: TextFormField(
                    validator: (value) {
                      return passwordValidator(
                          newPassword: value, context: context);
                    },
                    controller: passwordController,
                    textAlign: TextAlign.start,
                    obscureText: true,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                    decoration: InputDecoration(
                      labelText: 'Password',
                      labelStyle:
                          TextStyle(color: Color(0xff74d7df), fontSize: 15),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: TextFormField(
                    validator: (value) {
                      return confirmPasswordValidator(
                          context: context,
                          confirmPassword: value,
                          password: passwordController.text);
                    },
                    controller: confirmPasswordController,
                    textAlign: TextAlign.start,
                    obscureText: true,
                    style: TextStyle(color: Colors.black, fontSize: 18),
                    decoration: InputDecoration(
                      labelText: 'Confirm password',
                      labelStyle:
                          TextStyle(color: Color(0xff74d7df), fontSize: 15),
                      enabledBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide:
                            BorderSide(color: Color(0xff95989a), width: 1),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 80),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 50),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          if (_signUpFormKey.currentState.validate()) {
                            navigateToSignUpTwo();
                          }
                        },
                        child: Text(
                          'Continue',
                          style: TextStyle(
                            fontSize: 23,
                            color: Color(0xff74d7df),
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          if (_signUpFormKey.currentState.validate()) {
                            navigateToSignUpTwo();
                          }
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(40)),
                            color: Color(0xff74d7df),
                          ),
                          padding: EdgeInsets.all(15),
                          child: Image.asset(
                            'assets/paw.png',
                            color: Colors.white,
                            height: 40,
                            width: 40,
                            fit: BoxFit.cover,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(height: 40),
              ],
            ),
          ),
        ],
      ),
    );
  }

  SignUpRequest setUpSignUpDataInModel() {
    SignUpRequest request = SignUpRequest(
      name: nameController.text,
      email: emailController.text,
      password: passwordController.text,
      phoneNumber: mobileController.text,
      countryCode: "+"+_selected.dialingCode
    );
    return request;
  }

  navigateToSignUpTwo() {
    Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => new AuthProvider(
          child: new SignUpTwoScreen(
            request: setUpSignUpDataInModel()
          ),
          authBloc: new AuthBLoc(
            authAPI: AuthServiceAPI(),
          ),
        ),
      ),
    );
  }
}
