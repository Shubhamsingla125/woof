import 'dart:async';

import 'package:flutter/material.dart';
import 'package:woof_fini/BLocs/AuthBLoc.dart';
import 'package:woof_fini/Model/Network/APIError.dart';
import 'package:woof_fini/Providers/AuthProvider.dart';
import 'package:woof_fini/utils/ReusableWidgets.dart';
import 'package:woof_fini/utils/UniversalFunctions.dart';
import '../../utils/ValidatorFunctions.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  // Global keys
  final _resetPasswordFormKey = GlobalKey<FormState>();

  // Data props.
  AuthBLoc authBLoc;

  final StreamController<bool> _loaderStreamController =
      new StreamController<bool>();

  TextEditingController currentPasswordController = new TextEditingController();
  TextEditingController newPasswordController = new TextEditingController();
  TextEditingController confirmPasswordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    authBLoc = AuthProvider.of(context);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Change Password'),
        backgroundColor: Colors.black,
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(Icons.arrow_back_ios),
        ),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            color: Colors.black,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: <Widget>[
                Center(
                  child: InkWell(
                    onTap: (){
                      if (_resetPasswordFormKey.currentState.validate()){
                        resetPassword();
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(40)),
                        color: Color(0xff74d7df),
                      ),
                      padding: EdgeInsets.all(15),
                      child: Image.asset(
                        'assets/paw.png',
                        color: Colors.white,
                        height: 40,
                        width: 40,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                new Form(
                  key: _resetPasswordFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 20),
                      Container(
                        height: 50,
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            color: Color(0xff393939),
                            borderRadius:
                                BorderRadius.all(Radius.circular(50))),
                        child: TextFormField(
                          validator: (value) {
                            return currentPasswordValidator(
                                currentPassword: value, context: context);
                          },
                          controller: currentPasswordController,
                          style: TextStyle(color: Color(0xff707070)),
                          decoration: InputDecoration(
                            hintText: 'Current Password',
                            hintStyle: TextStyle(color: Color(0xff707070)),
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 30, vertical: 15),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        height: 50,
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            color: Color(0xff393939),
                            borderRadius:
                                BorderRadius.all(Radius.circular(50))),
                        child: TextFormField(
                          controller: newPasswordController,
                          validator: (value) {
                            return newPasswordValidator(
                              newPassword: value,
                              context: context,
                            );
                          },
                          style: TextStyle(color: Color(0xff707070)),
                          decoration: InputDecoration(
                            hintText: 'New Password',
                            hintStyle: TextStyle(color: Color(0xff707070)),
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 30, vertical: 15),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      Container(
                        height: 50,
                        margin: EdgeInsets.symmetric(horizontal: 30),
                        decoration: BoxDecoration(
                            color: Color(0xff393939),
                            borderRadius:
                                BorderRadius.all(Radius.circular(50))),
                        child: TextFormField(
                          controller: confirmPasswordController,
                          validator: (value) {
                            return confirmPasswordValidator(
                              confirmPassword: value,
                              password: newPasswordController.text,
                              context: context,
                            );
                          },
                          style: TextStyle(color: Color(0xff707070)),
                          decoration: InputDecoration(
                            hintText: 'Confirm Password',
                            hintStyle: TextStyle(color: Color(0xff707070)),
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 30, vertical: 15),
                          ),
                        ),
                      ),



                    ],
                  ),
                ),
              ],
            ),
          ),
          new Center(
            child: _getLoader,
          ),
        ],
      ),
    );
  }

  get _getLoader {
    return getFullScreenLoader(
      stream: _loaderStreamController.stream,
      context: context,
    );
  }

  void resetPassword() async {
    _loaderStreamController.add(true); //show loader
    bool gotInternetConnection = await hasInternetConnection(
      context: context,
      mounted: mounted,
      canShowAlert: true,
      onFail: () {
        _loaderStreamController.add(false); //hide loader
      },
      onSuccess: () {},
    );

    if (gotInternetConnection) {
      var request = {
        "oldpassword": currentPasswordController?.text ?? "",
        "newpassword": newPasswordController?.text ?? ""
      };

      var response = await authBLoc.resetPassword(
        request: request,
        id: "",
        context: context,
      );
      _loaderStreamController.add(false); //hide loader
      //Reset successfully
      if (response != null && response is !APIError) {

      } else {
        APIError apiError = response;
        showAlert(
          context: context,
          titleText: "Error",
          message: apiError.error,
          actionCallbacks: {"OK": () {}},
        );
      }
    }
  }
}
