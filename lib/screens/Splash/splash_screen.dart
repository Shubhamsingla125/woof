import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:woof_fini/screens/Landing/landing_screen.dart';
import 'package:woof_fini/screens/Welcome/welcome_screen.dart';
import 'package:woof_fini/utils/memory_management.dart';



class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    startTimeout();
    super.initState();
    setUpSharedPreference();

  }

  setUpSharedPreference()async{
    bool sharedPrefsInitialized = await MemoryManagement.init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            color: Color(0xff74d7df),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Image.asset(
              'assets/splash_bg.png',
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              fit: BoxFit.cover,
              color: Color(0xff707070).withOpacity(0.2),
            ),
          ),
          Center(
            child: GestureDetector(
              onTap: (){
                Navigator.of(context).push(MaterialPageRoute(builder: (context)=>LandingScreen()));
              },
              child: Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(100)),
                  color: Color(0xfff8633b),
                ),
                padding: EdgeInsets.all(8),
                child: Image.asset('assets/woof_logo.png'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  startTimeout(){
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage(){
    bool isUserLogin = MemoryManagement?.getUserLoggedIn() ?? false;
    if (isUserLogin){
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => WelcomeScreen()));
    } else {
      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>LandingScreen()));
    }

  }
}
