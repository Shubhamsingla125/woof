class LoginResponse {
  String message;
  Data data;

  LoginResponse({this.message, this.data});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  User user;
  String token;

  Data({this.user, this.token});

  Data.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    data['token'] = this.token;
    return data;
  }
}

class User {
  Null googleId;
  Null facebookId;
  String sId;
  String name;
  String email;
  String breedName;
  String dob;
  String phoneNumber;
  String gender;
  int weight;
  String categoryId;
  String createdAt;
  String updatedAt;
  int iV;
  String countryCode;

  User(
      {this.googleId,
        this.facebookId,
        this.sId,
        this.name,
        this.email,
        this.breedName,
        this.dob,
        this.phoneNumber,
        this.gender,
        this.weight,
        this.categoryId,
        this.createdAt,
        this.updatedAt,
        this.iV,
        this.countryCode});

  User.fromJson(Map<String, dynamic> json) {
    googleId = json['google_id'];
    facebookId = json['facebook_id'];
    sId = json['_id'];
    name = json['name'];
    email = json['email'];
    breedName = json['breed_name'];
    dob = json['dob'];
    phoneNumber = json['phone_number'];
    gender = json['gender'];
    weight = json['weight'];
    categoryId = json['category_id'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
    countryCode = json['country_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['google_id'] = this.googleId;
    data['facebook_id'] = this.facebookId;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['breed_name'] = this.breedName;
    data['dob'] = this.dob;
    data['phone_number'] = this.phoneNumber;
    data['gender'] = this.gender;
    data['weight'] = this.weight;
    data['category_id'] = this.categoryId;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    data['country_code'] = this.countryCode;
    return data;
  }
}