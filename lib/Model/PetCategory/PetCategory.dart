class PetCategory {
  String status;
  Data data;

  PetCategory({this.status, this.data});

  PetCategory.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Pets> pets;

  Data({this.pets});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['pets'] != null) {
      pets = new List<Pets>();
      json['pets'].forEach((v) {
        pets.add(new Pets.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.pets != null) {
      data['pets'] = this.pets.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Pets {
  String image;
  String sId;
  String name;
  String createdAt;
  String updatedAt;
  int iV;

  Pets(
      {this.image,
        this.sId,
        this.name,
        this.createdAt,
        this.updatedAt,
        this.iV});

  Pets.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    sId = json['_id'];
    name = json['name'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['_id'] = this.sId;
    data['name'] = this.name;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['__v'] = this.iV;
    return data;
  }
}