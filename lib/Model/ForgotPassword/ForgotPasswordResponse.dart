class ForgotPasswordResponse {
  String message;
  int status;
  String otpCode;

  ForgotPasswordResponse({this.message, this.status, this.otpCode});

  ForgotPasswordResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    status = json['status'];
    otpCode = json['otp_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['status'] = this.status;
    data['otp_code'] = this.otpCode;
    return data;
  }
}