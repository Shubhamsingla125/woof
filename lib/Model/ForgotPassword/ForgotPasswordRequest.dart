class ForgotPasswordRequest {
  String username;
  int userType;
  String countryCode;

  ForgotPasswordRequest({
    this.username,
    this.userType,
    this.countryCode,
  });

  ForgotPasswordRequest.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    userType = json['user_type'];
    countryCode = json['country_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['user_type'] = this.userType;
    data['country_code'] = this.countryCode;
    return data;
  }
}
