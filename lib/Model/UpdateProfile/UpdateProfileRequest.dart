class UpdateProfileRequest {
  String name;
  String email;
  String breedName;
  String dob;
  String phoneNumber;
  String gender;
  String weight;
  String categoryId;

  UpdateProfileRequest(
      {this.name,
        this.email,
        this.breedName,
        this.dob,
        this.phoneNumber,
        this.gender,
        this.weight,
        this.categoryId});

  UpdateProfileRequest.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    breedName = json['breed_name'];
    dob = json['dob'];
    phoneNumber = json['phone_number'];
    gender = json['gender'];
    weight = json['weight'];
    categoryId = json['category_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['email'] = this.email;
    data['breed_name'] = this.breedName;
    data['dob'] = this.dob;
    data['phone_number'] = this.phoneNumber;
    data['gender'] = this.gender;
    data['weight'] = this.weight;
    data['category_id'] = this.categoryId;
    return data;
  }
}